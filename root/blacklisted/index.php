<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
require ('../../connection/config.php');
if (!isset($_SESSION['root_data']))
{
    echo '<div class="alert alert-danger" role="alert">
    User data not found!
  </div>';
  echo "<meta http-equiv='Refresh' Content='0; url=../../index.php'>"; 
  die();
}
$appname = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey AND APPID = :appid");
$appname->bindParam("appkey",$_SESSION['root_data']->APPKEY);
$appname->bindParam("appid",$_SESSION['root_data']->CURRAPP);
if ($appname->execute())
{
    $appnamef = $appname->fetchObject();
    $_SESSION['app_data'] = $appnamef;
}
if (isset($_POST['banhwid']))
{
    $Hwid = $_POST['HWID'];
    if (preg_match('/[A-Za-z]/',$Hwid))
    {
    $checkName = $database->prepare("SELECT NULL FROM blacklisted WHERE HWID = :hwid AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkName->bindParam("hwid",$Hwid);
    $checkName->bindParam("appkey",$_SESSION['root_data']->APPKEY);
    $checkName->bindParam("appownerkey",$_SESSION['root_data']->APPOWNERKEY);
    $checkName->execute();
    if ($checkName->rowCount() > 0)
    {
    }
  else
  {
    $addUser = $database->prepare("INSERT INTO blacklisted(HWID,APPKEY,APPOWNERKEY) VALUES(:HWID,:APPKEY,:APPOWNERKEY)");
    $addUser->bindParam("HWID",$Hwid);
    $addUser->bindParam("APPKEY",$_SESSION['root_data']->APPKEY);
    $addUser->bindParam("APPOWNERKEY",$_SESSION['root_data']->APPOWNERKEY);
    if ($addUser->execute())
    {
    }
    else
    {
    }
    }
  }
}
if (isset($_POST['deletehwid']))
{
    $DeletedUser = $_POST['deletehwid'];
    $removeuser = $database->prepare("DELETE FROM blacklisted WHERE HWID = :hwid AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $removeuser->bindParam("hwid",$DeletedUser);    
    $removeuser->bindParam("appkey",$_SESSION['root_data']->APPKEY);
        $removeuser->bindParam("appownerkey",$_SESSION['root_data']->APPOWNERKEY);
        $removeuser->execute();
}
if (isset($_POST['delall']))
{
    $removeuser = $database->prepare("DELETE FROM blacklisted WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $removeuser->bindParam("appkey",$_SESSION['root_data']->APPKEY);
        $removeuser->bindParam("appownerkey",$_SESSION['root_data']->APPOWNERKEY);
        $removeuser->execute();
}
?>
<head>
           
        <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;
    }
    </style>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="../../index.php">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item">
              <a class="nav-link" href="../keys">Keys</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../users">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../variables">Variables</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../blacklisted">Blacklisted</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../dashboard">Account</a>
            </li>
           
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">
      <br>
 <form method="POST">
 <div class="col-sm-4">
<div class="card text-white bg-dark">
<div class="card-body">
 <div class="d-grid gap-2 d-md-block">
 <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#newkey">Ban New HWID</button>
 <button class="btn btn-secondary" type="submit" name="delall">Unban All</button>
 </div>
 </div>
 </div>
 </div>
 <br>
 <div class="modal fade" id="newkey" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ban HWID</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
    HWID : <input class="form-control" placeholder="Hardware ID" name="HWID"/>
              <p> </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="banhwid">Ban</button>
      </div>
    </div>
  </div>
</div>
<div class="card text-white bg-dark">
<div class="card-body">
 <table class="table table-striped table-dark">
  <thead>
    <tr>
    <th scope="col">HWID</th>
    <th scope="col">Management</th>
    </tr>
  </thead>
  <tbody>
  <?php
    $sqlResult = $database->prepare("SELECT HWID FROM blacklisted WHERE APPOWNERKEY = :appownerkey AND APPKEY = :appkey");    $sqlResult->bindParam("appkey",$_SESSION['root_data']->APPKEY);
    $sqlResult->bindParam("appownerkey",$_SESSION['root_data']->APPOWNERKEY);
    $sqlResult->bindParam("appkey",$_SESSION['root_data']->APPKEY);
    $sqlResult->execute();
      foreach($sqlResult AS $result)
      {
        $HWID = $result['HWID'];
        echo '<tr>
        <th scope="row">'. $HWID .'</th>
        <td>
        <button class="btn btn-danger" type="submit" value='.$HWID.' name="deletehwid">Delete</button>
        </td>
      </tr>';
      }
    ?>
  </tbody>
</table>
</div>
</div>
</form>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
</div>