<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
require ('../../connection/config.php');
if (!isset($_SESSION['root_data']))
{
    echo '<div class="alert alert-danger" role="alert">
    User data not found!
  </div>';
  echo "<meta http-equiv='Refresh' Content='0; url=../../index.php'>"; 
  die();
}
$appname = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey AND APPID = :appid");
$appname->bindParam("appkey",$_SESSION['root_data']->APPKEY);
$appname->bindParam("appid",$_SESSION['root_data']->CURRAPP);
if ($appname->execute())
{
    $appnamef = $appname->fetchObject();
    $_SESSION['app_data'] = $appnamef;
}
if (isset($_POST['adduser']))
{
    $Username = $_POST['Username'];
    if (preg_match('/[A-Za-z]/',$Username))
    {
    $Password = $_POST['Password'];
    if (preg_match('/[A-Za-z]/',$Password))
    {
    $Rank = $_POST['Rank'];
    if (preg_match('/[A-Za-z]/',$Rank))
    {
    $checkName = $database->prepare("SELECT NULL FROM accounts WHERE NAME = :name AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkName->bindParam("name",$Username);
    $checkName->bindParam("appkey",$_SESSION['root_data']->APPKEY);
    $checkName->bindParam("appownerkey",$_SESSION['root_data']->APPOWNERKEY);
    $checkName->execute();
    if ($checkName->rowCount() > 0)
    {
  }
  else
  {
    $timenow =date("Y/m/d");
        $oneday = date("Y/m/d", strtotime("+1 day"));
        $threedays = date("Y/m/d", strtotime("+3 day"));
        $oneweek = date("Y/m/d", strtotime("+7 day"));
        $threeweeks = date("Y/m/d", strtotime("+21 day"));
        $onemonth = date("Y/m/d", strtotime("+1 month"));
        $threemonths = date("Y/m/d", strtotime("+3 month"));
        $USERexpireDATE = 'lifetime';
        if ($_POST['flexRadioDefaultrrw'] === "Lifetime" )
        {
          $USERexpireDATE = 'Lifetime';
        }
        else if ($_POST['flexRadioDefaultrrw'] === '3m' )
        {
          $USERexpireDATE = $threemonths;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "1m" )
        {
          $USERexpireDATE = $onemonth;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "3w" )
        {
          $USERexpireDATE = $threeweeks;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "1w" )
        {
          $USERexpireDATE = $oneweek;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "3d" )
        {
          $USERexpireDATE = $threedays;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "1d" )
        {
          $USERexpireDATE = $oneday;
        }
        $addUser = $database->prepare("INSERT INTO accounts(NAME,PASSWORD,RANKUSER,CREATEDATE,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:NAME,:PASSWORD,:RANKUSER,:CREATEDATE,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
        $addUser->bindParam("NAME",$Username);
        $addUser->bindParam("PASSWORD",hash($Secure_Hash_Algorithm,$Password));
        $addUser->bindParam("RANKUSER",$Rank);
        $addUser->bindParam("CREATEDATE",$timenow);
        $addUser->bindParam("EXPIREDATE",$USERexpireDATE);
        $addUser->bindParam("APPKEY",$_SESSION['root_data']->APPKEY);
        $addUser->bindParam("APPOWNERKEY",$_SESSION['root_data']->APPOWNERKEY);
        if ($addUser->execute())
        {
        }
        else
        {
        }
    }
    }
  }
  }
}
if (isset($_POST['deleteuser']))
{
  {
    $DeletedUser = $_POST['deleteuser'];
    $checkU = $database->prepare("SELECT NULL FROM accounts WHERE ID = :name AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkU->bindParam(":name",$DeletedUser);
    $checkU->bindParam(":appkey",$_SESSION['root_data']->APPKEY);
    $checkU->bindParam(":appownerkey",$_SESSION['root_data']->APPOWNERKEY);
    $checkU->execute();
    if ($checkU->rowCount() === 1)
    {
    $removeuser = $database->prepare("DELETE FROM accounts WHERE ID = :name AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $removeuser->bindParam("name",$DeletedUser);    
    $removeuser->bindParam("appkey",$_SESSION['root_data']->APPKEY);
        $removeuser->bindParam("appownerkey",$_SESSION['root_data']->APPOWNERKEY);
        if ($removeuser->execute())
        {
        }
        else
        {
        }
    }
    else
    {
    }
}
}
if (isset($_POST['resetuser']))
{
    $password = "NULL";
    $ResetedUser = $_POST['resetuser'];
    $changePass = $database->prepare("UPDATE accounts SET HWID = :hwid WHERE ID = :name AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $changePass->bindParam(":hwid",$password);
    $changePass->bindParam(":name",$ResetedUser);
    $changePass->bindParam(":appkey",$_SESSION['root_data']->APPKEY);
    $changePass->bindParam(":appownerkey",$_SESSION['root_data']->APPOWNERKEY);
        if ($changePass->execute())
        {
        }
        else
        {
        }
}
if (isset($_POST['delall']))
{
    $aremovekey = $database->prepare("DELETE FROM accounts WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $aremovekey->bindParam("appkey",$_SESSION['root_data']->APPKEY);
        $aremovekey->bindParam("appownerkey",$_SESSION['root_data']->APPOWNERKEY);
        $aremovekey->execute();
}
if (isset($_POST['delused']))
{
    $password = "NULL";
    $aremovekey = $database->prepare("UPDATE accounts SET HWID = :hwid WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");    $aremovekey->bindParam("appkey",$_SESSION['root_data']->APPKEY);
    $aremovekey->bindParam("hwid",$password);
    $aremovekey->bindParam("appkey",$_SESSION['root_data']->APPKEY);
    $aremovekey->bindParam("appownerkey",$_SESSION['root_data']->APPOWNERKEY);
        $aremovekey->execute();
}
?>
<head>
        <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;
    }
    </style>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="../../index.php">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item">
              <a class="nav-link" href="../keys">Keys</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../users">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../variables">Variables</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../blacklisted">Blacklisted</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../dashboard">Account</a>
            </li>
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">
      <br>
 <form method="POST">
 <div class="col-sm-5">
<div class="card text-white bg-dark">
<div class="card-body">
 <div class="d-grid gap-2 d-md-block">
 <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#newkey">Add New user</button>
 <button class="btn btn-secondary" type="submit" name="delall">Delete All users</button>
 <button class="btn btn-success" type="submit" name="delused">Mass HWID reset</button>
 </div>
 </div>
 </div>
 </div>
 <br>
 <div class="modal fade" id="newkey" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
    Username : <input class="form-control" placeholder="Username" name="Username"/>
              <p> </p>
    Password : <input class="form-control" placeholder="Password" name="Password"/>
              <p> </p>
    Rank : <input class="form-control" placeholder="User Rank" name="Rank" value="Customer"/>
              <p> </p>
    Expire Date : <select name="flexRadioDefaultrrw" class="form-control">
                <option value="1d">1 Day</option>
                <option value="3d">3 Days</option>
                <option value="1w">1 Week</option>
                <option value="3w">3 Weeks</option>
                <option value="1m">1 Month</option>
                <option value="3m">3 Months</option>
                <option value="Lifetime">Lifetime</option>
              </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="adduser">Create</button>
      </div>
    </div>
  </div>
</div>
<div class="card text-white bg-dark">
<div class="card-body">
 <table class="table table-striped table-dark">
  <thead>
    <tr>
    <th scope="col">Username</th>
    <th scope="col">Rank</th>
    <th scope="col">Create Date</th>
    <th scope="col">Expire Date</th>
    <th scope="col">Hardware ID</th>
    <th scope="col">Management</th>
    </tr>
  </thead>
  <tbody>
  <?php
    $sqlResult = $database->prepare("SELECT ID, NAME, RANKUSER, CREATEDATE, EXPIREDATE, HWID FROM accounts WHERE APPOWNERKEY = :appownerkey AND APPKEY = :appkey");    $sqlResult->bindParam("appkey",$_SESSION['root_data']->APPKEY);
    $sqlResult->bindParam("appownerkey",$_SESSION['root_data']->APPOWNERKEY);
    $sqlResult->execute();
      foreach($sqlResult AS $result)
      {
          $id = $result['ID'];
        $username = $result['NAME'];
        $rank = $result['RANKUSER'];
        $createdate = $result['CREATEDATE'];
        $expiredate = $result['EXPIREDATE'];
        $hwid = $result['HWID'];
        echo '<tr>
        <th scope="row">'. $username .'</th>
        <td>'. $rank .'</td>
        <td>'. $createdate .'</td>
        <td>'. $expiredate .'</td>
        <td>'. $hwid .'</td>
        <td>
        <button class="btn btn-warning" type="submit" value='.$id.' name="resetuser">Reset HWID</button>
        <button class="btn btn-danger" type="submit" value='.$id.' name="deleteuser">Delete</button>
        </td>
      </tr>';
      }
    ?>
  </tbody>
</table>
</div>
</div>
</form>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
</div>