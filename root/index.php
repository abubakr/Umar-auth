<?php
      session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
ob_start();
require ('../connection/config.php');
if (isset($_SESSION['root_data']))
{
  echo "<meta http-equiv='Refresh' Content='0; url=./keys'>"; 
  exit();  
}
if (isset($_GET['app'])) 
{
     $appkey = $_GET['app']; 
}
else
{
    echo "<meta http-equiv='Refresh' Content='0; url=../index.php'>"; 
    exit();  
}
if (isset($_GET['owner'])) 
{
     $acckey = $_GET['owner']; 
}
else
{
  echo "<meta http-equiv='Refresh' Content='0; url=../index.php'>"; 
    exit();  
}
$sRank = "0";
$checkRANKl = $database->prepare("SELECT ID, RANK FROM users WHERE ACCOUNTKEY = :ACCOUNTKEY");
$checkRANKl->bindParam(":ACCOUNTKEY", $acckey);
$checkRANKl->execute();
$owner_infX = $checkRANKl->fetchObject();
if ($checkRANKl->rowCount() !== 1)
{
  echo "<meta http-equiv='Refresh' Content='0; url=../index.php'>"; 
  exit();  
}
else
{
  if ($owner_infX->RANK == $sRank)
  {
    echo "<meta http-equiv='Refresh' Content='0; url=../index.php'>"; 
    exit();  
  }
}
$checkAPPEx = $database->prepare("SELECT APPUNID FROM apps WHERE APPKEY = :APPKEY");
$checkAPPEx->bindParam(":APPKEY", $appkey);
$checkAPPEx->execute();
$app_infX = $checkAPPEx->fetchObject();
if ($checkAPPEx->rowCount() !== 1)
{
  echo "<meta http-equiv='Refresh' Content='0; url=../index.php'>"; 
  exit();  
}
else
{
  if ($owner_infX->ID !== $app_infX->APPUNID)
  {
    echo "<meta http-equiv='Refresh' Content='0; url=../index.php'>"; 
    exit();  
  }
}
if (isset($_POST['signin']))
{
    $Username = $_POST['Username'];
    $Password = $_POST['Password'];
    $checkE = $database->prepare("SELECT * FROM rootpanel WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkE->bindParam(":name",$Username);
    $checkE->bindParam(":password",hash($Secure_Hash_Algorithm,$Password));
    $checkE->bindParam(":appkey",$appkey);
    $checkE->bindParam(":appownerkey",$acckey);
    $checkE->execute();
    if ($checkE->rowCount() === 1)
    {
      $ruser = $checkE->fetchObject();
      $_SESSION['root_data'] = $ruser;
      if (isset($_SESSION['root_data']))
      {
        echo '<div class="alert alert-success" role="alert">
        Access Granted
      </div>';
      echo "<meta http-equiv='Refresh' Content='0; url=keys'>";   
      }
    }
    else
    {
        echo '<div class="alert alert-danger" role="alert">
        Access Denied
      </div>';
    }
}
ob_end_flush();
?>
<!doctype html>
<html lang="en">
<head>
        <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    .form-control:disabled {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    </style>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="../index.php">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
          <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
<div class="container">
<body class="text-center">
<div class="col d-flex justify-content-center">
<form method="POST">
    <br>
    <img class="mb-4" src="../assets/authLogo.png" alt="" width="100" height="100">
    <div class="card text-white bg-dark" style="width: 16rem;">
  <class="card-img-top" alt="...">
  <div class="card-body">
  <input class="form-control mt-3" placeholder="Username" type="text" name="Username" required/>
  <input class="form-control mt-3" placeholder="Password" type="password" name="Password" required/>
    <div class="d-grid gap-2">
    <button class="btn btn-primary mt-3" type="submit" name="signin">Go</button>
    </div>
  </div>
</div>
</form>
</div>
</body>
</div>
  </body>
</html>