<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
require ('../../connection/config.php');
if (!isset($_SESSION['root_data']))
{
    echo '<div class="alert alert-danger" role="alert">
    User data not found!
  </div>';
  echo "<meta http-equiv='Refresh' Content='0; url=../../index.php'>"; 
  die();
}
$appname = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey AND APPID = :appid");
$appname->bindParam("appkey",$_SESSION['root_data']->APPKEY);
$appname->bindParam("appid",$_SESSION['root_data']->CURRAPP);
if ($appname->execute())
{
    $appnamef = $appname->fetchObject();
    $_SESSION['app_data'] = $appnamef;
}
if (isset($_POST['signout']))
{
    session_unset();
    session_destroy();
    echo "<meta http-equiv='Refresh' Content='0; url=../../index.php'>"; 
    die();
}
?>
<head>
        <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;
    }
    </style>
</head>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="../../index.php">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item">
              <a class="nav-link" href="../keys">Keys</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../users">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../variables">Variables</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../blacklisted">Blacklisted</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../dashboard">Account</a>
            </li>
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">

  <form method="POST">
    <br>
    <div class="col-sm-2">
    <div class="card text-white bg-dark">
          <div class="card-body">
          <button class="btn btn-primary mt-3" type="submit" name="signout">Logout</button> 
            </div>
          </div>
        </div>
 </form>
</form>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
</div>