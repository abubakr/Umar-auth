<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <style>
        @import url('../assets/font.css');
*
{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: 'Poppins', sans-serif;
}
        section
{
    position: relative;
    width: 100%;
    min-height: 100vh;
    padding: 100px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    background: #161616;
}
section .circle
{
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: #1e64e5;
    clip-path: circle(70% at right -20%);
}
header
{
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    padding: 40px 100px;
    display: flex;
    justify-content: space-between;
    align-items: center;
}
header .logo
{
    position: relative;
    max-width: 150px;
}
header .navigation
{
    position: relative;
    display: flex;
}
header .navigation li
{
    list-style: none;
}
header .navigation li a
{
    display: inline-block;
    color: #fff;
    font-weight: 500;
    text-decoration: none;
    margin-left: 40px;
}
.content
{
    position: relative;
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
}
.content .textBox
{
    position: relative;
    max-width: 600px;
    margin-right: 20px;
}
.content .textBox h2
{
    color: #fff;
    font-size: 3em;
    margin-bottom: 10px;
    line-height: 1.4em;
    font-weight: 700;
}
.content .textBox p
{
    color: #fff;
}
.content .textBox a
{
    display: inline-block;
    margin-top: 20px;
    padding: 8px 20px;
    background: #1e64e5;
    color: #fff;
    border-radius: 40px;
    font-weight: 500;
    letter-spacing: 1px;
    text-decoration: none;
}
.bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .b-example-divider {
        height: 3rem;
        background-color: #191919;
        border: solid rgba(0, 0, 0, .20);
        border-width: 1px 0;
        box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .20);
      }

      .b-example-vr {
        flex-shrink: 0;
        width: 1.5rem;
        height: 100vh;
      }

      .bi {
        vertical-align: -.125em;
        fill: currentColor;
      }

      .nav-scroller {
        position: relative;
        z-index: 2;
        height: 2.75rem;
        overflow-y: hidden;
      }

      .nav-scroller .nav {
        display: flex;
        flex-wrap: nowrap;
        padding-bottom: 1rem;
        margin-top: -1px;
        overflow-x: auto;
        text-align: center;
        white-space: nowrap;
        -webkit-overflow-scrolling: touch;
        
      }

      body {
        background-color: rgb(44,48,52);
        color: white;
      }
    </style>
  </head>
  <body>
      
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="../index.php">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
          <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
<div class="container">
<form method="POST">
    <br>
    <div class="row" data-masonry='{"percentPosition": true }'>
    <div class="col-sm-6 col-lg-4 mb-4">
      <div class="card text-white bg-dark p-3">
        <figure class="p-3 mb-0">
          <blockquote class="blockquote">
            <p>Ground rules</p>
          </blockquote>
          <figcaption class="blockquote-footer mb-0 text-muted">
          The dos and don'ts to avoid getting <cite title="Source Title">terminated</cite>
          </figcaption>
        </figure>
      </div>
    </div>
    <div class="col-sm-12 col-lg-8 mb-8">
      <div class="card text-white bg-dark p-3">
        <figure class="p-3 mb-0">
          <p style="color:red">- Sharing your account is not allowed</p>
          <p style="color:red">- Selling your account is not allowed</p>
          <p style="color:green">+ You can Log into your account on any computer, at any house, between houses, as long as it is you using the account</p>
          <p style="color:green">+ Use of VPN's is fine</p>
        </figure>
      </div>
    </div>
</div>
<p><strong>Terms of Service</strong></p>
<p><strong>-> Important Information</strong></p>
<p>You must first read these conditions very carefully, because once you log in to the website, you agree to all these laws without any exception and breaching any of them will lead to a permanent ban and without any restrictions and without contacting you.</p>
<p>• When we say "Umar" or "us" or "we" it refers the company "Tawhid industries, LLC".</p>
<p>• When we say "You" it means you as a user of our services.</p>
<p><strong>-> Conditions</strong></p>
<p>• We do not allow anything haram and uses must be at all costs under Islamic law, no exceptions.</p>
<p>• We do not allow anything related to cheats/hacks, even fraud and anything malicious, and we consider this law to be clear and does not need any evidence of why it is taken and why we always careful users to not breached it.</p>
<p>• We do not allow any kind of attempts to crack or eavesdrop on our services, and you must inform us in our Discord server if you find a bug.</p>
<p><strong>-> Refund Policy</strong></p>
<p>You are entitled to a refund provided you contact us with 30 days of purchase, with a valid reason. After these 30 days you are no longer entitled to any refund of any kind. If you would like to request a refund, please contact us by creating a ticket in our Discord server.</p>
<p>Examples of what we would not consider a valid reasoning like:</p>
<p>• Requesting a refund after 30 days.</p>
<p>• Stating that the purchase was simply to test, i.e. a trial purchase.</p>
<p>• Stating that you need the money back, or that you no longer use the services, etc.</p>
<p><strong>-> Privacy Policy</strong></p>
<p>At Umar, we take your privacy seriously. We value your privacy as if it's our own and we ensure that your anonymity is protected using our service. We understand that your data is personal and valuable, and we take our responsibility to protect it seriously. We will never sell, share, or rent any of your data to third parties. We already can't collect any of your personal information nor your IP address or any sensitive data at all. We ensure your anonymity using our service and your email and password are SHA512 hashed, making sure that your login credentials are secure. Trust us to keep your data safe and secure.</p>
</form>
</div>
  </body>
</html>