<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
<?php
require ('../connection/config.php');
if (!isset($_SESSION['user_data']))
{
  echo "<meta http-equiv='Refresh' Content='0; url=../index.php'>"; 
  die();
}
if ($_SESSION['user_data']->RANK !== "1")
{
echo "<meta http-equiv='Refresh' Content='0; url=../index.php'>"; 
die();
}
if (isset($_POST['deleteapp']))
{
  $APPKEY = $_POST['deleteapp'];
  $checkOID = $database->prepare("SELECT APPUNID FROM apps WHERE APPKEY = :appkey");
  $checkOID->bindParam(":appkey",$APPKEY);
  $checkOID->execute();
  $appowner_data = $checkOID->fetchObject();
  $UniqueID = $appowner_data->APPUNID;
  $deleteaccAccs = $database->prepare("DELETE FROM accounts WHERE APPKEY = :appkey;");
  $deleteaccAccs->bindParam(":appkey",$APPKEY);
  $deleteaccAccs->execute();
  $removeAKeys = $database->prepare("DELETE FROM userskeys WHERE APPKEY = :appkey");
  $removeAKeys->bindParam(":appkey",$APPKEY);
  $removeAKeys->execute();
  $removeARoot = $database->prepare("DELETE FROM rootpanel WHERE APPKEY = :appkey");
  $removeARoot->bindParam(":appkey",$APPKEY);
  $removeARoot->execute();
  $removeAVars = $database->prepare("DELETE FROM vars WHERE APPKEY = :appkey");
  $removeAVars->bindParam(":appkey",$APPKEY);
  $removeAVars->execute();
  $removeABLK = $database->prepare("DELETE FROM blacklisted WHERE APPKEY = :appkey");
  $removeABLK->bindParam(":appkey",$APPKEY);
  $removeABLK->execute();
  $updatewApps = $database->prepare("UPDATE users SET APPS = APPS - 1 WHERE ID = :id");
  $updatewApps->bindParam(":id",$UniqueID);
  $updatewApps->execute();
  $removeApps = $database->prepare("DELETE FROM apps WHERE APPKEY = :appkey");
  $removeApps->bindParam(":appkey",$APPKEY);
  $removeApps->execute();
}
?>
  <head>
    <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <link rel="icon" href="../assets/authLogo.png">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    .form-control:disabled {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    </style>
      <div class="container-fluid">
        <a class="navbar-brand" href="../index.php">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item">
              <a class="nav-link" href="./index.php">Accounts</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./apps.php">Applications</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./stats.php">Statistics</a>
            </li>
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">
  <form method="POST">
    <br>
    <div class="card text-white bg-dark">
<div class="card-body">
<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col"> </th>
      <th scope="col">  </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Total Users:</th>
      <td><?php
          $checkA = $database->prepare("SELECT NULL FROM users");
          $checkA->execute();
          $num = $checkA->rowCount();
          echo $num;
      ?></td>
    </tr>
    <tr>
      <th scope="row">Total Applications:</th>
      <td><?php
          $checkA = $database->prepare("SELECT NULL FROM apps");
          $checkA->execute();
          $num = $checkA->rowCount();
          echo $num;
      ?></td>
    </tr>
        <tr>
      <th scope="row">Total Requests:</th>
      <td><?php
          $checkReq = $database->prepare("SELECT NULL FROM req");
          $checkReq->execute();
          $num = $checkReq->rowCount();
          echo $num;
      ?></td>
    </tr>
  </tbody>
</table>
</div>
    </div>
 </form>
 <form method="POST">
 <div class="card text-white bg-dark">
<div class="card-body">
<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col"> </th>
      <th scope="col">  </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Premium Users:</th>
      <td><?php
      $rankNUMbro = "2";
          $checkAccPRemium = $database->prepare("SELECT NULL FROM users WHERE RANK = :rank");
          $checkAccPRemium->bindParam(":rank", $rankNUMbro);
          $checkAccPRemium->execute();
          $num = $checkAccPRemium->rowCount();
          echo $num;
      ?></td>
    </tr>
    <tr>
      <th scope="row">Premium Pourcentage:</th>
      <td><?php
      $rankNUMbro = "2";
      $checkAccPRemium = $database->prepare("SELECT NULL FROM users WHERE RANK = :rank");
      $checkAccPRemium->bindParam(":rank", $rankNUMbro);
      $checkAccPRemium->execute();
      $num = $checkAccPRemium->rowCount();
      $checkA = $database->prepare("SELECT NULL FROM users");
      $checkA->execute();
      $num2 = $checkA->rowCount();
      $num3 = $num * 100 / $num2;
      echo intval($num3), "%";
      ?></td>
    </tr>
  </tbody>
</table>
</div>
    </div>
 </form>
   <form method="POST">
    <br>
    <div class="card text-white bg-dark">
<div class="card-body">
<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col"> </th>
      <th scope="col">  </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Total Accounts:</th>
      <td><?php
          $checkA = $database->prepare("SELECT NULL FROM accounts");
          $checkA->execute();
          $num = $checkA->rowCount();
          echo $num;
      ?></td>
    </tr>
    <tr>
      <th scope="row">Total Keys:</th>
      <td><?php
          $checkA = $database->prepare("SELECT NULL FROM userskeys");
          $checkA->execute();
          $num = $checkA->rowCount();
          echo $num;
      ?></td>
    </tr>
        <tr>
      <th scope="row">Total Variables:</th>
      <td><?php
          $checkA = $database->prepare("SELECT NULL FROM vars");
          $checkA->execute();
          $num = $checkA->rowCount();
          echo $num;
      ?></td>
    </tr>
        <tr>
      <th scope="row">Total Root Accounts:</th>
      <td><?php
          $checkA = $database->prepare("SELECT NULL FROM rootpanel");
          $checkA->execute();
          $num = $checkA->rowCount();
          echo $num;
      ?></td>
    </tr>
  </tbody>
</table>
</div>
    </div>
 </form>
</div>
  </head>