<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
<?php
require ('../connection/config.php');
if (!isset($_SESSION['user_data']))
{
  echo "<meta http-equiv='Refresh' Content='0; url=../index.php'>"; 
  die();
}
if ($_SESSION['user_data']->RANK !== "1")
{
echo "<meta http-equiv='Refresh' Content='0; url=../index.php'>"; 
die();
}
if (isset($_POST['deleteapp']))
{
  $APPKEY = $_POST['deleteapp'];
  $checkOID = $database->prepare("SELECT APPUNID FROM apps WHERE APPKEY = :appkey");
  $checkOID->bindParam(":appkey",$APPKEY);
  $checkOID->execute();
  $appowner_data = $checkOID->fetchObject();
  $UniqueID = $appowner_data->APPUNID;
  $deleteaccAccs = $database->prepare("DELETE FROM accounts WHERE APPKEY = :appkey;");
  $deleteaccAccs->bindParam(":appkey",$APPKEY);
  $deleteaccAccs->execute();
  $removeAKeys = $database->prepare("DELETE FROM userskeys WHERE APPKEY = :appkey");
  $removeAKeys->bindParam(":appkey",$APPKEY);
  $removeAKeys->execute();
  $removeARoot = $database->prepare("DELETE FROM rootpanel WHERE APPKEY = :appkey");
  $removeARoot->bindParam(":appkey",$APPKEY);
  $removeARoot->execute();
  $removeAVars = $database->prepare("DELETE FROM vars WHERE APPKEY = :appkey");
  $removeAVars->bindParam(":appkey",$APPKEY);
  $removeAVars->execute();
  $removeABLK = $database->prepare("DELETE FROM blacklisted WHERE APPKEY = :appkey");
  $removeABLK->bindParam(":appkey",$APPKEY);
  $removeABLK->execute();
  $updatewApps = $database->prepare("UPDATE users SET APPS = APPS - 1 WHERE ID = :id");
  $updatewApps->bindParam(":id",$UniqueID);
  $updatewApps->execute();
  $removeApps = $database->prepare("DELETE FROM apps WHERE APPKEY = :appkey");
  $removeApps->bindParam(":appkey",$APPKEY);
  $removeApps->execute();
}
?>
  <head>
    <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <link rel="icon" href="../assets/authLogo.png">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    .form-control:disabled {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    </style>
      <div class="container-fluid">
        <a class="navbar-brand" href="">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item">
              <a class="nav-link" href="./index.php">Accounts</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./apps.php">Applications</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./stats.php">Statistics</a>
            </li>
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">
    <style>
        .pfpbruh{
          width: 80px;
            height: 80px;
            border-radius: 15px;
            padding: 0px;
            box-shadow: 0px 0px 15px #2C2E34;
        }
    </style>
  <form method="POST">
    <br>
    <div class="card text-white bg-dark">
<div class="card-body">
 <table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Owner Name</th>
      <th scope="col">Application Name</th>
            <th scope="col">Accounts</th>
                  <th scope="col">Keys</th>
                        <th scope="col">Variables</th>
      <th scope="col">Management</th>
    </tr>
  </thead>
  <tbody>
<?php
$sqlReq = $database->prepare("SELECT ID, APPOWNERID, APPNAME, APPKEY, APPID FROM apps");
$sqlReq->execute();
foreach($sqlReq AS $result)
{
  $dddd = $result['ID'];
  $appownerid = $result['APPOWNERID'];
  $appownerdata_yes = $database->prepare("SELECT NAME FROM users WHERE ACCOUNTID = :appownerid");
  $appownerdata_yes->bindParam(":appownerid",$appownerid);
  $appownerdata_yes->execute();
  $appownerdata_yes = $appownerdata_yes->fetch();
  $appownername = $appownerdata_yes['NAME'];
  $appname = $result['APPNAME'];
  $appkey = $result['APPKEY'];
  $appid = $result['APPID'];
  $sUsers = $database->prepare("SELECT NULL FROM accounts WHERE APPKEY = :appkey");
  $sUsers->bindParam(":appkey",$appkey);
  $sUsers->execute();
  $usera = $sUsers->rowCount();
  $sKeys = $database->prepare("SELECT NULL FROM userskeys WHERE APPKEY = :appkey");
  $sKeys->bindParam(":appkey",$appkey);
  $sKeys->execute();
  $keya = $sKeys->rowCount();
  $sVars = $database->prepare("SELECT NULL FROM vars WHERE APPKEY = :appkey");
  $sVars->bindParam(":appkey",$appkey);
  $sVars->execute();
  $vara = $sVars->rowCount();
  echo '<tr>
  <th>'.$appownername.'</td>
  <th scope="row">'. $appname .'</th>
    <th>'. $usera .'</th>
      <th>'. $keya .'</th>
        <th>'. $vara .'</th>
  <td>
  <button class="btn btn-danger" type="submit" value='.$appkey.' name="deleteapp">Delete</button>
  </td>
</tr>';
}
?>
  </tbody>
</table>
</div>
    </div>
 </form>
</div>
  </head>