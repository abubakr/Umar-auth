<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="sweetalert2.all.min.js"></script>
<?php
require ('../connection/config.php');
if (!isset($_SESSION['user_data']))
{
  echo "<meta http-equiv='Refresh' Content='0; url=../index.php'>"; 
  die();
}
if ($_SESSION['user_data']->RANK !== "1")
{
echo "<meta http-equiv='Refresh' Content='0; url=../index.php'>"; 
die();
}
if (isset($_POST['removeacc']))
{
  $AccKey = $_POST['removeacc'];
  if ($_SESSION['user_data']->ACCOUNTKEY != $AccKey) {
      $deleteaccKeys = $database->prepare("DELETE FROM userskeys WHERE APPOWNERKEY = :appownerkey;");
  $deleteaccKeys->bindParam(":appownerkey",$AccKey);
  if($deleteaccKeys->execute())
  {
    $deleteaccAccs = $database->prepare("DELETE FROM accounts WHERE APPOWNERKEY = :appownerkey;");
    $deleteaccAccs->bindParam(":appownerkey",$AccKey);
    if($deleteaccAccs->execute())
    {
  $removeARoot = $database->prepare("DELETE FROM rootpanel WHERE APPOWNERKEY = :appownerid");
  $removeARoot->bindParam(":appownerid",$AccKey);
  $removeARoot->execute();
  $removeAVars = $database->prepare("DELETE FROM vars WHERE APPOWNERKEY = :appownerid");
  $removeAVars->bindParam(":appownerid",$AccKey);
  $removeAVars->execute();
  $removeABLK = $database->prepare("DELETE FROM blacklisted WHERE APPOWNERKEY = :appownerid");
  $removeABLK->bindParam(":appownerid",$AccKey);
  $removeABLK->execute();
  $removeApps = $database->prepare("DELETE FROM apps WHERE APPOWNERKEY = :appownerid");
  $removeApps->bindParam(":appownerid",$AccKey);
  $removeApps->execute();
  $removeAccount = $database->prepare("DELETE FROM users WHERE ACCOUNTKEY = :accountkey");
  $removeAccount->bindParam(":accountkey",$AccKey);
  $removeAccount->execute();
  }
}
  }
}
?>
  <head>
    <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    .form-control:disabled {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    </style>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="../index.php">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item">
              <a class="nav-link" href="./index.php">Accounts</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./apps.php">Applications</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./stats.php">Statistics</a>
            </li>
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">
      <br>
    <style>
        .pfpbruh{
          width: 80px;
            height: 80px;
            border-radius: 15px;
            padding: 0px;
            box-shadow: 0px 0px 15px #2C2E34;
        }
    </style>

  <form method="POST">
  <div class="card text-white bg-dark">
<div class="card-body">
 <table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Username</th>
      <th scope="col">Rank</th>
      <th scope="col">Apps</th>
      <th scope="col">Referrals</th>
      <th scope="col">Earnings</th>
      <th scope="col">Management</th>
    </tr>
  </thead>
  <tbody>
<?php
function getRankbynum($num)
{
  if ($num == "0")
  {
    return "Starter";
  }
  else if ($num == "1")
  {
    return "Adminstrator";
  }
  else if ($num == "2")
  {
    return "Premium";
  }
}
function getEar($gv)
{
    return $gv * 4.99;
}
$valPR = "2";
$sqlReq = $database->prepare("SELECT ID, NAME, ACCOUNTKEY, ACCOUNTID, RANK, APPS FROM users");
$sqlReq->execute();
foreach($sqlReq AS $result)
{
    $numid = $result['ID'];
  $username = $result['NAME'];
  $acckey = $result['ACCOUNTKEY'];
  $accid = $result['ACCOUNTID'];
  $rank = $result['RANK'];
  $numofapps = $result['APPS'];
  $findREF = $database->prepare("SELECT NULL FROM users WHERE REF = :ref");
  $findREF->bindParam(":ref", $accid);
  $findREF->execute();
  $refyesorno = $findREF->rowCount();
  $gEarn = $database->prepare("SELECT NULL FROM users WHERE REF = :ref AND RANK = :ranky");
  $gEarn->bindParam(":ref", $accid);
  $gEarn->bindParam(":ranky", $valPR);
  $gEarn->execute();
  $gEVal = $gEarn->rowCount();
  echo '<tr>
  <td>'. $username .'</td>
  <td>'. getRankbynum($rank) .'</td>
  <td>'. $numofapps .'</td>
    <td>'. $refyesorno .'</td>
      <td>'. getEar($gEVal), "$" .'</td>
  <td>
  <div class="btn-group" role="group" aria-label="Basic example">
  <button class="btn btn-secondary" type="submit" value='.$accid.' name="promoteacc">Promote</button>
  <button class="btn btn-danger" type="submit" value='.$acckey.' name="removeacc">Delete</button>
</div>
  </td>
</tr>';
}
?>
  </tbody>
</table>
</div>
  </div>
                <?php
if (isset($_POST['promoteacc']))
{
  $accid = $_POST['promoteacc'];
  $checkR = $database->prepare("SELECT RANK FROM users WHERE ACCOUNTID = :accountid");
  $checkR->bindParam(":accountid",$accid);
  $checkR->execute();
foreach($checkR AS $result)
{
  $rank = $result['RANK'];
}
$nrank = "0";
if ($rank === "0")
{
  $nrank = "2";
}
else if ($rank === "2")
{
  $nrank = "0";
}

    $upgradeRank = $database->prepare("UPDATE users SET RANK = :rank WHERE ACCOUNTID = :accountid");
    $upgradeRank->bindParam(":rank",$nrank);
    $upgradeRank->bindParam(":accountid",$accid);
    if ($upgradeRank->execute())
    {
        echo "<meta http-equiv='Refresh' Content='0; url=./index.php'>"; 
        die();
    }
}
        ?>
        <div id="custom-target"></div>
      </div>
    </div>
  </div>
</div>
</form>
</div>
  </head>