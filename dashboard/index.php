<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
require ('../connection/config.php');
if (!isset($_SESSION['user_data']))
{
    echo '<div class="alert alert-dark" role="alert">
    Error!
  </div>';
  echo "<meta http-equiv='Refresh' Content='0; url= ../index.php'>"; 
  die();
}
if (isset($_POST['signout']))
{
    session_unset();
    session_destroy();
    echo "<meta http-equiv='Refresh' Content='0; url= ../index.php'>"; 
    die();
}
else if (isset($_POST['savestyling']))
{
  $formatgiven = $_POST['flexRadioDefault'];
  $stylingsave = $database->prepare("UPDATE users SET DATEFORMAT = :dateformat WHERE EMAIL = :email;");
  $stylingsave->bindParam(":dateformat",$formatgiven);
  $stylingsave->bindParam(":email",$_SESSION['user_data']->EMAIL);
  $stylingsave->execute();
  $checkE = $database->prepare("SELECT * FROM users WHERE EMAIL = :email AND PASSWORD = :password");
  $checkE->bindParam(":email",$_SESSION['user_data']->EMAIL);
  $checkE->bindParam(":password",$_SESSION['user_data']->PASSWORD);
  $checkE->execute();
    $user = $checkE->fetchObject();
    $_SESSION['user_data'] = $user;
  echo "<meta http-equiv='Refresh' Content='0; url=.'>"; 
  die();
}
else if (isset($_POST['regenacckey']))
{
  function randString($length = 18) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}
$appkeyforfunc = $_SESSION['app_data']->APPKEY;
  $randFunction = randString();
  $regendaccKeyKeys = $database->prepare("UPDATE userskeys SET APPOWNERKEY = :newkeyaccgen WHERE APPKEY = :appkey;");
  $regendaccKeyKeys->bindParam(":newkeyaccgen",$randFunction);
  $regendaccKeyKeys->bindParam(":appkey",$appkeyforfunc);
  if($regendaccKeyKeys->execute())
  {
    $regendaccKeyAccs = $database->prepare("UPDATE accounts SET APPOWNERKEY = :newkeyaccgen WHERE APPKEY = :appkey;");
    $regendaccKeyAccs->bindParam(":newkeyaccgen",$randFunction);
    $regendaccKeyAccs->bindParam(":appkey",$appkeyforfunc);
    if($regendaccKeyAccs->execute())
    {
      $regendaccVars = $database->prepare("UPDATE vars SET APPOWNERKEY = :newkeyaccgen WHERE APPKEY = :appkey;");
      $regendaccVars->bindParam(":newkeyaccgen",$randFunction);
      $regendaccVars->bindParam(":appkey",$appkeyforfunc);
      if($regendaccVars->execute())
      {
        $regendaccBans = $database->prepare("UPDATE blacklisted SET APPOWNERKEY = :newkeyaccgen WHERE APPKEY = :appkey;");
        $regendaccBans->bindParam(":newkeyaccgen",$randFunction);
        $regendaccBans->bindParam(":appkey",$appkeyforfunc);
        if($regendaccBans->execute())
        {
          $regendaccRoot = $database->prepare("UPDATE rootpanel SET APPOWNERKEY = :newkeyaccgen WHERE APPKEY = :appkey;");
          $regendaccRoot->bindParam(":newkeyaccgen",$randFunction);
          $regendaccRoot->bindParam(":appkey",$appkeyforfunc);
          if($regendaccRoot->execute())
          {
            $regendaccReq = $database->prepare("UPDATE req SET APPOWNERKEY = :newkeyaccgen WHERE APPKEY = :appkey;");
            $regendaccReq->bindParam(":newkeyaccgen",$randFunction);
            $regendaccReq->bindParam(":appkey",$appkeyforfunc);
            $regendaccReq->execute();
  $regendaccKey = $database->prepare("UPDATE users SET ACCOUNTKEY = :newkeyaccgen WHERE NAME = :name AND EMAIL = :email;");
  $regendaccKey->bindParam(":newkeyaccgen",$randFunction);
  $regendaccKey->bindParam(":name",$_SESSION['user_data']->NAME);
  $regendaccKey->bindParam(":email",$_SESSION['user_data']->EMAIL);
  if($regendaccKey->execute())
  {
    $checkE = $database->prepare("SELECT * FROM users WHERE EMAIL = :email AND PASSWORD = :password");
    $checkE->bindParam(":email",$_SESSION['user_data']->EMAIL);
    $checkE->bindParam(":password",$_SESSION['user_data']->PASSWORD);
    $checkE->execute();
      $user = $checkE->fetchObject();
      $_SESSION['user_data'] = $user;
      $appname = $database->prepare("SELECT * FROM apps WHERE APPOWNERID = :appownerid AND APPID = :appid AND APPUNID = :appunid");
$appname->bindParam("appownerid",$_SESSION['user_data']->ACCOUNTID);
$appname->bindParam("appid",$_SESSION['user_data']->CURRAPP);
$appname->bindParam("appunid",$_SESSION['user_data']->ID);
    $appnamef = $appname->fetchObject();
    $_SESSION['app_data'] = $appnamef;
      echo "<meta http-equiv='Refresh' Content='0; url=.'>"; 
      die();
  }
}
}
}
}
}
}
else if (isset($_POST['removeacc']))
{
  $deleteaccKeys = $database->prepare("DELETE FROM userskeys WHERE APPOWNERKEY = :appownerkey;");
  $deleteaccKeys->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
  if($deleteaccKeys->execute())
  {
    $deleteaccAccs = $database->prepare("DELETE FROM accounts WHERE APPOWNERKEY = :appownerkey;");
    $deleteaccAccs->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    if($deleteaccAccs->execute())
    {
  $removeApps = $database->prepare("DELETE FROM apps WHERE APPOWNERID = :appownerid");
  $removeApps->bindParam(":appownerid",$_SESSION['user_data']->ACCOUNTID);
  $removeApps->execute();
  $UniqueID = $_SESSION['user_data']->ID;
  $removeAKeys = $database->prepare("DELETE FROM userskeys WHERE APPOWNERKEY = :appownerkey");
  $removeAKeys->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
  $removeAKeys->execute();
  $removeARoot = $database->prepare("DELETE FROM rootpanel WHERE APPOWNERKEY = :appownerkey");
  $removeARoot->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
  $removeARoot->execute();
  $removeAVars = $database->prepare("DELETE FROM vars WHERE APPOWNERKEY = :appownerkey");
  $removeAVars->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
  $removeAVars->execute();
  $removeABLK = $database->prepare("DELETE FROM blacklisted WHERE APPOWNERKEY = :appownerkey");
  $removeABLK->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
  $removeABLK->execute();
  $removeAccount = $database->prepare("DELETE FROM users WHERE ID = :id AND ACCOUNTID = :accountid");
  $removeAccount->bindParam(":id",$UniqueID);
  $removeAccount->bindParam(":accountid",$_SESSION['user_data']->ACCOUNTID);
  $removeAccount->execute();
  session_unset();
  session_destroy();
  echo "<meta http-equiv='Refresh' Content='0; url= ../index.php'>"; 
  die();
  }
}
}
else if (isset($_POST['changepass']))
{
  if (hash($Secure_Hash_Algorithm,$_POST['currPass']) === $_SESSION['user_data']->PASSWORD)
  {
    if ($_POST['confnewPass'] === $_POST['newPass'])
    {
      $changePass = $database->prepare("UPDATE users SET PASSWORD = :password WHERE ID = :id AND ACCOUNTID = :accid;");
      $changePass->bindParam(":password",hash($Secure_Hash_Algorithm,$_POST['newPass']));
      $changePass->bindParam(":id",$_SESSION['user_data']->ID);
      $changePass->bindParam(":accid",$_SESSION['user_data']->ACCOUNTID);
      if ($changePass->execute())
      {
    $checkE = $database->prepare("SELECT * FROM users WHERE EMAIL = :email AND PASSWORD = :password");
    $checkE->bindParam(":email",$_SESSION['user_data']->EMAIL);
    $checkE->bindParam(":password",$_SESSION['user_data']->PASSWORD);
    $checkE->execute();
      $user = $checkE->fetchObject();
      $_SESSION['user_data'] = $user;
            echo "<meta http-equiv='Refresh' Content='0; url=.'>"; 
      die();
      }
    }
    else
    {
    }
  }
  else
  {
  }
}
?>
<head>
        <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    .form-control:disabled {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    </style>
</head>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="../index.php">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
          <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
            <?php if ($_SESSION['user_data']->RANK == 0)
  {
    echo '<li class="nav-item">
    <a class="nav-link" href="apps">Applications</a>
  </li>';
  }
  else
  {
    echo '<li class="nav-item">
    <a class="nav-link" href="premium/apps">Applications</a>
  </li>';
  }?>
            <li class="nav-item">
              <a class="nav-link" href="keys">Keys</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="users">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="variables">Variables</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="blacklisted">Blacklisted</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">Account</a>
            </li>
           
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="alert alert-danger" role="alert">
  NOTE: We do not allow anything related to cheating/hacks/malicious, If you're using our services to any of these types we suggest you deleting your account before we take any action by ourselves.
</div>
    <?php
    if ($_SESSION['user_data']->RANK === "1")
    {
        echo '<a class="btn btn-primary" href="../admin" role="button">Admin</a>';
    }
    ?>
<div class="container">
<form method="POST">
    <br>
    <div class="row">
        <div class="col-sm-2-lg-2">
          <div class="card text-white bg-dark">
            <div class="card-body">
            Account Name : <input class="form-control" placeholder="Account Name" value="<?php echo $_SESSION['user_data']->NAME; ?>" disabled readonly required/>
            Account Key : <input class="form-control" placeholder="Account ID" value="<?php echo $_SESSION['user_data']->ACCOUNTKEY; ?>" disabled readonly required/>
            Plan : <input class="form-control" placeholder="Rank" value="<?php
            function backROLE($numgiven)
            {
                if ($numgiven == "0")
                {
                    return "Starter";
                }
                else if ($numgiven == "2")
                {
                    return "Premium";
                }
                else
                {
                    return "Adminstrator";
                }
            }
            echo backROLE($_SESSION['user_data']->RANK);
            ?>" disabled readonly required/>
             <button class="btn btn-primary mt-3" type="submit" name="regenacckey">Re-generate Account Key</button> 
             <button class="btn btn-secondary mt-3" type="submit" name="removeacc">Delete Account</button> 
             <button class="btn btn-light mt-3" type="button" data-bs-toggle="modal" data-bs-target="#style">Styling</button>
             <button class="btn btn-success mt-3" type="submit" name="signout">Logout</button> 
            </div>
          </div>
        </div>
           </div>
        <br>
        <div class="col-sm-2-lg-2">
          <div class="card text-white bg-dark">
            <div class="card-body">
              Current Password : <input class="form-control" type="password" name="currPass" placeholder="Current Password"/>
              New Password : <input class="form-control" type="password" name="newPass" placeholder="New Password"/>
              Confirm New Password : <input class="form-control" type="password" name="confnewPass" placeholder="Confirm New Password"/>
              <button class="btn btn-danger mt-3" type="submit" name="changepass">Change Password</button> 
            </div>
          </div>
        </div>
        <br>
        <div class="modal fade" id="style" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Preferences</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      Date Format : <select name="flexRadioDefault" class="form-control" style="border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;">
                <option value="Y/m/d, h:i a">2000/01/01, 01:00</option>
                <option value="F j, Y, g:i a">December 1, 2000, 01:00</option>
                <option value="l jS \,  F Y">Friday 1st, December 2000</option>
                <option value="Y/m/d">2020/11/03</option>
              </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="savestyling">Save</button>
      </div>
    </div>
  </div>
</div>
      <div class="col-sm-2-lg-2">
          <div class="card text-white bg-dark">
            <div class="card-body">
                <p><strong>Invite friends and earn some extra money!</strong></p>
                <p>Everyone who joins Umar using your referral link and they subscribe to an Umar plan, you earn 7$ of that purchase!</p>
                <a>Referrals: <?php
                $v = $database->prepare("SELECT NULL FROM users WHERE REF = :ref");
                $v->bindParam(":ref", $_SESSION['user_data']->ACCOUNTID);
                $v->execute();
                $val = $v->rowCount();
                echo $val
                ?>
                </a>
                <p>Earnings: <?php
                function getEar($gv){
                    return $gv * 6.99;
                    
                }
                $valPR = "2";
                $gEarn = $database->prepare("SELECT NULL FROM users WHERE REF = :ref AND RANK = :ranky");
                $gEarn->bindParam(":ref", $_SESSION['user_data']->ACCOUNTID);
                $gEarn->bindParam(":ranky", $valPR);
                $gEarn->execute();
                $gEVal = $gEarn->rowCount();
                echo getEar($gEVal), "$"
                ?>
                </p>
            Your referral link : <input class="form-control" placeholder="what are you doing here" id="myInput" value="<?php echo $Host_Name, "/identify?r=", $_SESSION['user_data']->ACCOUNTID; ?>" disabled readonly required/>
             <button class="btn btn-outline-warning mt-3" type="submit" onclick="myFunction()" name="copyref">Copy</button> 
            </div>
          </div>
        </div>
 </form>
 <script>
          function myFunction() {
  var copyText = document.getElementById("myInput");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  navigator.clipboard.writeText(copyText.value);
}
        </script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
</div>