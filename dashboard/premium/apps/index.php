<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
require ('../../../connection/config.php');
if (!isset($_SESSION['user_data']))
{
    echo '<div class="alert alert-dark" role="alert">
    User data not found!
  </div>';
  echo "<meta http-equiv='Refresh' Content='0; url= ../../../index.php'>"; 
  die();
}
if ($_SESSION['user_data']->RANK === "1")
{
}
if ($_SESSION['user_data']->RANK === "0")
{
echo "<meta http-equiv='Refresh' Content='0; url=.'>"; 
die();
}
if ($_SESSION['user_data']->APPS === "0")
{
echo "<meta http-equiv='Refresh' Content='0; url= ../newapps'>"; 
die();
}
if ($_SESSION['user_data']->CURRAPP === "0")
{
echo "<meta http-equiv='Refresh' Content='0; url= ../manage'>"; 
die();
}
$appname = $database->prepare("SELECT * FROM apps WHERE APPOWNERID = :appownerid AND APPID = :appid AND APPUNID = :appunid");
$appname->bindParam("appownerid",$_SESSION['user_data']->ACCOUNTID);
$appname->bindParam("appid",$_SESSION['user_data']->CURRAPP);
$appname->bindParam("appunid",$_SESSION['user_data']->ID);
if ($appname->execute())
{
    $appnamef = $appname->fetchObject();
    $_SESSION['app_data'] = $appnamef;
}
$appkey = $_SESSION['app_data']->APPKEY;
$appownerkey = $_SESSION['user_data']->ACCOUNTKEY;
$rportal = $Host_Name.'/root/?app=' .$appkey. '&owner=' . $appownerkey;
if (isset($_POST['regenappid']))
{
  $randFunction = rand(100000000000000000,900000000000000000);
              $regendaccRoot = $database->prepare("UPDATE rootpanel SET CURRAPP = :newkeyaccgen WHERE CURRAPP = :currapp;");
          $regendaccRoot->bindParam(":newkeyaccgen",$randFunction);
          $regendaccRoot->bindParam(":currapp",$_SESSION['user_data']->CURRAPP);
          if($regendaccRoot->execute())
          {
                $regendaccKey = $database->prepare("UPDATE apps SET APPID = :newkeyaccgen WHERE APPID = :currapp;");
          $regendaccKey->bindParam(":newkeyaccgen",$randFunction);
          $regendaccKey->bindParam(":currapp",$_SESSION['user_data']->CURRAPP);
          if($regendaccKey->execute())
          {
  $regendaccKey = $database->prepare("UPDATE users SET CURRAPP = :newkeyaccgen WHERE CURRAPP = :currapp;");
          $regendaccKey->bindParam(":newkeyaccgen",$randFunction);
          $regendaccKey->bindParam(":currapp",$_SESSION['user_data']->CURRAPP);
          if($regendaccKey->execute())
          {
    $checkE = $database->prepare("SELECT * FROM users WHERE EMAIL = :email AND PASSWORD = :password");
    $checkE->bindParam(":email",$_SESSION['user_data']->EMAIL);
    $checkE->bindParam(":password",$_SESSION['user_data']->PASSWORD);
    $checkE->execute();
    if ($checkE->rowCount() === 1)
    {
      $user = $checkE->fetchObject();
      $_SESSION['user_data'] = $user;
      echo "<meta http-equiv='Refresh' Content='0; url= .'>"; 
      die();
    }
  }
  }
}
}
if (isset($_POST['regenappkey']))
{
  function randString($length = 18) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}
  $randFunction = randString();
  $regendaccKeyKeys = $database->prepare("UPDATE userskeys SET APPKEY = :newkeyaccgen WHERE APPOWNERKEY = :appownerkey;");
  $regendaccKeyKeys->bindParam(":newkeyaccgen",$randFunction);
  $regendaccKeyKeys->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
  if($regendaccKeyKeys->execute())
  {
    $regendaccKeyAccs = $database->prepare("UPDATE accounts SET APPKEY = :newkeyaccgen WHERE APPOWNERKEY = :appownerkey;");
    $regendaccKeyAccs->bindParam(":newkeyaccgen",$randFunction);
    $regendaccKeyAccs->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    if($regendaccKeyAccs->execute())
    {
      $regendaccVars = $database->prepare("UPDATE vars SET APPKEY = :newkeyaccgen WHERE APPOWNERKEY = :appownerkey;");
      $regendaccVars->bindParam(":newkeyaccgen",$randFunction);
      $regendaccVars->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
      if($regendaccVars->execute())
      {
        $regendaccBans = $database->prepare("UPDATE blacklisted SET APPKEY = :newkeyaccgen WHERE APPOWNERKEY = :appownerkey;");
        $regendaccBans->bindParam(":newkeyaccgen",$randFunction);
        $regendaccBans->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
        if($regendaccBans->execute())
        {
          $regendaccRoot = $database->prepare("UPDATE rootpanel SET APPKEY = :newkeyaccgen WHERE APPOWNERKEY = :appownerkey;");
          $regendaccRoot->bindParam(":newkeyaccgen",$randFunction);
          $regendaccRoot->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
          if($regendaccRoot->execute())
          {
            $regendaccReq = $database->prepare("UPDATE req SET APPKEY = :newkeyaccgen WHERE APPOWNERKEY = :appownerkey;");
            $regendaccReq->bindParam(":newkeyaccgen",$randFunction);
            $regendaccReq->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
            $regendaccReq->execute();
  $regendaccKey = $database->prepare("UPDATE apps SET APPKEY = :newkeyaccgen WHERE APPUNID = :appunid;");
  $regendaccKey->bindParam(":newkeyaccgen",$randFunction);
  $regendaccKey->bindParam(":appunid",$_SESSION['user_data']->ID);
  if($regendaccKey->execute())
  {
    $checkE = $database->prepare("SELECT * FROM users WHERE EMAIL = :email AND PASSWORD = :password");
    $checkE->bindParam(":email",$_SESSION['user_data']->EMAIL);
    $checkE->bindParam(":password",$_SESSION['user_data']->PASSWORD);
    $checkE->execute();
    if ($checkE->rowCount() === 1)
    {
      $user = $checkE->fetchObject();
      $_SESSION['user_data'] = $user;
      echo "<meta http-equiv='Refresh' Content='0; url= .'>"; 
      die();
    }
  }
}
}
}
}
}
}
if (isset($_POST['deleteapp']))
{
  $deleteaccAccs = $database->prepare("DELETE FROM accounts WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
  $deleteaccAccs->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
  $deleteaccAccs->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
  $deleteaccAccs->execute();
  $removeAKeys = $database->prepare("DELETE FROM userskeys WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
  $removeAKeys->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
  $removeAKeys->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
  $removeAKeys->execute();
  $removeARoot = $database->prepare("DELETE FROM rootpanel WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
  $removeARoot->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
  $removeARoot->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
  $removeARoot->execute();
  $removeAVars = $database->prepare("DELETE FROM vars WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
  $removeAVars->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
  $removeAVars->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
  $removeAVars->execute();
  $removeABLK = $database->prepare("DELETE FROM blacklisted WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
  $removeABLK->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
  $removeABLK->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
  $removeABLK->execute();
  $updatewApps = $database->prepare("UPDATE users SET APPS = APPS - 1 WHERE ID = :id");
  $updatewApps->bindParam(":id",$_SESSION['app_data']->APPUNID);
  $updatewApps->execute();
  $removeApps = $database->prepare("DELETE FROM apps WHERE APPID = :appid AND APPOWNERID = :appownerid");
  $removeApps->bindParam(":appid",$_SESSION['app_data']->APPID);
  $removeApps->bindParam(":appownerid",$_SESSION['user_data']->ACCOUNTID);
  $removeApps->execute();
  $removeDiscApps = $database->prepare("DELETE FROM discord WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
  $removeDiscApps->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
  $removeDiscApps->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
  $removeDiscApps->execute();
  $checkE = $database->prepare("SELECT * FROM users WHERE EMAIL = :email AND PASSWORD = :password");
  $checkE->bindParam(":email",$_SESSION['user_data']->EMAIL);
  $checkE->bindParam(":password",$_SESSION['user_data']->PASSWORD);
  $checkE->execute();
  if ($checkE->rowCount() === 1)
  {
    $user = $checkE->fetchObject();
    $_SESSION['user_data'] = $user;
    echo "<meta http-equiv='Refresh' Content='0; url= .'>"; 
    die();
  }
}
if (isset($_POST['savechangesapp']))
{
  if (isset($_POST['inlineRadioOptions']))
  {
    $updateApps = $database->prepare("UPDATE apps SET STATUS = :option WHERE APPUNID = :id");
    $updateApps->bindParam(":option",$_POST['inlineRadioOptions']);
    $updateApps->bindParam(":id",$_SESSION['user_data']->ID);
    $updateApps->execute();
  }
  if (isset($_POST['inlineRadioOptions2']))
  {
    $updateHApps = $database->prepare("UPDATE apps SET HWID = :option WHERE APPUNID = :id");
    $updateHApps->bindParam(":option",$_POST['inlineRadioOptions2']);
    $updateHApps->bindParam(":id",$_SESSION['user_data']->ID);
    $updateHApps->execute();
  }
  if (isset($_POST['inlineRadioOptions3']))
  {
    $updateHApps = $database->prepare("UPDATE apps SET DISC = :option WHERE APPUNID = :id");
    $updateHApps->bindParam(":option",$_POST['inlineRadioOptions3']);
    $updateHApps->bindParam(":id",$_SESSION['user_data']->ID);
    $updateHApps->execute();
  }
  $updateApps = $database->prepare("UPDATE apps SET DISCWH = :discwh, LOGGED = :logged, REGISTERED = :registered, PAUSED = :paused WHERE APPID = :id");
  $updateApps->bindParam(":discwh",$_POST['webhookurl']);
  $updateApps->bindParam(":logged",$_POST['loggedmessage']);
  $updateApps->bindParam(":registered",$_POST['registeredmessage']);
  $updateApps->bindParam(":paused",$_POST['pausedmessage']);
  $updateApps->bindParam(":id",$_SESSION['user_data']->CURRAPP);
  if ($updateApps->execute())
  {
    echo "<meta http-equiv='Refresh' Content='0; url= .'>"; 
    die();
  }
  else
  {
  }
}
if (isset($_POST['resetchangesapp']))
{
    $updateAppsStatus = $database->prepare("UPDATE apps SET STATUS = 1 WHERE APPUNID = :id");
    $updateAppsStatus->bindParam(":id",$_SESSION['user_data']->ID);
    $updateAppsStatus->execute();
    $updateAppsHwid = $database->prepare("UPDATE apps SET HWID = 1 WHERE APPUNID = :id");
    $updateAppsHwid->bindParam(":id",$_SESSION['user_data']->ID);
    $updateAppsHwid->execute();
    $updateAppsVarChars = $database->prepare("UPDATE apps SET DISCWH = :discwh, LOGGED = :logged, REGISTERED = :registered, PAUSED = :paused WHERE APPID = :id");
    $updateAppsVarChars->bindParam(":discwh",$Default_Discord_Webhook);
    $updateAppsVarChars->bindParam(":logged",$Default_Login_Message);
    $updateAppsVarChars->bindParam(":registered",$Default_Register_Message);
    $updateAppsVarChars->bindParam(":paused",$Default_Pause_Message);
    $updateAppsVarChars->bindParam(":id",$_SESSION['user_data']->CURRAPP);
    if ($updateAppsVarChars->execute())
    {
      echo "<meta http-equiv='Refresh' Content='0; url= .'>"; 
      die();
    }
    else
    {
    }
}

if (isset($_POST['adduser']))
{
  $Username = $_POST['rootname'];
  if (preg_match('/[A-Za-z]/',$Username))
  {
    $Password = $_POST['rootpass'];
    if (preg_match('/[A-Za-z]/',$Password))
    {
      $checkN = $database->prepare("SELECT NULL FROM rootpanel WHERE NAME = :NAME");
      $checkN->bindParam("NAME",$Username);
      $checkN->execute();
      if ($checkN->rowCount() > 0)
      {
      }
      else
      {
        $addUser = $database->prepare("INSERT INTO rootpanel(NAME,PASSWORD,CREATEDATE,APPKEY,APPOWNERKEY,CURRAPP) VALUES(:NAME,:PASSWORD,:CREATEDATE,:APPKEY,:APPOWNERKEY,:CURRAPP)");
        $addUser->bindParam("NAME",$Username);
        $addUser->bindParam("PASSWORD",hash($Secure_Hash_Algorithm,$Password));
        $addUser->bindParam("CREATEDATE",date("Y/m/d"));
        $addUser->bindParam("APPKEY",$_SESSION['app_data']->APPKEY);
        $addUser->bindParam("APPOWNERKEY",$_SESSION['user_data']->ACCOUNTKEY);
        $addUser->bindParam("CURRAPP",$_SESSION['app_data']->APPID);
        if ($addUser->execute())
        {
        }
       else
        {
        }
      }
    }
  }
}
if (isset($_POST['deleteroot']))
{
    $DeletedUser = $_POST['deleteroot'];
    $removeuser = $database->prepare("DELETE FROM rootpanel WHERE NAME = :name AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey AND CURRAPP = :currapp");
    $removeuser->bindParam("name",$DeletedUser);    
    $removeuser->bindParam("appkey",$_SESSION['app_data']->APPKEY);
        $removeuser->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
        $removeuser->bindParam("currapp",$_SESSION['user_data']->CURRAPP);
        if ($removeuser->execute())
        {
        }
        else
        {
        }
}
?>
  <head>
    <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    .form-control:disabled {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    </style>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="../../../index.php">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item">
              <a class="nav-link" href=".">Applications</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../../keys">Keys</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../../users">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../../variables">Variables</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../../blacklisted">Blacklisted</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../..">Account</a>
            </li>
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">

  <form method="POST">
    <br>
    <div class="row">
        <div class="col-sm-2-lg-2">
          <div class="card text-white bg-dark">
            <div class="card-body">
              Application Name : <input class="form-control" placeholder="Application Name" value="<?php echo $_SESSION['app_data']->APPNAME; ?>" disabled readonly required/>
              Application Key : <input class="form-control" placeholder="Application Key" value="<?php echo $_SESSION['app_data']->APPKEY; ?>" disabled readonly required/>
              Application ID : <input class="form-control" placeholder="Application ID" value="<?php echo $_SESSION['app_data']->APPID; ?>" disabled readonly required/>
              <button class="btn btn-primary mt-3" type="submit" name="regenappkey">Re-generate Key</button>
              <button class="btn btn-secondary mt-3" type="submit" name="regenappid">Re-generate ID</button>
              <a href="../../discord"><button class="btn btn-success mt-3" type="button">Discord Management</button></a>
              <a href="../manage"><button class="btn btn-light mt-3" type="button">Change Application</button></a>
              <button class="btn btn-danger mt-3" type="submit" name="deleteapp">Delete Application</button> 
              <br>
            </div>
          </div>
          <br>
        </div>
        <br>
        <div class="col-sm-2-lg-2">
          <div class="card text-white bg-dark">
            <div class="card-body">
Application Status : <select name="inlineRadioOptions" class="form-control">
<?php if ($_SESSION['app_data']->STATUS == 1)
  {
    echo '<option value="1">Enabled</option>
    <option value="0">Disabled</option>';
  }
  else if ($_SESSION['app_data']->STATUS == 0)
  {
    echo '<option value="0">Disabled</option>
    <option value="1">Enabled</option>';
  }?>
              </select>
Hwid Lock : <select name="inlineRadioOptions2" class="form-control">
<?php if ($_SESSION['app_data']->HWID == 1)
  {
    echo '<option value="1">Enabled</option>
    <option value="0">Disabled</option>';
  }
  else if ($_SESSION['app_data']->HWID == 0)
  {
    echo '<option value="0">Disabled</option>
    <option value="1">Enabled</option>';
  }?>
              </select>
Discord Webhook Status : <select name="inlineRadioOptions3" class="form-control">
  <?php if ($_SESSION['app_data']->DISC == 1)
  {
    echo '<option value="1">Enabled</option>
    <option value="0">Disabled</option>';
  }
  else if ($_SESSION['app_data']->DISC == 0)
  {
    echo '<option value="0">Disabled</option>
    <option value="1">Enabled</option>';
  }?>
              </select>
              Discord Webhook : <input class="form-control" placeholder="Discord Webhook" name="webhookurl" value="<?php echo $_SESSION['app_data']->DISCWH; ?>" />
              Successful login message : <input class="form-control" placeholder="Successful login message" name="loggedmessage" value="<?php echo $_SESSION['app_data']->LOGGED; ?>" />
              Successful register message : <input class="form-control" placeholder="Successful register message" name="registeredmessage" value="<?php echo $_SESSION['app_data']->REGISTERED; ?>" />
              Disabled Application message : <input class="form-control" placeholder="Paused Application message" name="pausedmessage" value="<?php echo $_SESSION['app_data']->PAUSED; ?>"/>
              <div class="d-grid gap-2 d-md-flex">
                <button class="btn btn-danger mt-3" type="submit" name="savechangesapp">Save Changes</button> 
                <button class="btn btn-light mt-3" type="submit" name="resetchangesapp">Reset</button> 
              </div>
            </div>
          </div>
        </div>
    </div>
    <br>
        <div class="col-sm-2-lg-2">
          <div class="card text-white bg-dark">
            <div class="card-body">
              Application Root Portal : <input class="form-control" id="myInput" placeholder="Root Portal" value="<?php echo $rportal ?>" disabled required/>
              <button class="btn btn-outline-info mt-3" onclick="myFunction()">Copy URL</button>
            </div>
          </div>
        </div>
        <br>
        <div class="col-sm-2-lg-2">
          <div class="card text-white bg-dark">
            <div class="card-body">
              Root Username : <input class="form-control" name="rootname"/>
              Root Password : <input class="form-control" name="rootpass"/>
              <button class="btn btn-outline-warning mt-3" type="submit" name="adduser">Add</button>
            </div>
          </div>
        </div>
 </form>
 <form method="POST">
 <div class="card text-white bg-dark">
<div class="card-body">
 <table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Root Username</th>
      <th scope="col">Create Date</th>
      <th scope="col">Management</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $appkeysre = $_SESSION['app_data']->APPKEY;
    $acckeysre = $_SESSION['user_data']->ACCOUNTKEY;
    $sqlResult = $database->prepare("SELECT NAME, CREATEDATE FROM rootpanel WHERE APPOWNERKEY = :appownerkey AND APPKEY = :appkey");
    $sqlResult->bindParam("appkey",$appkeysre);
    $sqlResult->bindParam("appownerkey",$acckeysre);
    $sqlResult->execute();
      foreach($sqlResult AS $result)
      {
        $username = $result['NAME'];
        $createdate = $result['CREATEDATE'];
        echo '<tr>
        <th scope="row">'. $username .'</th>
        <td>'. $createdate .'</td>
        <td>
        <button class="btn btn-danger" type="submit" value='.$username.' name="deleteroot">Delete</button>
        </td>
      </tr>';
      }
    ?>
  </tbody>
</table>
</div>
 </div>
</form>
<script>
          function myFunction() {
  var copyText = document.getElementById("myInput");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  navigator.clipboard.writeText(copyText.value);
}
        </script>
</div>
  </head>