<?php

use LDAP\Result;

session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
require ('../../../connection/config.php');
if (!isset($_SESSION['user_data']))
{
    echo '<div class="alert alert-dark" role="alert">
    Error!
  </div>';
  echo "<meta http-equiv='Refresh' Content='0; url= ../../../index.php'>"; 
  die();
}
if ($_SESSION['user_data']->RANK === "0")
{
echo "<meta http-equiv='Refresh' Content='0; url= ../../newapps'>"; 
die();
}
if ($_SESSION['user_data']->APPS === "0")
{
echo "<meta http-equiv='Refresh' Content='0; url= ../../premium/newapps'>"; 
die();
}
if (isset($_POST['continueapp']))
{
    $AppID = $_POST['continueapp'];
    $checkA = $database->prepare("SELECT NULL FROM apps WHERE APPID = :appid AND APPUNID = :appunid AND APPOWNERID = :appownerid");
    $checkA->bindParam(":appid",$AppID);
    $checkA->bindParam(":appunid",$_SESSION['user_data']->ID);
    $checkA->bindParam(":appownerid",$_SESSION['user_data']->ACCOUNTID);
    $checkA->execute();
    if ($checkA->rowCount() === 1)
    {
    $checkApp = $database->prepare("UPDATE users SET CURRAPP = :currapp WHERE ACCOUNTID = :accid AND ID = :id;");
    $checkApp->bindParam(":currapp",$AppID);
    $checkApp->bindParam(":id",$_SESSION['user_data']->ID);
    $checkApp->bindParam(":accid",$_SESSION['user_data']->ACCOUNTID);
    if ($checkApp->execute())
    {
            $checkE = $database->prepare("SELECT * FROM users WHERE EMAIL = :email AND PASSWORD = :password");
    $checkE->bindParam(":email",$_SESSION['user_data']->EMAIL);
    $checkE->bindParam(":password",$_SESSION['user_data']->PASSWORD);
    $checkE->execute();
    if ($checkE->rowCount() === 1)
    {
      $user = $checkE->fetchObject();
      $_SESSION['user_data'] = $user;
    }
    }
  }
  else
  {
  }
    echo "<meta http-equiv='Refresh' Content='0; url= ../apps'>"; 
  die();
}
if (isset($_POST['createapp']))
{
  $AppName = $_POST['AppName'];
  $checkName = $database->prepare("SELECT NULL FROM apps WHERE APPNAME = :appname");
  $checkName->bindParam("appname",$AppName);
  $checkName->execute();
  if ($checkName->rowCount() > 0)
  {
  }
  else
  {
    if (preg_match('/[A-Za-z]/',$AppName))
    {
    function randString($length = 18) 
    {
      return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
  }
    $randKey = randString();
    $randNumber = rand(100000000000000000,900000000000000000);
    $randFunction = $randNumber;
    $AppName = $_POST['AppName'];
      $addApp = $database->prepare("INSERT INTO apps(APPNAME,APPOWNERID,APPID,APPKEY,APPUNID) VALUES(:appname,:appownerid,:appid,:appkey,:appunid)");
      $addApp->bindParam("appname",$AppName);
      $addApp->bindParam("appownerid",$_SESSION['user_data']->ACCOUNTID);
      $addApp->bindParam("appid",$randFunction);
      $addApp->bindParam("appkey",$randKey);
      $addApp->bindParam("appunid",$_SESSION['user_data']->ID);
      if (preg_match('/[A-Za-z]/',$AppName))
      {
      if ($addApp->execute())
      {
        $updateApps = $database->prepare("UPDATE users SET APPS = APPS + 1 WHERE ID = :id");
        $updateApps->bindParam(":id",$_SESSION['user_data']->ID);
        if ($updateApps->execute())
        {
          $updateCurrApp = $database->prepare("UPDATE users SET CURRAPP = :appid WHERE ID = :id AND ACCOUNTID = :accid");
          $updateCurrApp->bindParam("appid",$randFunction);
          $updateCurrApp->bindParam(":id",$_SESSION['user_data']->ID);
          $updateCurrApp->bindParam(":accid",$_SESSION['user_data']->ACCOUNTID);
          if ($updateCurrApp->execute())
          {
          }
        }
        else
        {
        }
      }
    }
      else
      {
      }
    }
    else
    {
    }
  }
}
if (isset($_POST['deleteapp']))
{
    $currappkeyso = $_SESSION['app_data']->APPKEY;
    $accidso = $_SESSION['user_data']->ACCOUNTKEY;
  $AppKey = $_POST['deleteapp'];
  if ($AppKey == $currappkeyso)
  {
    $updateKeys = $database->prepare("DELETE FROM userskeys WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $updateKeys->bindParam(":appkey",$AppKey);
    $updateKeys->bindParam(":appownerkey", $accidso);
    if ($updateKeys->execute())
    {
    $updateAccs = $database->prepare("DELETE FROM accounts WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $updateAccs->bindParam(":appkey",$AppKey);
    $updateAccs->bindParam(":appownerkey", $accidso);
    if ($updateAccs->execute())
    {
      $updateApps = $database->prepare("UPDATE users SET APPS = APPS - 1 WHERE ID = :id");
      $updateApps->bindParam(":id",$_SESSION['user_data']->ID);
      if ($updateApps->execute())
      {
      $removeApp = $database->prepare("DELETE FROM apps WHERE APPKEY = :appkey AND APPOWNERID = :appownerid");
      $removeApp->bindParam(":appkey",$AppKey);
      $removeApp->bindParam(":appownerid",$_SESSION['user_data']->ACCOUNTID);
      if ($removeApp->execute())
      {
        $updateApps = $database->prepare("UPDATE users SET CURRAPP = 0 WHERE ID = :id");
        $updateApps->bindParam(":id",$_SESSION['user_data']->ID);
        if ($updateApps->execute())
        {
                $checkE = $database->prepare("SELECT * FROM users WHERE EMAIL = :email AND PASSWORD = :password");
    $checkE->bindParam(":email",$_SESSION['user_data']->EMAIL);
    $checkE->bindParam(":password",$_SESSION['user_data']->PASSWORD);
    $checkE->execute();
    if ($checkE->rowCount() === 1)
    {
      $user = $checkE->fetchObject();
      $_SESSION['user_data'] = $user;
    }
      }
    }
  }
    }
  }
  }
  else
  {
    $updateKeys = $database->prepare("DELETE FROM userskeys WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $updateKeys->bindParam(":appkey",$AppKey);
    $updateKeys->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    if ($updateKeys->execute())
    {
    $updateAccs = $database->prepare("DELETE FROM accounts WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $updateAccs->bindParam(":appkey",$AppKey);
    $updateAccs->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    if ($updateAccs->execute())
    {
      $updateApps = $database->prepare("UPDATE users SET APPS = APPS - 1 WHERE ID = :id");
      $updateApps->bindParam(":id",$_SESSION['user_data']->ID);
      if ($updateApps->execute())
      {
      $removeApp = $database->prepare("DELETE FROM apps WHERE APPKEY = :appkey AND APPOWNERID = :appownerid");
      $removeApp->bindParam(":appkey",$AppKey);
      $removeApp->bindParam(":appownerid",$_SESSION['user_data']->ACCOUNTID);
      if ($removeApp->execute())
      {
    }
  }
    }
  }
  }
}
?>
  <head>
    <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    .form-control:disabled {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    </style>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href=" ../../../index.php">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
          <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item">
              <a class="nav-link" href=" ../../premium/apps">Applications</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../keys">Keys</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../users">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../variables">Variables</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../blacklisted">Blacklisted</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../..">Account</a>
            </li>
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">
    <br>
<form method="POST">
<div class="col-sm-7">
<div class="card text-white bg-dark">
            <div class="card-body">
            <input class="form-control mt-3" placeholder="Application Name" type="text" name="AppName"/>
            <button class="btn btn-primary mt-3" type="submit" name="createapp">Create</button> 
            </div>
          </div>
        </div>
        <br>
        <div class="card text-white bg-dark">
<div class="card-body">
 <table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Application Name</th>
      <th scope="col">Application Key</th>
      <th scope="col">Management</th>
    </tr>
  </thead>
  <tbody>
<?php
$sqlReq = $database->prepare("SELECT APPNAME, APPKEY, APPID FROM apps WHERE APPOWNERID = :appownerid AND APPUNID = :appunid");
$sqlReq->bindParam("appownerid",$_SESSION['user_data']->ACCOUNTID);
$sqlReq->bindParam("appunid",$_SESSION['user_data']->ID);
$sqlReq->execute();
foreach($sqlReq AS $result)
{
  $appname = $result['APPNAME'];
  $appkey = $result['APPKEY'];
  $appid = $result['APPID'];
  echo '<tr>
  <th scope="row">'. $appname .'</th>
  <td>'. $appkey .'</td>
  <td>
  <button class="btn btn-primary" type="submit" value='.$appid.' name="continueapp">Continue</button>
  <button class="btn btn-primary" type="submit" value='.$appkey.' name="deleteapp">Delete</button>
  </td>
</tr>';
}
?>
  </tbody>
</table>
</div>
</div>
</form>
<br>
</div>
  </head>