<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
require ('../../../connection/config.php');
if (!isset($_SESSION['user_data']))
{
    echo '<div class="alert alert-dark" role="alert">
    Error!
  </div>';
  echo "<meta http-equiv='Refresh' Content='0; url= ../..'>"; 
  die();
}
if ($_SESSION['user_data']->RANK == "0")
{
echo "<meta http-equiv='Refresh' Content='0; url= ../../newapps'>"; 
die();
}
if ($_SESSION['user_data']->APPS !== "0")
{
echo "<meta http-equiv='Refresh' Content='0; url= ../apps'>"; 
die();
}
if (isset($_POST['createapp']))
{
  $AppName = $_POST['AppName'];
  $checkName = $database->prepare("SELECT NULL FROM apps WHERE APPNAME = :appname");
  $checkName->bindParam("appname",$AppName);
  $checkName->execute();
  if ($checkName->rowCount() > 0)
  {
  }
  else
  {
    if (preg_match('/[A-Za-z]/',$AppName))
    {
    function randString($length = 18) 
    {
      return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
  }
    $randKey = randString();
    $randNumber = rand(100000000000000000,900000000000000000);
    $randFunction = $randNumber;
    $AppName = $_POST['AppName'];
      $addApp = $database->prepare("INSERT INTO apps(APPNAME,APPOWNERID,APPID,APPKEY,APPUNID) VALUES(:appname,:appownerid,:appid,:appkey,:appunid)");
      $addApp->bindParam("appname",$AppName);
      $addApp->bindParam("appownerid",$_SESSION['user_data']->ACCOUNTID);
      $addApp->bindParam("appid",$randFunction);
      $addApp->bindParam("appkey",$randKey);
      $addApp->bindParam("appunid",$_SESSION['user_data']->ID);
      if (preg_match('/[A-Za-z]/',$AppName))
      {
      if ($addApp->execute())
      {
        $updateApps = $database->prepare("UPDATE users SET APPS = APPS + 1 WHERE ID = :id");
        $updateApps->bindParam(":id",$_SESSION['user_data']->ID);
        if ($updateApps->execute())
        {
          $updateCurrApp = $database->prepare("UPDATE users SET CURRAPP = :appid WHERE ID = :id AND ACCOUNTID = :accid");
          $updateCurrApp->bindParam("appid",$randFunction);
          $updateCurrApp->bindParam(":id",$_SESSION['user_data']->ID);
          $updateCurrApp->bindParam(":accid",$_SESSION['user_data']->ACCOUNTID);
          if ($updateCurrApp->execute())
          {
                  $checkE = $database->prepare("SELECT * FROM users WHERE EMAIL = :email AND PASSWORD = :password");
    $checkE->bindParam(":email",$_SESSION['user_data']->EMAIL);
    $checkE->bindParam(":password",$_SESSION['user_data']->PASSWORD);
    $checkE->execute();
    if ($checkE->rowCount() === 1)
    {
      $user = $checkE->fetchObject();
      $_SESSION['user_data'] = $user;
          echo "<meta http-equiv='Refresh' Content='0; url= .'>"; 
    die();
    }
          }
        }
        else
        {
        }
      }
    }
      else
      {
      }
    }
    else
    {
    }
  }
}
?>
<head>
            
        <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    .form-control:disabled {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    </style>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href=" ../..">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
          <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/apps">Applications</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard">Account</a>
            </li>
           
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">
<form method="POST">
    <br>
    <div class="card text-white bg-dark" style="width: 18rem;">
  <class="card-img-top" alt="...">
  <div class="card-body">
    <div class="d-grid gap-2">
    <input class="form-control mt-3" placeholder="Application Name" type="text" name="AppName" required/>
    <button class="btn btn-primary" type="submit" name="createapp">Create</button> 
</div>
  </div>
</div>
</form>
</div>
</head>