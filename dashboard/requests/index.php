<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
require ('../../connection/config.php');
if (!isset($_SESSION['user_data']))
{
    echo '<div class="alert alert-dark" role="alert">
    Error!
  </div>';
  echo "<meta http-equiv='Refresh' Content='0; url= ../..'>"; 
  die();
}
?>
<head>
        <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    .form-control:disabled {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    </style>
</head>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href=" ../..">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
          <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
            <?php if ($_SESSION['user_data']->RANK == 0)
  {
    echo '<li class="nav-item">
    <a class="nav-link" href=" ../../dashboard/apps">Applications</a>
  </li>';
  }
  else
  {
    echo '<li class="nav-item">
    <a class="nav-link" href=" ../../dashboard/premium/apps">Applications</a>
  </li>';
  }?>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/keys">Keys</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/users">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/variables">Variables</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/blacklisted">Blacklisted</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard">Account</a>
            </li>
           
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
</div>
<br>
    <div class="card text-white bg-dark">
<div class="card-body">
 <table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Type</th>
      <th scope="col">Date</th>
    </tr>
  </thead>
  <tbody>
<?php
$df = $_SESSION['user_data']->DATEFORMAT;
$appkey_sr = $_SESSION['app_data']->APPKEY;
$acckey_sr = $_SESSION['user_data']->ACCOUNTKEY;
$sqlReq = $database->prepare("SELECT TYPE, DATE FROM req WHERE APPKEY = :APPKEY AND APPOWNERKEY = :APPOWNERKEY");
$sqlReq->bindParam(":APPKEY",$appkey_sr);
$sqlReq->bindParam(":APPOWNERKEY",$acckey_sr);
$sqlReq->execute();
foreach($sqlReq AS $result)
{
  $typeofs = $result['TYPE'];
  $dateofs = $result['DATE'];
  echo '<tr>
  <th>'.$typeofs.'</td>
  <th scope="row">'. date($df, $dateofs) .'</th>
  <td>
  </td>
</tr>';
}
?>
  </tbody>
</table>
</div>
    </div>