<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
require ('../../connection/config.php');
if (!isset($_SESSION['user_data']))
{
    echo '<div class="alert alert-dark alert-dismissible fade show" role="alert">
    User data not found!
  </div>';
  echo "<meta http-equiv='Refresh' Content='0; url= ../..'>"; 
  die();
}
if ($_SESSION['user_data']->APPS === "0")
{
echo "<meta http-equiv='Refresh' Content='0; url= ../../dashboard/apps'>"; 
die();
}
$appname = $database->prepare("SELECT * FROM apps WHERE APPOWNERID = :appownerid AND APPID = :appid AND APPUNID = :appunid");
$appname->bindParam("appownerid",$_SESSION['user_data']->ACCOUNTID);
$appname->bindParam("appid",$_SESSION['user_data']->CURRAPP);
$appname->bindParam("appunid",$_SESSION['user_data']->ID);
if ($appname->execute())
{
    $appnamef = $appname->fetchObject();
    $_SESSION['app_data'] = $appnamef;
} 
if (isset($_POST['adduserid']))
{
    $userid = $_POST['userid'];
    if (!empty($userid))
    {
        $appname = $database->prepare("SELECT NULL FROM discord WHERE USERID = :userid");
$appname->bindParam(":userid",$userid);
$appname->execute();
if ($appname->rowCount() == 0) {

  if (strlen($userid) == 18) { 

    if (ctype_digit($userid)) {

      $addKey = $database->prepare("INSERT INTO discord(USERID,APPKEY,APPOWNERKEY) VALUES(:USERID,:APPKEY,:APPOWNERKEY)");
      $addKey->bindParam("USERID",$userid);
      $addKey->bindParam("APPKEY",$_SESSION['app_data']->APPKEY);
      $addKey->bindParam("APPOWNERKEY",$_SESSION['user_data']->ACCOUNTKEY);
      $addKey->execute();
    }
  }
}
else {
  echo 'This user ID is already registered to an other app!';
}

    }
}
if (isset($_POST['deleteuserid']))
{
    $DeletedKey = $_POST['deleteuserid'];
    $checkK = $database->prepare("DELETE FROM discord WHERE USERID = :userid AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkK->bindParam(":userid",$DeletedKey);
    $checkK->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
    $checkK->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $checkK->execute();
}
if (isset($_POST['delall']))
{
    $aremovekey = $database->prepare("DELETE FROM discord WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $aremovekey->bindParam("appkey",$_SESSION['app_data']->APPKEY);
        $aremovekey->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
        $aremovekey->execute();
}
?>
<head>
            
        <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;
    }
    </style>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href=" ../..">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
          <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <?php if ($_SESSION['user_data']->RANK == 0)
  {
    echo '<li class="nav-item">
    <a class="nav-link" href=" ../../dashboard/apps">Applications</a>
  </li>';
  }
  else
  {
    echo '<li class="nav-item">
    <a class="nav-link" href=" ../../dashboard/premium/apps">Applications</a>
  </li>';
  }?>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/keys">Keys</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/users">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/variables">Variables</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/blacklisted">Blacklisted</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard">Account</a>
            </li>
           
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">
      <br> 
 <div class="col-sm-7">
<div class="card text-white bg-dark">
<div class="card-body">
 <div class="d-grid gap-2 d-md-block">
 <form method="POST">
 <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#newkey">Add new User ID</button>
 <button class="btn btn-secondary" type="submit" name="delall">Delete all User IDs</button>
 <a href="<?php echo $Discord_Bot_Invite?>"><button class="btn btn-success" type="button">Invite Discord Bot</button></a>
 </form>
 </div>
 </div>
 </div>
 </div>
 <br>
 <form method="POST">
 <div class="modal fade" id="newkey" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New User ID</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        User ID : <input class="form-control" placeholder="000000000000000000" name="userid" value=""/>
              <p> </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="adduserid">Add</button>
      </div>
    </div>
  </div>
</div>
<div class="card text-white bg-dark">
<div class="card-body">
 <table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">User ID</th>
      <th scope="col">Management</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $sqlResult = $database->prepare("SELECT USERID FROM discord WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $sqlResult->bindParam("appkey",$_SESSION['app_data']->APPKEY);
    $sqlResult->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $sqlResult->execute();
      foreach($sqlResult AS $result)
      {
        $userid = $result['USERID'];
        echo '<tr>
        <th scope="row">'. $userid .'</th>
        <td>
        <button class="btn btn-danger" type="submit" value='.$userid.' name="deleteuserid">Delete</button>
        </td>
      </tr>';
      }
    ?>
    
  </tbody>
</table>
</div>
</div>
</form>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
</div>