<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
require ('../../connection/config.php');
if (!isset($_SESSION['user_data']))
{
    echo '<div class="alert alert-dark alert-dismissible fade show" role="alert">
    User data not found!
  </div>';
  echo "<meta http-equiv='Refresh' Content='0; url= ../..'>"; 
  die();
}
if ($_SESSION['user_data']->APPS === "0")
{
echo "<meta http-equiv='Refresh' Content='0; url= ../../dashboard/apps'>"; 
die();
}
$appname = $database->prepare("SELECT * FROM apps WHERE APPOWNERID = :appownerid AND APPID = :appid AND APPUNID = :appunid");
$appname->bindParam("appownerid",$_SESSION['user_data']->ACCOUNTID);
$appname->bindParam("appid",$_SESSION['user_data']->CURRAPP);
$appname->bindParam("appunid",$_SESSION['user_data']->ID);
if ($appname->execute())
{
    $appnamef = $appname->fetchObject();
    $_SESSION['app_data'] = $appnamef;
} 
if (isset($_POST['genkey']))
{
  $oneday = "+1 Day";
  $threedays = "+3 Days";
  $oneweek = "+1 Week";
  $threeweeks = "+3 Weeks";
  $onemonth = "+1 Month";
  $threemonths = "+3 Months";
  $oneyear = "+1 Year";
  $threeyears = "+3 Years";
    $Rank = $_POST['Rank'];
    $Quantity = $_POST['Quantity'];
    $Quantity = intval(preg_replace('/\D/', '', $Quantity));
    $Length = $_POST['Length'];
    $Length = intval(preg_replace('/\D/', '', $Length));
    $USERexpireDATE = 'lifetime';
    if ($_POST['flexRadioDefault'] === "Lifetime" )
    {
      $USERexpireDATE = 'Lifetime';
    }
    else if ($_POST['flexRadioDefault'] === "3y" )
    {
      $USERexpireDATE = $threeyears;
    }
    else if ($_POST['flexRadioDefault'] === "1y" )
    {
      $USERexpireDATE = $oneyear;
    }
    else if ($_POST['flexRadioDefault'] === '3m' )
    {
      $USERexpireDATE = $threemonths;
    }
    else if ($_POST['flexRadioDefault'] === "1m" )
    {
      $USERexpireDATE = $onemonth;
    }
    else if ($_POST['flexRadioDefault'] === "3w" )
    {
      $USERexpireDATE = $threeweeks;
    }
    else if ($_POST['flexRadioDefault'] === "1w" )
    {
      $USERexpireDATE = $oneweek;
    }
    else if ($_POST['flexRadioDefault'] === "3d" )
    {
      $USERexpireDATE = $threedays;
    }
    else if ($_POST['flexRadioDefault'] === "1d" )
    {
      $USERexpireDATE = $oneday;
    }
    if (preg_match('/[A-Za-z]/',$Rank))
    {
      function randString() {
              $length = $_POST['Length'];
              $length = intval(preg_replace('/\D/', '', $length));
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
    if (intval($Quantity) > 0 && intval($Quantity) <= 100 && intval($Length) > 8 && intval($Length) < 28)
    {
    for ($x = 0; $x < intval($Quantity); $x++) {
        $addKey = $database->prepare("INSERT INTO userskeys(KEYID,RANKUSER,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:KEYID,:RANKUSER,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
        $addKey->bindParam("KEYID",randString());
        $addKey->bindParam("RANKUSER",$Rank);
        $addKey->bindParam("EXPIREDATE",$USERexpireDATE);
        $addKey->bindParam("APPKEY",$_SESSION['app_data']->APPKEY);
        $addKey->bindParam("APPOWNERKEY",$_SESSION['user_data']->ACCOUNTKEY);
        $addKey->execute();
    }
    }
    }
}
if (isset($_POST['deletekey']))
{
    $DeletedKey = $_POST['deletekey'];
    $checkK = $database->prepare("SELECT NULL FROM userskeys WHERE KEYID = :keyid AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkK->bindParam(":keyid",$DeletedKey);
    $checkK->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
    $checkK->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $checkK->execute();
    if ($checkK->rowCount() === 1)
    {
    $removekey = $database->prepare("DELETE FROM userskeys WHERE KEYID = :keyid AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $removekey->bindParam("keyid",$DeletedKey);    
    $removekey->bindParam("appkey",$_SESSION['app_data']->APPKEY);
        $removekey->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
        $removekey->execute();
    }
}
$statusun = 'UNAPPLIED';
if (isset($_POST['renewkey']))
{
    $sKey = $_POST['renewkey'];
    $checkK = $database->prepare("SELECT NULL FROM userskeys WHERE KEYID = :keyid AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkK->bindParam(":keyid",$sKey);
    $checkK->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
    $checkK->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $checkK->execute();
    if ($checkK->rowCount() === 1)
    {
    $removekey = $database->prepare("UPDATE userskeys SET STATUS = :status WHERE KEYID = :keyid AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $removekey->bindParam("status",$statusun);   
    $removekey->bindParam("keyid",$sKey);    
    $removekey->bindParam("appkey",$_SESSION['app_data']->APPKEY);
        $removekey->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
        $removekey->execute();
    }
}
if (isset($_POST['delall']))
{
    $aremovekey = $database->prepare("DELETE FROM userskeys WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $aremovekey->bindParam("appkey",$_SESSION['app_data']->APPKEY);
        $aremovekey->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
        $aremovekey->execute();
}
if (isset($_POST['delused']))
{
  $statusu = 'APPLIED';
    $uremovekey = $database->prepare("DELETE FROM userskeys WHERE STATUS = :status AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $uremovekey->bindParam("status",$statusu);
    $uremovekey->bindParam("appkey",$_SESSION['app_data']->APPKEY);
    $uremovekey->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $uremovekey->execute();
}
if (isset($_POST['delunused']))
{
  $statusun = 'UNAPPLIED';
    $uremovekey = $database->prepare("DELETE FROM userskeys WHERE STATUS = :status AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $uremovekey->bindParam("status",$statusun);
    $uremovekey->bindParam("appkey",$_SESSION['app_data']->APPKEY);
    $uremovekey->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $uremovekey->execute();
}
?>
<head>
            
        <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;
    }
    </style>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href=" ../..">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
          <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <?php if ($_SESSION['user_data']->RANK == 0)
  {
    echo '<li class="nav-item">
    <a class="nav-link" href=" ../../dashboard/apps">Applications</a>
  </li>';
  }
  else
  {
    echo '<li class="nav-item">
    <a class="nav-link" href=" ../../dashboard/premium/apps">Applications</a>
  </li>';
  }?>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/keys">Keys</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/users">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/variables">Variables</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/blacklisted">Blacklisted</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard">Account</a>
            </li>
           
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">
      <br> 
 <div class="col-sm-7">
<div class="card text-white bg-dark">
<div class="card-body">
 <div class="d-grid gap-2 d-md-block">
 <form method="POST">
 <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#newkey">Generate New key</button>
 <button class="btn btn-secondary" type="submit" name="delall">Delete all keys</button>
 <button class="btn btn-success" type="submit" name="delused">Delete used keys</button>
 <button class="btn btn-danger" type="submit" name="delunused">Delete unused keys</button>
 </form>
 <form method="POST" action="export.php">
 <input type="submit" name="Export" class="btn btn-light" value="Export" />
 </form>
 </div>
 </div>
 </div>
 </div>
 <br>
 <form method="POST">
 <div class="modal fade" id="newkey" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Generate New key</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Rank : <input class="form-control" placeholder="Key Rank" name="Rank" value="Customer"/>
              <p> </p>
        Length (min: 9, max: 27) : <input class="form-control" placeholder="Length" name="Length" value="9"/>
              <p> </p>
        Quantity (min: 1, max: 100) : <input class="form-control" placeholder="Quantity" name="Quantity" value="1"/>
              <p> </p>
        Expire Duration : <select name="flexRadioDefault" class="form-control">
                <option value="1d">1 Day</option>
                <option value="3d">3 Days</option>
                <option value="1w">1 Week</option>
                <option value="3w">3 Weeks</option>
                <option value="1m">1 Month</option>
                <option value="3m">3 Months</option>
                <option value="1y">1 Year</option>
                <option value="3y">3 Years</option>
                <option value="Lifetime">Lifetime</option>
              </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="genkey">Start</button>
      </div>
    </div>
  </div>
</div>
<div class="card text-white bg-dark">
<div class="card-body">
 <table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Key</th>
      <th scope="col">Rank</th>
      <th scope="col">Expire Duration</th>
      <th scope="col">Status</th>
      <th scope="col">Management</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $sqlResult = $database->prepare("SELECT KEYID, RANKUSER, EXPIREDATE, STATUS FROM userskeys WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $sqlResult->bindParam("appkey",$_SESSION['app_data']->APPKEY);
    $sqlResult->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $sqlResult->execute();
      foreach($sqlResult AS $result)
      {
        $keyid = $result['KEYID'];
        $rank = $result['RANKUSER'];
        $expiredate = $result['EXPIREDATE'];
        $expiredate = trim($expiredate, "+");
        $status = $result['STATUS'];
        echo '<tr>
        <th scope="row">'. $keyid .'</th>
        <td>'. $rank .'</td>
        <td>'. $expiredate .'</td>
        <td>'. $status .'</td>
        <td>
        <a href="./edit.php?k='.$keyid.'"class="btn btn-outline-info mt-1">Edit</a>
        <button class="btn btn-outline-warning" type="submit" value='.$keyid.' name="renewkey">Renew</button>
        <button class="btn btn-danger" type="submit" value='.$keyid.' name="deletekey">Delete</button>
        </td>
      </tr>';
      }
    ?>
    
  </tbody>
</table>
</div>
</div>
</form>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
</div>