<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
require ('../../connection/config.php');
if (!isset($_SESSION['user_data']))
{
    echo '<div class="alert alert-dark" role="alert">
    User data not found!
  </div>';
  echo "<meta http-equiv='Refresh' Content='0; url= ../..'>"; 
  die();
}
if ($_SESSION['user_data']->APPS === "0")
{
echo "<meta http-equiv='Refresh' Content='0; url= ../../dashboard/apps'>"; 
die();
}
$appname = $database->prepare("SELECT * FROM apps WHERE APPOWNERID = :appownerid AND APPID = :appid AND APPUNID = :appunid");
$appname->bindParam("appownerid",$_SESSION['user_data']->ACCOUNTID);
$appname->bindParam("appid",$_SESSION['user_data']->CURRAPP);
$appname->bindParam("appunid",$_SESSION['user_data']->ID);
if ($appname->execute())
{
    $appnamef = $appname->fetchObject();
    $_SESSION['app_data'] = $appnamef;
}
if (isset($_POST['continuesub']))
{
    $Choice = $_POST['flexRadioDefaultzrw'];
    $SubDays = $_POST['subdays'];
    $sqlResult = $database->prepare("SELECT EXPIREDATE, ID FROM accounts WHERE APPOWNERKEY = :appownerkey AND APPKEY = :appkey");    $sqlResult->bindParam("appkey",$_SESSION['app_data']->APPKEY);
    $sqlResult->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $sqlResult->bindParam("appkey",$_SESSION['app_data']->APPKEY);
    $sqlResult->execute();
      foreach($sqlResult AS $result)
      {
                 if ($result['EXPIREDATE'] !== "Lifetime")
                 {
          if ($Choice == "y")
          {
                        $id = $result['ID'];
        $expiredate = $result['EXPIREDATE'];
        $newexpire = strtotime("+".$SubDays." day", $expiredate);
            $changeDate = $database->prepare("UPDATE accounts SET EXPIREDATE = :expiredate WHERE ID = :id AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $changeDate->bindParam(":expiredate",$newexpire);
    $changeDate->bindParam(":id",$id);
    $changeDate->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
    $changeDate->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $changeDate->execute();
          }
          else
          {
                                      $id = $result['ID'];
        $expiredate = $result['EXPIREDATE'];
        $newexpire = strtotime("+".$SubDays." day", $expiredate);
        $expireW = preg_replace("/[^0-9.]/", "", $expiredate);
        $dateW = preg_replace("/[^0-9.]/", "", $timenow =date("Y/m/d"));
        if (intval($expireW) > intval($dateW))
        {
                        $changeDate = $database->prepare("UPDATE accounts SET EXPIREDATE = :expiredate WHERE ID = :id AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $changeDate->bindParam(":expiredate",$newexpire);
    $changeDate->bindParam(":id",$id);
    $changeDate->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
    $changeDate->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $changeDate->execute();
        }
          }
                               
                 }
      }
}
if (isset($_POST['adduser']))
{
    $Username = $_POST['Username'];
    $Password = $_POST['Password'];
    if (preg_match('/[A-Za-z]/',$Password))
    {
    $Rank = $_POST['Rank'];
    if (preg_match('/[A-Za-z]/',$Rank))
    {
    $checkName = $database->prepare("SELECT NULL FROM accounts WHERE NAME = :name AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkName->bindParam("name",$Username);
    $checkName->bindParam("appkey",$_SESSION['app_data']->APPKEY);
    $checkName->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $checkName->execute();
    if ($checkName->rowCount() > 0)
    {
  }
  else
  {
    $checkA = $database->prepare("SELECT NULL FROM accounts WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkA->bindParam("appkey",$_SESSION['app_data']->APPKEY);
    $checkA->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $checkA->execute();
    $timenow =time();
    if ($_SESSION['user_data']->RANK === "0")
    {
      $num = $checkA->rowCount();
      if ($num >= 100)
      {
        echo '<div class="alert alert-dark" role="alert">
        The Application has reached the maximum of users, it\'s time for an upgrade!
      </div>';
      }
      else
      {
        $oneday = strtotime('+1 day', time());
        $threedays = strtotime('+3 day', time());
        $oneweek = strtotime('+7 day', time());
        $threeweeks = strtotime('+21 day', time());
        $onemonth = strtotime('+1 month', time());
        $threemonths = strtotime('+3 month', time());
        $USERexpireDATE = 'lifetime';
        if ($_POST['flexRadioDefaultrrw'] === "Lifetime" )
        {
          $USERexpireDATE = 'Lifetime';
        }
        else if ($_POST['flexRadioDefaultrrw'] === '3m' )
        {
          $USERexpireDATE = $threemonths;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "1m" )
        {
          $USERexpireDATE = $onemonth;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "3w" )
        {
          $USERexpireDATE = $threeweeks;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "1w" )
        {
          $USERexpireDATE = $oneweek;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "3d" )
        {
          $USERexpireDATE = $threedays;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "1d" )
        {
          $USERexpireDATE = $oneday;
        }
        $Username = str_replace(' ', '', $Username);
        $Password = str_replace(' ', '', $Password);
        $addUser = $database->prepare("INSERT INTO accounts(NAME,PASSWORD,RANKUSER,CREATEDATE,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:NAME,:PASSWORD,:RANKUSER,:CREATEDATE,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
        $addUser->bindParam("NAME",$Username);
        $addUser->bindParam("PASSWORD",hash($Secure_Hash_Algorithm,$Password));
        $addUser->bindParam("RANKUSER",$Rank);
        $addUser->bindParam("CREATEDATE",$timenow);
        $addUser->bindParam("EXPIREDATE",$USERexpireDATE);
        $addUser->bindParam("APPKEY",$_SESSION['app_data']->APPKEY);
        $addUser->bindParam("APPOWNERKEY",$_SESSION['user_data']->ACCOUNTKEY);
        if ($addUser->execute())
        {
        }
        else
        {
        }
      }
    }
    else
    {
        $oneday = strtotime('+1 day', time());
        $threedays = strtotime('+3 day', time());
        $oneweek = strtotime('+7 day', time());
        $threeweeks = strtotime('+21 day', time());
        $onemonth = strtotime('+1 month', time());
        $threemonths = strtotime('+3 month', time());
        $oneyear = strtotime('+1 year', time());
        $threeyears = strtotime('+3 years', time());
        $USERexpireDATE = 'lifetime';
        if ($_POST['flexRadioDefaultrrw'] === "Lifetime" )
        {
          $USERexpireDATE = 'Lifetime';
        }
        else if ($_POST['flexRadioDefaultrrw'] === '3y' )
        {
          $USERexpireDATE = $threeyears;
        }
        else if ($_POST['flexRadioDefaultrrw'] === '1y' )
        {
          $USERexpireDATE = $oneyear;
        }
        else if ($_POST['flexRadioDefaultrrw'] === '3m' )
        {
          $USERexpireDATE = $threemonths;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "1m" )
        {
          $USERexpireDATE = $onemonth;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "3w" )
        {
          $USERexpireDATE = $threeweeks;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "1w" )
        {
          $USERexpireDATE = $oneweek;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "3d" )
        {
          $USERexpireDATE = $threedays;
        }
        else if ($_POST['flexRadioDefaultrrw'] === "1d" )
        {
          $USERexpireDATE = $oneday;
        }
        $Username = str_replace(' ', '', $Username);
        $Password = str_replace(' ', '', $Password);
        $addUser = $database->prepare("INSERT INTO accounts(NAME,PASSWORD,RANKUSER,CREATEDATE,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:NAME,:PASSWORD,:RANKUSER,:CREATEDATE,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
        $addUser->bindParam("NAME",$Username);
        $addUser->bindParam("PASSWORD",hash($Secure_Hash_Algorithm,$Password));
        $addUser->bindParam("RANKUSER",$Rank);
        $addUser->bindParam("CREATEDATE",$timenow);
        $addUser->bindParam("EXPIREDATE",$USERexpireDATE);
        $addUser->bindParam("APPKEY",$_SESSION['app_data']->APPKEY);
        $addUser->bindParam("APPOWNERKEY",$_SESSION['user_data']->ACCOUNTKEY);
        if ($addUser->execute())
        {
        }
        else
        {
        }
      }
    }
    }
  }
}
if (isset($_POST['deleteuser']))
{
  {
    $DeletedUser = $_POST['deleteuser'];
    $checkU = $database->prepare("SELECT NULL FROM accounts WHERE ID = :name AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkU->bindParam(":name",$DeletedUser);
    $checkU->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
    $checkU->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $checkU->execute();
    if ($checkU->rowCount() === 1)
    {
    $removeuser = $database->prepare("DELETE FROM accounts WHERE ID = :name AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $removeuser->bindParam("name",$DeletedUser);    
    $removeuser->bindParam("appkey",$_SESSION['app_data']->APPKEY);
        $removeuser->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
        if ($removeuser->execute())
        {
        }
        else
        {
        }
    }
    else
    {
    }
}
}
if (isset($_POST['resetuser']))
{
    $password = "NULL";
    $ResetedUser = $_POST['resetuser'];
    $changePass = $database->prepare("UPDATE accounts SET HWID = :hwid WHERE ID = :name AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $changePass->bindParam(":hwid",$password);
    $changePass->bindParam(":name",$ResetedUser);
    $changePass->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
    $changePass->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
        if ($changePass->execute())
        {
        }
        else
        {
        }
}
if (isset($_POST['delall']))
{
    $aremovekey = $database->prepare("DELETE FROM accounts WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $aremovekey->bindParam("appkey",$_SESSION['app_data']->APPKEY);
        $aremovekey->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
        $aremovekey->execute();
}
if (isset($_POST['delused']))
{
    $password = "NULL";
    $aremovekey = $database->prepare("UPDATE accounts SET HWID = :hwid WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");    $aremovekey->bindParam("appkey",$_SESSION['app_data']->APPKEY);
    $aremovekey->bindParam("hwid",$password);
    $aremovekey->bindParam("appkey",$_SESSION['app_data']->APPKEY);
    $aremovekey->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
        $aremovekey->execute();
}
?>
<head>
            
        <meta charset="UTF-8">
        <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(33,37,41);
        box-shadow: 0px 1px 1px rgb(33,37,41) inset, 0px 0px 8px rgb(33,37,41);
         background-color: rgb(33,37,41);
         color:gray;
    }
    </style>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href=" ../..">UMAR</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarScroll">
          <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <?php if ($_SESSION['user_data']->RANK == 0)
  {
    echo '<li class="nav-item">
    <a class="nav-link" href=" ../../dashboard/apps">Applications</a>
  </li>';
  }
  else
  {
    echo '<li class="nav-item">
    <a class="nav-link" href=" ../../dashboard/premium/apps">Applications</a>
  </li>';
  }?>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/keys">Keys</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/users">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/variables">Variables</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard/blacklisted">Blacklisted</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href=" ../../dashboard">Account</a>
            </li>
           
          </ul>
          <form class="d-flex" role="search">
          </form>
        </div>
      </div>
    </nav>
    <div class="container">
      <br>
 <form method="POST">
 <div class="col-sm-5">
<div class="card text-white bg-dark">
<div class="card-body">
 <div class="d-grid gap-2 d-md-block">
 <button class="btn btn-primary" type="button" data-bs-toggle="modal" data-bs-target="#newkey">Add New user</button>
   <button class="btn btn-secondary" type="button" data-bs-toggle="modal" data-bs-target="#extendsub">Extend Subscription</button>
 <button class="btn btn-success" type="submit" name="delused">Mass HWID reset</button>
  <button class="btn btn-danger" type="submit" name="delall">Delete All users</button>
 </form>
 <form method="POST" action="export.php">
 <input type="submit" name="Export" class="btn btn-light" value="Export" />
 </form>
 <form method="POST">
 </div>
 </div>
 </div>
 </div>
 <br>
 <div class="modal fade" id="newkey" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
    Username : <input class="form-control" placeholder="Username" name="Username"/>
              <p> </p>
    Password : <input class="form-control" placeholder="Password" name="Password"/>
              <p> </p>
    Rank : <input class="form-control" placeholder="User Rank" name="Rank" value="Customer"/>
              <p> </p>
    Expire Date : <select name="flexRadioDefaultrrw" class="form-control">
                <option value="1d">1 Day</option>
                <option value="3d">3 Days</option>
                <option value="1w">1 Week</option>
                <option value="3w">3 Weeks</option>
                <option value="1m">1 Month</option>
                <option value="3m">3 Months</option>
                <option value="1m">1 Year</option>
                <option value="3m">3 Years</option>
                <option value="Lifetime">Lifetime</option>
              </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="adduser">Create</button>
      </div>
    </div>
  </div>
</div>
 <div class="modal fade" id="extendsub" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Extend Subscription</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
                        <p>Note: This will apply to all users</p>
    Extend by days : <input class="form-control" placeholder="day(s)" name="subdays"/>
    Effect even on expired users : <select name="flexRadioDefaultzrw" class="form-control">
                <option value="n">No</option>
                <option value="y">Yes</option>
              </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="continuesub">Continue</button>
      </div>
    </div>
  </div>
</div>
<div class="card text-white bg-dark">
<div class="card-body">
 <table class="table table-striped table-dark">
  <thead>
    <tr>
    <th scope="col">Username</th>
    <th scope="col">Rank</th>
    <th scope="col">Create</th>
    <th scope="col">Last Login</th>
    <th scope="col">Expire</th>
    <th scope="col">Hardware ID</th>
    <th scope="col">Management</th>
    </tr>
  </thead>
  <tbody>
  <?php
    function expiredornot($expiredate, $format)
    {
        if (time() >= $expiredate)
        {
          return "Expired";
        }
        else
        {
          return date($format, $expiredate);
        }
    }
            function lastlogindate($s, $d)
        {
            if ($d !== "N/A")
            {
                return date($s, $d);
            }
            else
            {
                return $d;
            }
        }
    $sqlResult = $database->prepare("SELECT ID, NAME, RANKUSER, CREATEDATE, EXPIREDATE, LASTLOGINDATE, HWID FROM accounts WHERE APPOWNERKEY = :appownerkey AND APPKEY = :appkey");    $sqlResult->bindParam("appkey",$_SESSION['app_data']->APPKEY);
    $sqlResult->bindParam("appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
    $sqlResult->bindParam("appkey",$_SESSION['app_data']->APPKEY);
    $sqlResult->execute();
      foreach($sqlResult AS $result)
      {
          $format = $_SESSION['user_data']->DATEFORMAT;
         $id = $result['ID'];
        $username = $result['NAME'];
        $rank = $result['RANKUSER'];
        $createdate = $result['CREATEDATE'];
        $expiredate = $result['EXPIREDATE'];
        $lastlogin = $result['LASTLOGINDATE'];
        if ($expiredate !== "Lifetime") {
            $expiredate = expiredornot($expiredate, $format);
        }
        $hwid = $result['HWID'];
        echo '<tr>
        <th scope="row">'. $username .'</th>
        <td>'. $rank .'</td>
        <td>'. date($format, $createdate) .'</td>
        <td>'. lastlogindate($format, $lastlogin) .'</td>
        <td>'. $expiredate .'</td>
        <td>'. $hwid .'</td>
        <td>
        <a href="./edit.php?n='.$id.'"class="btn btn-outline-info mt-1">Edit</a>
        <button class="btn btn-outline-warning" type="submit" value='.$id.' name="resetuser">Reset HWID</button>
        <button class="btn btn-danger" type="submit" value='.$id.' name="deleteuser">Delete</button>
        </td>
      </tr>';
      }
    ?>
  </tbody>
</table>
</div>
</div>
</form>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
</div>