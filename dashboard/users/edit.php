<?php
session_start();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<?php
require ('../../connection/config.php');
if (!isset($_SESSION['user_data']))
{
    echo '<div class="alert alert-dark alert-dismissible fade show" role="alert">
    User data not found!
  </div>';
  echo "<meta http-equiv='Refresh' Content='0; url= ../..'>"; 
  die();
}
if ($_SESSION['user_data']->APPS === "0")
{
echo "<meta http-equiv='Refresh' Content='0; url= ../../dashboard/apps'>"; 
die();
}
$appname = $database->prepare("SELECT * FROM apps WHERE APPOWNERID = :appownerid AND APPID = :appid AND APPUNID = :appunid");
$appname->bindParam("appownerid",$_SESSION['user_data']->ACCOUNTID);
$appname->bindParam("appid",$_SESSION['user_data']->CURRAPP);
$appname->bindParam("appunid",$_SESSION['user_data']->ID);
if ($appname->execute())
{
    $appnamef = $appname->fetchObject();
    $_SESSION['app_data'] = $appnamef;
}
    $name = $_GET['n'];
        $checkU = $database->prepare("SELECT * FROM accounts WHERE ID = :id AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
        $checkU->bindParam(":id",$name);
        $checkU->bindParam(":appkey",$_SESSION['app_data']->APPKEY);
        $checkU->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
        $checkU->execute();
        if ($checkU->rowCount() === 1)
        {
          $usernamef = $checkU->fetchObject();
          $_SESSION['ua_data'] = $usernamef;
        }
        else
        {
            echo "<meta http-equiv='Refresh' Content='0; url= ../../404.html'>"; 
            die();
        }
        if (isset($_POST['save']))
        {
            $u_name = $_POST['u_Username'];
            $u_pass = $_POST['u_Password'];
            $u_rank = $_POST['u_Rank'];
            $u_expire = $_POST['u_ExpireDate'];
            if (!(empty($u_name)))
            {
              if (!(empty($u_rank)))
              {
                if (!(empty($u_expire)))
                {
                  if (empty($u_pass))
                  {
                    $u_name = str_replace(' ', '', $u_name);
                    $u_rank = str_replace(' ', '', $u_rank);
                    $u_expire = str_replace(' ', '', $u_expire);
                    $upUser = $database->prepare("UPDATE accounts SET NAME = :name WHERE ID = :oldname AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                    $upUser->bindParam(":name",$u_name);
                    $upUser->bindParam(":oldname",$name); 
                    $upUser->bindParam(":appkey",$_SESSION['app_data']->APPKEY);   
                        $upUser->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
                        $upUser->execute();
                        $upRser = $database->prepare("UPDATE accounts SET RANKUSER = :rankuser WHERE ID = :oldname AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                        $upRser->bindParam(":rankuser",$u_rank);
                        $upRser->bindParam(":oldname",$name); 
                        $upRser->bindParam(":appkey",$_SESSION['app_data']->APPKEY);   
                            $upRser->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
                            $upRser->execute();
                            $upEser = $database->prepare("UPDATE accounts SET EXPIREDATE = :expiredate WHERE ID = :oldname AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                            $upEser->bindParam(":expiredate",strtotime($u_expire));
                            $upEser->bindParam(":oldname",$name); 
                            $upEser->bindParam(":appkey",$_SESSION['app_data']->APPKEY);   
                                $upEser->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
                                $upEser->execute();
                        echo "<meta http-equiv='Refresh' Content='0; url= ../../dashboard/users'>"; 
                        exit();
                  }
                  else
                  {
                    $u_name = str_replace(' ', '', $u_name);
                    $u_pass = str_replace(' ', '', $u_pass);
                    $u_rank = str_replace(' ', '', $u_rank);
                    $u_expire = str_replace(' ', '', $u_expire);
                    $upUser = $database->prepare("UPDATE accounts SET NAME = :name WHERE ID = :oldname AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                    $upUser->bindParam(":name",$u_name);
                    $upUser->bindParam(":oldname",$name); 
                    $upUser->bindParam(":appkey",$_SESSION['app_data']->APPKEY);   
                        $upUser->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
                        $upUser->execute();
                        $upRser = $database->prepare("UPDATE accounts SET RANKUSER = :rankuser WHERE ID = :oldname AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                        $upRser->bindParam(":rankuser",$u_rank);
                        $upRser->bindParam(":oldname",$name); 
                        $upRser->bindParam(":appkey",$_SESSION['app_data']->APPKEY);   
                            $upRser->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
                            $upRser->execute();
                            $upEser = $database->prepare("UPDATE accounts SET EXPIREDATE = :expiredate WHERE ID = :oldname AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                            $upEser->bindParam(":expiredate",strtotime($u_expire));
                            $upEser->bindParam(":oldname",$name); 
                            $upEser->bindParam(":appkey",$_SESSION['app_data']->APPKEY);   
                                $upEser->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
                                $upEser->execute();
                                $upPser = $database->prepare("UPDATE accounts SET PASSWORD = :password WHERE ID = :oldname AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                                $upPser->bindParam(":password",hash($Secure_Hash_Algorithm,$u_pass));
                                $upPser->bindParam(":oldname",$name); 
                                $upPser->bindParam(":appkey",$_SESSION['app_data']->APPKEY);   
                                    $upPser->bindParam(":appownerkey",$_SESSION['user_data']->ACCOUNTKEY);
                                    $upPser->execute();
                        echo "<meta http-equiv='Refresh' Content='0; url= ../../dashboard/users'>"; 
                        exit();
                  }
                }
              }
            }
        }
?>
<head>
            
        <meta charset="UTF-8">
    <meta name="description" content="The most advanced authentication system ever seen!">
     
     
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../../assets/authLogo.png">
    <title>Umar - The most advanced licensing system solution for developers</title>
    <style>
      body {
        background-color: rgb(44,48,52);
        color: white;
      }
      .modal-content { background: rgb(44,48,52) !important; }
      .body-bg { background: rgb(44,48,52) !important; }
      .form-control {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
      .form-control:focus {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    .form-control:disabled {
        border-color: rgb(44,48,52);
        box-shadow: 0px 1px 1px rgb(44,48,52) inset, 0px 0px 8px rgb(44,48,52);
         background-color: rgb(44,48,52);
         color:gray;
    }
    </style>
</head>
<div class="container">
      <br>
 <form method="POST">
 <br>
<div class="card text-white bg-dark">
<div class="card-body">
Username : <input class="form-control" placeholder="Username" type="text" name="u_Username" value=" <?php echo str_replace(' ', '', $_SESSION['ua_data']->NAME) ?>" required/>
<p> </p>
Password : <input class="form-control" type="password" name="u_Password"/>
<p> </p>
Rank : <input class="form-control" placeholder="Rank" type="text" name="u_Rank" value=" <?php echo str_replace(' ', '', $_SESSION['ua_data']->RANKUSER) ?>" required/>
<p> </p>
Expire Date : <input class="form-control" placeholder="(yy/mm/dd) or Lifetime" type="text" name="u_ExpireDate" value=" <?php
        if ($_SESSION['ua_data']->EXPIREDATE !== "Lifetime") {
            echo date($_SESSION['user_data']->DATEFORMAT, $_SESSION['ua_data']->EXPIREDATE);
        }
        else
        {
            echo 'Lifetime';
        }
?>" required/>
  <div class="d-grid gap-2">
    <button class="btn btn-primary mt-3" type="submit" name="save">Save</button>
    <a href=" ../../dashboard/users" class="btn btn-danger mt-1">Return</a>
    </div>
</div>
</div>
</form>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
</div>