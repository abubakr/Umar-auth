const sign_in_btn = document.querySelector("#sign-in-btn");
const sign_up_btn = document.querySelector("#sign-up-btn");
const sdk_button = document.querySelector("#sdk");
const container = document.querySelector(".container");

sign_up_btn.addEventListener("click", () => {
  container.classList.add("sign-up-mode");
});

sdk_button.addEventListener("click", () => {
  window.location.href = "../sdk";
});

sign_in_btn.addEventListener("click", () => {
  container.classList.remove("sign-up-mode");
});
