<?php
require ('../connection/config.php');
$altashfir = array("0","1","2","3","4","5","6","7","8","9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
$tabadal = array("-QZ-", "-SA-", "-IF-", "DE-", "-EE-", "-JJ-", "-GG-", "MP-", "-WI-", "-ZF-","-XC-", "-YU-", "-OL-", "MV-", "-RS-", "-EV-", "-WZ-", "DP-", "-IJ-", "-KN-", "-CA-", "-TW-", "-BI-", "-JH-", "-MW-", "-IS-", "-LA-", "-ME-", "-EP-", "-ON-", "-WK-", "-NB-", "-BA-", "-RE-", "-IN-", "-LU-");
function getClientIP()
{
    $IP = false;
    if (getenv('HTTP_CLIENT_IP'))
    {
        $IP = getenv('HTTP_CLIENT_IP');
    }
    else if(getenv('HTTP_X_FORWARDED_FOR'))
    {
        $IP = getenv('HTTP_X_FORWARDED_FOR');
    }
    else if(getenv('HTTP_X_FORWARDED'))
    {
        $IP = getenv('HTTP_X_FORWARDED');
    }
    else if(getenv('HTTP_FORWARDED_FOR'))
    {
        $IP = getenv('HTTP_FORWARDED_FOR');
    }
    else if(getenv('HTTP_FORWARDED'))
    {
        $IP = getenv('HTTP_FORWARDED');
    }
    else if(getenv('REMOTE_ADDR'))
    {
        $IP = getenv('REMOTE_ADDR');
    }

    //If HTTP_X_FORWARDED_FOR == server ip
    if((($IP) && ($IP == getenv('SERVER_ADDR')) && (getenv('REMOTE_ADDR')) || (!filter_var($IP, FILTER_VALIDATE_IP))))
    {
        $IP = getenv('REMOTE_ADDR');
    }

    if($IP)
    {
        if(!filter_var($IP, FILTER_VALIDATE_IP))
        {
            $IP = false;
        }
    }
    else
    {
        $IP = false;
    }
    return $IP;
}
if ($_POST['s0rt'])
{
    $type = $_POST['s0rt'];
    try {
        //...
    }
    catch (exception $e) {
    }
}
else if ($_GET['s0rt'])
{
    $type = $_GET['s0rt'];
    try {
        //...
    }
    catch (exception $e) {
    }
}
//convert time


function convert_t($date, $ownerkey, $database, $altashfir, $tabadal)
{
    $ownerkey = str_replace($tabadal, $altashfir, $ownerkey);
    $sqlreq = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :accountkey");
    $sqlreq->bindParam(":accountkey",$ownerkey);
    $sqlreq->execute();
    $ownerinfo = $sqlreq->fetchObject();
    $format = $ownerinfo->DATEFORMAT;
    if ($date == "Lifetime")
    {
        return "Lifetime";
    }
    else
    {
        return date($format, $date);
    }
}


//return expiration date


function expire_t($date)
{
    if ($date == "Lifetime")
    {
        return "Lifetime";
    }
    else
    {
        return strtotime($date);
    }
}


//init from public api




if ($type === 'init')
{
$appkey = $_POST['appkey'];
$acckey = $_POST['acckey'];
$findowner = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :appownerkey");
$findowner->bindParam(":appownerkey",$acckey);
$findowner->execute();
if ($findowner->rowCount() === 1)
{
$isqlreq = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
$isqlreq->bindParam(":appkey",$appkey);
$isqlreq->execute();
if ($isqlreq->rowCount() === 1)
{
        $typeofi = "Initialization";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
    $stmt = $database->prepare('SELECT * FROM apps WHERE APPKEY = :appkey');
    $stmt->bindParam(":appkey",$appkey);
    $stmt->execute();
    $rows = [];
    foreach ($stmt as $row) {
        $rows[] = [
            'APPNAME' => $row['APPNAME'],
            'STATUS' => $row['STATUS'],
            'LOGGED' => $row['LOGGED'],
            'REGISTERED' => $row['REGISTERED'],
            'PAUSED' => $row['PAUSED'],
        ];
    }
    $the_resylT = json_encode($rows);
    $one = trim($the_resylT, "[");
    $final = trim($one, "]");
    echo $final;
        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
    $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
    $getINFO->bindParam(":appkey",$appkey);
    $getINFO->execute();
    $appinfo = $getINFO->fetchObject();
    if ($appinfo->DISC == "1")
    {
        $webhookurl = $appinfo->DISCWH;
        $timestamp = date("c", strtotime("now"));
        $json_data = json_encode([
            "content" => "*You have a new alert from your Application!*",
            "tts" => false,
            "embeds" => [
                [
                    "title" => ":grey_question: New connection Detected",
                    "type" => "rich",
                    "timestamp" => $timestamp,
                    "color" => hexdec( "3366ff" ),
                    "footer" => [
                        "text" => "Umar - The Ultimate Solution",
                        "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                    ],
                    "image" => [
                        "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                    ],
                    "fields" => [
                    ]
                ]
            ]
        
        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
        $ch = curl_init( $webhookurl );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec( $ch );
        curl_close( $ch );
    }
}
else
{
    die("incorrect_application_key");
}
}
else
{
    die("incorrect_account_key");
}
}



//login from public api


else if ($type === 'login')
{
$username = $_POST['username'];
$password = $_POST['passw0rd'];
$password = hash($Secure_Hash_Algorithm, $password);
$appkey = $_POST['appkey'];
$acckey = $_POST['acckey'];
$sqlreq = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
$sqlreq->bindParam(":name",$username);
$sqlreq->bindParam(":password",$password);
$sqlreq->bindParam(":appkey",$appkey);
$sqlreq->bindParam(":appownerkey",$acckey);
$sqlreq->execute();
if ($sqlreq->rowCount() === 1)
{
    $timenow =time();
    $userinfo = $sqlreq->fetchObject();
    if ($userinfo->EXPIREDATE == "Lifetime")
    {
        if ($_POST['hwid']) {
            $hwid = $_POST['hwid'];
        if ($userinfo->HWID == "NULL")
        {
                        $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
            $changeHwid = $database->prepare("UPDATE accounts SET HWID = :hwid WHERE NAME = :name AND PASSWORD = :password");
            $changeHwid->bindParam(":name",$username);
            $changeHwid->bindParam(":hwid",$hwid);
            $changeHwid->bindParam(":password",$password);
            if ($changeHwid->execute())
            {
                $findowner = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :appownerkey");
                $findowner->bindParam(":appownerkey",$acckey);
                $findowner->execute();
                $findowner = $findowner->fetchObject();
                $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                $stmt->bindParam(":name",$username);
                $stmt->bindParam(":password",$password);
                $stmt->bindParam(":appkey",$appkey);
                $stmt->bindParam(":appownerkey",$acckey);
                $stmt->execute();
                $rows = [];
                foreach ($stmt as $row) {
                    $rows[] = [
                        'NAME' => $row['NAME'],
                        'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                        'HWID' => $row['HWID'],
                    ];
                }
    $the_resylT = json_encode($rows);
    $one = trim($the_resylT, "[");
    $final = trim($one, "]");
    echo $final;
                $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                $getINFO->bindParam(":appkey",$appkey);
                $getINFO->execute();
                $appinfo = $getINFO->fetchObject();
                if ($appinfo->DISC == "1")
                {
                    $webhookurl = $appinfo->DISCWH;
                    $timestamp = date("c", strtotime("now"));
                    $json_data = json_encode([
                        "content" => "_ _",
                        "tts" => false,
                        "embeds" => [
                            [
                                "title" => ":white_check_mark: Sign in Detected",
                                "type" => "rich",
                                "timestamp" => $timestamp,
                                "color" => hexdec( "3366ff" ),
                                "footer" => [
                                    "text" => "Umar - The Ultimate Solution",
                                    "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                ],
                                "image" => [
                                    "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                ],
                                "fields" => [
                                    [
                                        "name" => "• Username :",
                                        "value" => '```' . $userinfo->NAME . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Rank :",
                                        "value" => '```' . $userinfo->RANKUSER . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Create Date :",
                                        "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Expire Date :",
                                        "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• HWID :",
                                        "value" => '```' . $userinfo->HWID . '```',
                                        "inline" => true
                                    ],
                                ]
                            ]
                        ]
                    
                    ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                    $ch = curl_init( $webhookurl );
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                    curl_setopt( $ch, CURLOPT_POST, 1);
                    curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt( $ch, CURLOPT_HEADER, 0);
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                    $response = curl_exec( $ch );
                    curl_close( $ch );
                }

            }
            else
            {
                die("incorrect_user_details");
            }
        }
        else
        {
            $hwidlockop = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
            $hwidlockop->bindParam(":appkey",$appkey);
            $hwidlockop->execute();
            $hwidlockinfo = $hwidlockop->fetchObject();
            if ($hwidlockinfo->HWID == "1")
            {
                if ($userinfo->HWID == $hwid)
                {
                                $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                    $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                    $stmt->bindParam(":name",$username);
                    $stmt->bindParam(":password",$password);
                    $stmt->bindParam(":appkey",$appkey);
                    $stmt->bindParam(":appownerkey",$acckey);
                    $stmt->execute();
                    $rows = [];
                    foreach ($stmt as $row) {
                        $rows[] = [
                            'NAME' => $row['NAME'],
                            'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                            'HWID' => $row['HWID'],
                        ];
                    }
    $the_resylT = json_encode($rows);
    $one = trim($the_resylT, "[");
    $final = trim($one, "]");
    echo $final;
                $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                    $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                    $getINFO->bindParam(":appkey",$appkey);
                    $getINFO->execute();
                    $appinfo = $getINFO->fetchObject();
                    if ($appinfo->DISC == "1")
                    {
                        $webhookurl = $appinfo->DISCWH;
                        $timestamp = date("c", strtotime("now"));
                        $json_data = json_encode([
                            "content" => "_ _",
                            "tts" => false,
                            "embeds" => [
                                [
                                    "title" => ":white_check_mark: Sign in Detected",
                                    "type" => "rich",
                                    "timestamp" => $timestamp,
                                    "color" => hexdec( "3366ff" ),
                                    "footer" => [
                                        "text" => "Umar - The Ultimate Solution",
                                        "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                    ],
                                    "image" => [
                                        "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                    ],
                                    "fields" => [
                                        [
                                            "name" => "• Username :",
                                            "value" => '```' . $userinfo->NAME . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Rank :",
                                            "value" => '```' . $userinfo->RANKUSER . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Create Date :",
                                            "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Expire Date :",
                                            "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• HWID :",
                                            "value" => '```' . $userinfo->HWID . '```',
                                            "inline" => true
                                        ],
                                    ]
                                ]
                            ]
                        
                        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                        $ch = curl_init( $webhookurl );
                        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                        curl_setopt( $ch, CURLOPT_POST, 1);
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt( $ch, CURLOPT_HEADER, 0);
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                        $response = curl_exec( $ch );
                        curl_close( $ch );
                    }
                }
                else if ($userinfo->HWID != $hwid)
                {
                    die("hwid_does_not_match");
                }
            }
            else
            {
                            $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                $stmt->bindParam(":name",$username);
                $stmt->bindParam(":password",$password);
                $stmt->bindParam(":appkey",$appkey);
                $stmt->bindParam(":appownerkey",$acckey);
                $stmt->execute();
                $rows = [];
                foreach ($stmt as $row) {
                    $rows[] = [
                        'NAME' => $row['NAME'],
                        'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE', $database, $altashfir, $tabadal), $acckey),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE', $database, $altashfir, $tabadal), $acckey),
                        'HWID' => $row['HWID'],
                    ];
                }
    $the_resylT = json_encode($rows);
    $one = trim($the_resylT, "[");
    $final = trim($one, "]");
    echo $final;
                $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                $getINFO->bindParam(":appkey",$appkey);
                $getINFO->execute();
                $appinfo = $getINFO->fetchObject();
                if ($appinfo->DISC == "1")
                {
                    $webhookurl = $appinfo->DISCWH;
                    $timestamp = date("c", strtotime("now"));
                    $json_data = json_encode([
                        "content" => "_ _",
                        "tts" => false,
                        "embeds" => [
                            [
                                "title" => ":white_check_mark: Sign in Detected",
                                "type" => "rich",
                                "timestamp" => $timestamp,
                                "color" => hexdec( "3366ff" ),
                                "footer" => [
                                    "text" => "Umar - The Ultimate Solution",
                                    "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                ],
                                "image" => [
                                    "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                ],
                                "fields" => [
                                    [
                                        "name" => "• Username :",
                                        "value" => '```' . $userinfo->NAME . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Rank :",
                                        "value" => '```' . $userinfo->RANKUSER . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Create Date :",
                                        "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Expire Date :",
                                        "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• HWID :",
                                        "value" => '```' . $userinfo->HWID . '```',
                                        "inline" => true
                                    ],
                                ]
                            ]
                        ]
                    
                    ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                    $ch = curl_init( $webhookurl );
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                    curl_setopt( $ch, CURLOPT_POST, 1);
                    curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt( $ch, CURLOPT_HEADER, 0);
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                    $response = curl_exec( $ch );
                    curl_close( $ch );
                }
            }
        } //st
        }
        else
        {
                                    $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                $stmt->bindParam(":name",$username);
                $stmt->bindParam(":password",$password);
                $stmt->bindParam(":appkey",$appkey);
                $stmt->bindParam(":appownerkey",$acckey);
                $stmt->execute();
                $rows = [];
                foreach ($stmt as $row) {
                    $rows[] = [
                        'NAME' => $row['NAME'],
                        'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                        'HWID' => $row['HWID'],
                    ];
                }
    $the_resylT = json_encode($rows);
    $one = trim($the_resylT, "[");
    $final = trim($one, "]");
    echo $final;
                $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                $getINFO->bindParam(":appkey",$appkey);
                $getINFO->execute();
                $appinfo = $getINFO->fetchObject();
                if ($appinfo->DISC == "1")
                {
                    $webhookurl = $appinfo->DISCWH;
                    $timestamp = date("c", strtotime("now"));
                    $json_data = json_encode([
                        "content" => "_ _",
                        "tts" => false,
                        "embeds" => [
                            [
                                "title" => ":white_check_mark: Sign in Detected",
                                "type" => "rich",
                                "timestamp" => $timestamp,
                                "color" => hexdec( "3366ff" ),
                                "footer" => [
                                    "text" => "Umar - The Ultimate Solution",
                                    "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                ],
                                "image" => [
                                    "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                ],
                                "fields" => [
                                    [
                                        "name" => "• Username :",
                                        "value" => '```' . $userinfo->NAME . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Rank :",
                                        "value" => '```' . $userinfo->RANKUSER . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Create Date :",
                                        "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Expire Date :",
                                        "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                ]
                            ]
                        ]
                    
                    ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                    $ch = curl_init( $webhookurl );
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                    curl_setopt( $ch, CURLOPT_POST, 1);
                    curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt( $ch, CURLOPT_HEADER, 0);
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                    $response = curl_exec( $ch );
                    curl_close( $ch );
                }
        }
    }
    else
    {
        $expireW = $userinfo->EXPIREDATE;
        $dateW = time();
        if (intval($expireW) > intval($dateW))
        {
                    if ($_POST['hwid']) {
            $hwid = $_POST['hwid'];
            if ($userinfo->HWID == "NULL")
            {
                $changeHwid = $database->prepare("UPDATE accounts SET HWID = :hwid WHERE NAME = :name AND PASSWORD = :password");
                $changeHwid->bindParam(":name",$username);
                $changeHwid->bindParam(":hwid",$hwid);
                $changeHwid->bindParam(":password",$password);
                if ($changeHwid->execute())
                {
                                $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                    $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                    $stmt->bindParam(":name",$username);
                    $stmt->bindParam(":password",$password);
                    $stmt->bindParam(":appkey",$appkey);
                    $stmt->bindParam(":appownerkey",$acckey);
                    $stmt->execute();
                    $rows = [];
                    foreach ($stmt as $row) {
                        $rows[] = [
                            'NAME' => $row['NAME'],
                            'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                            'HWID' => $row['HWID'],
                        ];
                    }
    $the_resylT = json_encode($rows);
    $one = trim($the_resylT, "[");
    $final = trim($one, "]");
    echo $final;
                $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                    $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                    $getINFO->bindParam(":appkey",$appkey);
                    $getINFO->execute();
                    $appinfo = $getINFO->fetchObject();
                    if ($appinfo->DISC == "1")
                    {
                        $webhookurl = $appinfo->DISCWH;
                        $timestamp = date("c", strtotime("now"));
                        $json_data = json_encode([
                            "content" => "_ _",
                            "tts" => false,
                            "embeds" => [
                                [
                                    "title" => ":white_check_mark: Sign in Detected",
                                    "type" => "rich",
                                    "timestamp" => $timestamp,
                                    "color" => hexdec( "3366ff" ),
                                    "footer" => [
                                        "text" => "Umar - The Ultimate Solution",
                                        "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                    ],
                                    "image" => [
                                        "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                    ],
                                    "fields" => [
                                        [
                                            "name" => "• Username :",
                                            "value" => '```' . $userinfo->NAME . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Rank :",
                                            "value" => '```' . $userinfo->RANKUSER . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Create Date :",
                                            "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Expire Date :",
                                            "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• HWID :",
                                            "value" => '```' . $userinfo->HWID . '```',
                                            "inline" => true
                                        ],
                                    ]
                                ]
                            ]
                        
                        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                        $ch = curl_init( $webhookurl );
                        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                        curl_setopt( $ch, CURLOPT_POST, 1);
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt( $ch, CURLOPT_HEADER, 0);
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                        $response = curl_exec( $ch );
                        curl_close( $ch );
                    }
                }
                else
                {
                    die("incorrect_user_details");
                }
            }
            else
            {
                $hwidlockop = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                $hwidlockop->bindParam(":appkey",$appkey);
                $hwidlockop->execute();
                $hwidlockinfo = $hwidlockop->fetchObject();
                if ($hwidlockinfo->HWID == "1")
                {
                    if ($userinfo->HWID == $hwid)
                    {
                                    $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                        $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                        $stmt->bindParam(":name",$username);
                        $stmt->bindParam(":password",$password);
                        $stmt->bindParam(":appkey",$appkey);
                        $stmt->bindParam(":appownerkey",$acckey);
                        $stmt->execute();
                        $rows = [];
                        foreach ($stmt as $row) {
                            $rows[] = [
                                'NAME' => $row['NAME'],
                                'RANKUSER' => $row['RANKUSER'],
                                'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                                'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                                'HWID' => $row['HWID'],
                            ];
                        }
    $the_resylT = json_encode($rows);
    $one = trim($the_resylT, "[");
    $final = trim($one, "]");
    echo $final;
                $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                        $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                        $getINFO->bindParam(":appkey",$appkey);
                        $getINFO->execute();
                        $appinfo = $getINFO->fetchObject();
                        if ($appinfo->DISC == "1")
                        {
                            $webhookurl = $appinfo->DISCWH;
                            $timestamp = date("c", strtotime("now"));
                            $json_data = json_encode([
                                "content" => "_ _",
                                "tts" => false,
                                "embeds" => [
                                    [
                                        "title" => ":white_check_mark: Sign in Detected",
                                        "type" => "rich",
                                        "timestamp" => $timestamp,
                                        "color" => hexdec( "3366ff" ),
                                        "footer" => [
                                            "text" => "Umar - The Ultimate Solution",
                                            "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                        ],
                                        "image" => [
                                            "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                        ],
                                        "fields" => [
                                            [
                                                "name" => "• Username :",
                                                "value" => '```' . $userinfo->NAME . '```',
                                                "inline" => true
                                            ],
                                            [
                                                "name" => "• Rank :",
                                                "value" => '```' . $userinfo->RANKUSER . '```',
                                                "inline" => true
                                            ],
                                            [
                                                "name" => "• Create Date :",
                                                "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                                "inline" => true
                                            ],
                                            [
                                                "name" => "• Expire Date :",
                                                "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                                "inline" => true
                                            ],
                                            [
                                                "name" => "• HWID :",
                                                "value" => '```' . $userinfo->HWID . '```',
                                                "inline" => true
                                            ],
                                        ]
                                    ]
                                ]
                            
                            ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                            $ch = curl_init( $webhookurl );
                            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                            curl_setopt( $ch, CURLOPT_POST, 1);
                            curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                            curl_setopt( $ch, CURLOPT_HEADER, 0);
                            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                            $response = curl_exec( $ch );
                            curl_close( $ch );
                        }
                    }
                    else if ($userinfo->HWID != $hwid)
                    {
                        die("hwid_does_not_match");
                    }
                }
                else
                {
                                $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                    $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                    $stmt->bindParam(":name",$username);
                    $stmt->bindParam(":password",$password);
                    $stmt->bindParam(":appkey",$appkey);
                    $stmt->bindParam(":appownerkey",$acckey);
                    $stmt->execute();
                    $rows = [];
                    foreach ($stmt as $row) {
                        $rows[] = [
                            'NAME' => $row['NAME'],
                            'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                            'HWID' => $row['HWID'],
                        ];
                    }
    $the_resylT = json_encode($rows);
    $one = trim($the_resylT, "[");
    $final = trim($one, "]");
    echo $final;
                $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                    $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                    $getINFO->bindParam(":appkey",$appkey);
                    $getINFO->execute();
                    $appinfo = $getINFO->fetchObject();
                    if ($appinfo->DISC == "1")
                    {
                        $webhookurl = $appinfo->DISCWH;
                        $timestamp = date("c", strtotime("now"));
                        $json_data = json_encode([
                            "content" => "_ _",
                            "tts" => false,
                            "embeds" => [
                                [
                                    "title" => ":white_check_mark: Sign in Detected",
                                    "type" => "rich",
                                    "timestamp" => $timestamp,
                                    "color" => hexdec( "3366ff" ),
                                    "footer" => [
                                        "text" => "Umar - The Ultimate Solution",
                                        "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                    ],
                                    "image" => [
                                        "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                    ],
                                    "fields" => [
                                        [
                                            "name" => "• Username :",
                                            "value" => '```' . $userinfo->NAME . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Rank :",
                                            "value" => '```' . $userinfo->RANKUSER . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Create Date :",
                                            "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Expire Date :",
                                            "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• HWID :",
                                            "value" => '```' . $userinfo->HWID . '```',
                                            "inline" => true
                                        ],
                                    ]
                                ]
                            ]
                        
                        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                        $ch = curl_init( $webhookurl );
                        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                        curl_setopt( $ch, CURLOPT_POST, 1);
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt( $ch, CURLOPT_HEADER, 0);
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                        $response = curl_exec( $ch );
                        curl_close( $ch );
                    }
                }
            }
        }
        else
        {
                                                $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                $stmt->bindParam(":name",$username);
                $stmt->bindParam(":password",$password);
                $stmt->bindParam(":appkey",$appkey);
                $stmt->bindParam(":appownerkey",$acckey);
                $stmt->execute();
                $rows = [];
                foreach ($stmt as $row) {
                    $rows[] = [
                        'NAME' => $row['NAME'],
                        'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                        'HWID' => $row['HWID'],
                    ];
                }
    $the_resylT = json_encode($rows);
    $one = trim($the_resylT, "[");
    $final = trim($one, "]");
    echo $final;
                $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                $getINFO->bindParam(":appkey",$appkey);
                $getINFO->execute();
                $appinfo = $getINFO->fetchObject();
                if ($appinfo->DISC == "1")
                {
                    $webhookurl = $appinfo->DISCWH;
                    $timestamp = date("c", strtotime("now"));
                    $json_data = json_encode([
                        "content" => "_ _",
                        "tts" => false,
                        "embeds" => [
                            [
                                "title" => ":white_check_mark: Sign in Detected",
                                "type" => "rich",
                                "timestamp" => $timestamp,
                                "color" => hexdec( "3366ff" ),
                                "footer" => [
                                    "text" => "Umar - The Ultimate Solution",
                                    "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                ],
                                "image" => [
                                    "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                ],
                                "fields" => [
                                    [
                                        "name" => "• Username :",
                                        "value" => '```' . $userinfo->NAME . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Rank :",
                                        "value" => '```' . $userinfo->RANKUSER . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Create Date :",
                                        "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Expire Date :",
                                        "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                ]
                            ]
                        ]
                    
                    ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                    $ch = curl_init( $webhookurl );
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                    curl_setopt( $ch, CURLOPT_POST, 1);
                    curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt( $ch, CURLOPT_HEADER, 0);
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                    $response = curl_exec( $ch );
                    curl_close( $ch );
                }
        }
        }
        else
        {
            die("subscription_has_expired");
        }
    }
}
else
{
    die("incorrect_user_details");
}
}



//register from public api



else if ($type === 'register')
{
    $name = $_POST['username'];
    $password = $_POST['passw0rd'];
    $password = $_POST['passw0rd'];
    $password = hash($Secure_Hash_Algorithm, $password);
    $invite = $_POST['key'];
    $appkey = $_POST['appkey'];
    $acckey = $_POST['acckey'];
    $name = str_replace(' ', '', $name);
    $checkN = $database->prepare("SELECT * FROM accounts WHERE NAME = :NAME AND APPKEY = :APPKEY");
    $checkN->bindParam("NAME",$name);
    $checkN->bindParam("APPKEY",$appkey);
    $checkN->execute();
    if ($checkN->rowCount() > 0)
    {
        die("name_used");
    }
    else
    {
    $status = 'UNAPPLIED';
    $unstatus = 'APPLIED';
    $checkK = $database->prepare("SELECT * FROM userskeys WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND STATUS = :STATUS");
    $checkK->bindParam("KEYID",$invite);
    $checkK->bindParam("APPKEY",$appkey);
    $checkK->bindParam("STATUS",$status);
    $checkK->execute();
    $key_infow = $checkK->fetchObject();
    if ($checkK->rowCount() > 0)
    {
        $checkAcc = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :appownerkey");
        $checkAcc->bindParam("appownerkey",$acckey);
        $checkAcc->execute();
        $acc_user_info = $checkAcc->fetchObject();
        if ($acc_user_info->RANK === "0")
        {
            $checkbb = $database->prepare("SELECT * FROM accounts WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
            $checkbb->bindParam("appkey",$appkey);
            $checkbb->bindParam("appownerkey",$acckey);
            $checkbb->execute();
            $num = $checkbb->rowCount();
            if ($num >= 100)
            {
                die("limit_reached");
            }
            else
            {
                $checkKw = $database->prepare("SELECT * FROM userskeys WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND STATUS = :STATUS");
                $checkKw->bindParam("KEYID",$invite);
                $checkKw->bindParam("APPKEY",$appkey);
                $checkKw->bindParam("STATUS",$status);
                $checkKw->execute();
                $keyinfof = $checkKw->fetchObject();
                $Rankf = $keyinfof->RANKUSER;
                $EXPIREdatef = expire_t($keyinfof->EXPIREDATE);
                $timenow =time();
                $RegUserw = $database->prepare("INSERT INTO accounts(NAME,PASSWORD,RANKUSER,CREATEDATE,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:NAME,:PASSWORD,:RANKUSER,:CREATEDATE,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
                $RegUserw->bindParam("NAME",$name);
                $RegUserw->bindParam("PASSWORD",$password);
                $RegUserw->bindParam("RANKUSER",$Rankf);
                $RegUserw->bindParam("CREATEDATE",$timenow);
                $RegUserw->bindParam("EXPIREDATE",$EXPIREdatef);
                $RegUserw->bindParam("APPKEY",$appkey);
                $RegUserw->bindParam("APPOWNERKEY",$acckey);
                if ($RegUserw->execute())
                {
                    $changeStatusz = $database->prepare("UPDATE userskeys SET STATUS = :UNSTATUS WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND APPOWNERKEY = :APPOWNERKEY");
                    $changeStatusz->bindParam("UNSTATUS",$unstatus);
                    $changeStatusz->bindParam("KEYID",$invite);
                    $changeStatusz->bindParam("APPKEY",$appkey);
                    $changeStatusz->bindParam("APPOWNERKEY",$acckey);
                    if ($changeStatusz->execute())
                    {
                                                            $typeofi = "Register";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                        echo "success";
                            $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                    }
                }
                else
                {
                    die("key_not_found");
                }
            }
        }
        else
        {
            $checkK = $database->prepare("SELECT * FROM userskeys WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND STATUS = :STATUS");
            $checkK->bindParam("KEYID",$invite);
            $checkK->bindParam("APPKEY",$appkey);
            $checkK->bindParam("STATUS",$status);
            $checkK->execute();
        $keyinfo = $checkK->fetchObject();
        $Rank = $keyinfo->RANKUSER;
        $EXPIREdate = expire_t($keyinfo->EXPIREDATE);
        $timenoww =time();
        $RegUser = $database->prepare("INSERT INTO accounts(NAME,PASSWORD,RANKUSER,CREATEDATE,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:NAME,:PASSWORD,:RANKUSER,:CREATEDATE,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
        $RegUser->bindParam("NAME",$name);
        $RegUser->bindParam("PASSWORD",$password);
        $RegUser->bindParam("RANKUSER",$Rank);
        $RegUser->bindParam("CREATEDATE",$timenoww);
        $RegUser->bindParam("EXPIREDATE",$EXPIREdate);
        $RegUser->bindParam("APPKEY",$appkey);
        $RegUser->bindParam("APPOWNERKEY",$acckey);
        if ($RegUser->execute())
        {
            $changeStatusw = $database->prepare("UPDATE userskeys SET STATUS = :UNSTATUS WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND APPOWNERKEY = :APPOWNERKEY");
            $changeStatusw->bindParam("UNSTATUS",$unstatus);
            $changeStatusw->bindParam("KEYID",$invite);
            $changeStatusw->bindParam("APPKEY",$appkey);
            $changeStatusw->bindParam("APPOWNERKEY",$acckey);
            if ($changeStatusw->execute())
            {
                                                    $typeofi = "Register";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
            echo "success";
                $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
            $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
            $getINFO->bindParam(":appkey",$appkey);
            $getINFO->execute();
            $appinfo = $getINFO->fetchObject();
            if ($appinfo->DISC == "1")
            {
                $webhookurl = $appinfo->DISCWH;
                $timestamp = date("c", strtotime("now"));
                $json_data = json_encode([
                    "content" => "_ _",
                    "tts" => false,
                    "embeds" => [
                        [
                            "title" => ":pencil: Sign up Detected",
                            "type" => "rich",
                            "timestamp" => $timestamp,
                            "color" => hexdec( "3366ff" ),
                            "footer" => [
                                "text" => "Umar - The Ultimate Solution",
                                "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                            ],
                            "image" => [
                                "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                            ],
                            "fields" => [
                                [
                                    "name" => "• Username :",
                                    "value" => '```' . $name . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• Rank :",
                                    "value" => '```' . $key_infow->RANKUSER . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• Create Date :",
                                    "value" => '```' . $timenoww . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• Expire Date :",
                                    "value" => '```' . convert_t(expire_t($key_infow->EXPIREDATE), $acckey, $database, $altashfir, $tabadal) . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• Key Used :",
                                    "value" => '```' . $invite . '```',
                                    "inline" => true
                                ],
                            ]
                        ]
                    ]
                
                ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                $ch = curl_init( $webhookurl );
                curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                curl_setopt( $ch, CURLOPT_POST, 1);
                curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt( $ch, CURLOPT_HEADER, 0);
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                $response = curl_exec( $ch );
                curl_close( $ch );
            }
            }
        }
        else
        {
            die("key_not_found");
        }
        }
    }
    else
    {
        die("key_not_found");
    }
    }
}




//variable grabber



else if ($type === 'variable')
{
    if ($_POST['varid'])
    {
            $varid = $_POST['varid'];
            $appkey = $_POST['appkey'];
            $acckey = $_POST['acckey'];
    }
    else
    {
            $varid = $_GET['varid'];
            $appkey = $_GET['appkey'];
            $acckey = $_GET['acckey'];
    }
$findowner = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :appownerkey");
$findowner->bindParam(":appownerkey",$acckey);
$findowner->execute();
if ($findowner->rowCount() === 1)
{
$isqlreq = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
$isqlreq->bindParam(":appkey",$appkey);
$isqlreq->execute();
if ($isqlreq->rowCount() === 1)
{
    $variableFIND = $database->prepare("SELECT * FROM vars WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey AND VARID = :varid");
    $variableFIND->bindParam(":appkey",$appkey);
    $variableFIND->bindParam(":appownerkey",$acckey);
    $variableFIND->bindParam(":varid",$varid);
    $variableFIND->execute();
    $varVALUE = $variableFIND->fetchObject();
    if ($variableFIND->rowCount() === 1)
    {
                                            $typeofi = "Grabbing Variable";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
        die ($varVALUE->VARVALUE);
            $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
    }
    else
    {
        die("var_not_found");
    }
}
else
{
    die("incorrect_application_key");
}
}
else
{
    die("incorrect_account_key");
}
}






//hardware id reset



else if ($type === 'hwidreset')
{
    if ($_POST['username'])
    {
        $username = $_POST['username'];
        $appid = $_POST['appid'];
        $appkey = $_POST['appkey'];
        $acckey = $_POST['acckey'];
    }
    else
    {
        $username = $_GET['username'];
        $appid = $_GET['appid'];
        $appkey = $_GET['appkey'];
        $acckey = $_GET['acckey'];
    }
    
    $username = str_replace(' ', '', $username);
$findowner = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :appownerkey");
$findowner->bindParam(":appownerkey",$acckey);
$findowner->execute();
if ($findowner->rowCount() === 1)
{
$isqlreq = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey AND APPID = :appid");
$isqlreq->bindParam(":appkey",$appkey);
$isqlreq->bindParam(":appid",$appid);
$isqlreq->execute();
if ($isqlreq->rowCount() === 1)
{
    $variableUSER = $database->prepare("SELECT * FROM accounts WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey AND NAME = :name");
    $variableUSER->bindParam(":appkey",$appkey);
    $variableUSER->bindParam(":appownerkey",$acckey);
    $variableUSER->bindParam(":name",$username);
    $variableUSER->execute();
    if ($variableUSER->rowCount() > 0)
    {
                                            $typeofi = "Reseting HWID for: " . $username;
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
    $hwidnull = "NULL";
                $changeStatusw = $database->prepare("UPDATE accounts SET HWID = :hwid WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey AND NAME = :name");
            $changeStatusw->bindParam(":hwid",$hwidnull);
    $changeStatusw->bindParam(":appkey",$appkey);
    $changeStatusw->bindParam(":appownerkey",$acckey);
    $changeStatusw->bindParam(":name",$username);
            if ($changeStatusw->execute())
            {
                echo ("success");
                    $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
            }
    }
    else
    {
        die("user_not_found");
    }
}
else
{
    die("incorrect_application_info");
}
}
else
{
    die("incorrect_account_key");
}
}




//upgrade sub




else if ($type === 'upgrade')
{
    $username = $_POST['username'];
    $username = str_replace(' ', '', $username);
    $key = $_POST['key'];
    $appkey = $_POST['appkey'];
    $acckey = $_POST['acckey'];
$findowner = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :appownerkey");
$findowner->bindParam(":appownerkey",$acckey);
$findowner->execute();
if ($findowner->rowCount() === 1)
{
$isqlreq = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
$isqlreq->bindParam(":appkey",$appkey);
$isqlreq->execute();
if ($isqlreq->rowCount() === 1)
{
    $variableUSER = $database->prepare("SELECT * FROM accounts WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey AND NAME = :name");
    $variableUSER->bindParam(":appkey",$appkey);
    $variableUSER->bindParam(":appownerkey",$acckey);
    $variableUSER->bindParam(":name",$username);
    $variableUSER->execute();
    $userInfo = $variableUSER->fetchObject();
    if ($variableUSER->rowCount() > 0)
    {
        $st = "UNAPPLIED";
        $ust = "APPLIED";
            $variableKEY = $database->prepare("SELECT * FROM userskeys WHERE APPKEY = :appkey AND STATUS = :status AND APPOWNERKEY = :appownerkey AND KEYID = :keyid");
    $variableKEY->bindParam(":appkey",$appkey);
    $variableKEY->bindParam(":appownerkey",$acckey);
    $variableKEY->bindParam(":keyid",$key);
    $variableKEY->bindParam(":status",$st);
    $variableKEY->execute();
    $keyInfo = $variableKEY->fetchObject();
    if ($variableKEY->rowCount() > 0)
    {
                                                    $typeofi = "Upgrading sub for: " . $username;
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
    $uexp = $userInfo->EXPIREDATE;
    $kexp = $keyInfo->EXPIREDATE;
    if ($kexp == "Lifetime")
    {
        $uexp = $kexp;
    }
    else
    {
        if (time() >= $uexp)
        {
            $uexp = strtotime($kexp);
        }
        else
        {
            $uexp = strtotime($kexp, $uexp);
        }
    }
                $changeStatusw = $database->prepare("UPDATE accounts SET EXPIREDATE = :expiredate WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey AND NAME = :name");
            $changeStatusw->bindParam(":expiredate",$uexp);
    $changeStatusw->bindParam(":appkey",$appkey);
    $changeStatusw->bindParam(":appownerkey",$acckey);
    $changeStatusw->bindParam(":name",$username);
            if ($changeStatusw->execute())
            {
                                $changeStatusw = $database->prepare("UPDATE userskeys SET STATUS = :status WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey AND KEYID = :keyid");
            $changeStatusw->bindParam(":status",$ust);
    $changeStatusw->bindParam(":appkey",$appkey);
    $changeStatusw->bindParam(":appownerkey",$acckey);
    $changeStatusw->bindParam(":keyid",$key);
            if ($changeStatusw->execute())
            {
                echo ("success");
                    $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
            }
            }
    }
    else
    {
        die("key_not_found");
    }
    }
    else
    {
        die("user_not_found");
    }
}
else
{
    die("incorrect_application_key");
}
}
else
{
    die("incorrect_account_key");
}
}



//output cred from discord id



else if ($type == 'discordappkey')
{
    if ($_POST['userid'])
    {
            $userid = $_POST['userid'];
    }
    else
    {
            $userid = $_GET['userid'];
    }
$findowner = $database->prepare("SELECT * FROM discord WHERE USERID = :userid");
$findowner->bindParam(":userid",$userid);
$findowner->execute();
if ($findowner->rowCount() === 1)
{
    $data = $findowner->fetchObject();
    echo $data->APPKEY;
}
}
else if ($type == 'discordacckey')
{
    if ($_POST['userid'])
    {
            $userid = $_POST['userid'];
    }
    else
    {
            $userid = $_GET['userid'];
    }
$findowner = $database->prepare("SELECT * FROM discord WHERE USERID = :userid");
$findowner->bindParam(":userid",$userid);
$findowner->execute();
if ($findowner->rowCount() === 1)
{
    $data = $findowner->fetchObject();
    echo $data->APPOWNERKEY;
}
}
else if ($type == 'discordappid')
{
    if ($_POST['userid'])
    {
            $userid = $_POST['userid'];
    }
    else
    {
            $userid = $_GET['userid'];
    }
$findowner = $database->prepare("SELECT * FROM discord WHERE USERID = :userid");
$findowner->bindParam(":userid",$userid);
$findowner->execute();
if ($findowner->rowCount() === 1)
{
    $data = $findowner->fetchObject();
    $findapp = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
$findapp->bindParam(":appkey",$data->APPKEY);
$findapp->execute();
if ($findapp->rowCount() === 1)
{
    $data = $findapp->fetchObject();
    echo $data->APPID;
}
}
}
else if ($type == 'genkey')
{
    function getexp($ex)
    {
        if ($ex == "lifetime")
        {
            return "Lifetime";
        }
        if ($ex == "Lifetime")
        {
            return $ex;
        }
        else
        {
            return '+' . $ex;
        }
    }
    if ($_POST['appid'])
    {
            $appid = $_POST['appid'];
            $appkey = $_POST['appkey'];
            $acckey = $_POST['acckey'];
            $rank = $_POST['rank'];
            $expire = getexp($_POST['expire']);
    }
    else
    {
            $appid = $_GET['appid'];
            $appkey = $_GET['appkey'];
            $acckey = $_GET['acckey'];
            $rank = $_GET['rank'];
            $expire = getexp($_GET['expire']);
    }
    $findowner = $database->prepare("SELECT * FROM apps WHERE APPID = :appid");
$findowner->bindParam(":appid",$appid);
$findowner->execute();
$data = $findowner->fetchObject();
if ($data->APPKEY == $appkey)
{
          function randString() {
              $length = '15';
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
        $outkey = randString();
        $addKey = $database->prepare("INSERT INTO userskeys(KEYID,RANKUSER,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:KEYID,:RANKUSER,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
        $addKey->bindParam("KEYID",$outkey);
        $addKey->bindParam("RANKUSER",$rank);
        $addKey->bindParam("EXPIREDATE",$expire);
        $addKey->bindParam("APPKEY",$appkey);
        $addKey->bindParam("APPOWNERKEY",$acckey);
        $addKey->execute();
        echo $outkey;
}
}
else if ($type == 'delkey')
{
        if ($_POST['appid'])
    {
            $appid = $_POST['appid'];
            $appkey = $_POST['appkey'];
            $acckey = $_POST['acckey'];
            $key = $_POST['keyid'];
    }
    else
    {
            $appid = $_GET['appid'];
            $appkey = $_GET['appkey'];
            $acckey = $_GET['acckey'];
            $key = $_GET['keyid'];
    }
    $findowner = $database->prepare("SELECT * FROM apps WHERE APPID = :appid");
$findowner->bindParam(":appid",$appid);
$findowner->execute();
$data = $findowner->fetchObject();
if ($data->APPKEY == $appkey)
{
                $checkName = $database->prepare("SELECT * FROM userskeys WHERE KEYID = :keyid AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkName->bindParam("keyid",$key);
    $checkName->bindParam("appkey",$appkey);
    $checkName->bindParam("appownerkey",$acckey);
    $checkName->execute();
    if ($checkName->rowCount() < 1)
    {
        die("`Error`"." :x:");
    }
        $addKey = $database->prepare("DELETE FROM userskeys WHERE KEYID = :keyid AND appkey = :appkey AND appownerkey = :appownerkey");
        $addKey->bindParam(":keyid",$key);
        $addKey->bindParam(":appkey",$appkey);
        $addKey->bindParam(":appownerkey",$acckey);
        if ($addKey->execute())
        {
            echo "`Operation successful`"." :white_check_mark:";
        }
        else
        {
            echo "`Error`"." :x:";
        }
}
}
else if ($type == 'delvar')
{
        if ($_POST['appid'])
    {
            $appid = $_POST['appid'];
            $appkey = $_POST['appkey'];
            $acckey = $_POST['acckey'];
            $key = $_POST['varname'];
    }
    else
    {
            $appid = $_GET['appid'];
            $appkey = $_GET['appkey'];
            $acckey = $_GET['acckey'];
            $key = $_GET['varname'];
    }
    $findowner = $database->prepare("SELECT * FROM apps WHERE APPID = :appid");
$findowner->bindParam(":appid",$appid);
$findowner->execute();
$data = $findowner->fetchObject();
if ($data->APPKEY == $appkey)
{
            $checkName = $database->prepare("SELECT * FROM vars WHERE VARNAME = :varname AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkName->bindParam("varname",$key);
    $checkName->bindParam("appkey",$appkey);
    $checkName->bindParam("appownerkey",$acckey);
    $checkName->execute();
    if ($checkName->rowCount() < 1)
    {
        die("`Error`"." :x:");
    }
        $addKey = $database->prepare("DELETE FROM vars WHERE VARNAME = :varname AND appkey = :appkey AND appownerkey = :appownerkey");
        $addKey->bindParam(":varname",$key);
        $addKey->bindParam(":appkey",$appkey);
        $addKey->bindParam(":appownerkey",$acckey);
        if ($addKey->execute())
        {
            echo "`Operation successful`"." :white_check_mark:";
        }
        else
        {
            echo "`Error`"." :x:";
        }
}
}
else if ($type == 'addvar')
{
        if ($_POST['appid'])
    {
            $appid = $_POST['appid'];
            $appkey = $_POST['appkey'];
            $acckey = $_POST['acckey'];
            $Name = $_POST['varname'];
            $Value = $_POST['varvalue'];
    }
    else
    {
            $appid = $_GET['appid'];
            $appkey = $_GET['appkey'];
            $acckey = $_GET['acckey'];
            $Name = $_GET['varname'];
            $Value = $_GET['varvalue'];
    }
    $findowner = $database->prepare("SELECT * FROM apps WHERE APPID = :appid");
$findowner->bindParam(":appid",$appid);
$findowner->execute();
$data = $findowner->fetchObject();
if ($data->APPKEY == $appkey)
{
    function randString($length = 18) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
    $genkey = randString();

    if (!(empty($Name)))
    {
    if (!(empty($Value)))
    {
    $checkName = $database->prepare("SELECT * FROM vars WHERE VARNAME = :name AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkName->bindParam("name",$Name);
    $checkName->bindParam("appkey",$appkey);
    $checkName->bindParam("appownerkey",$acckey);
    $checkName->execute();
    if ($checkName->rowCount() > 0)
    {
        echo "`Error`"." :x:";
    }
  else
  {
    $checkA = $database->prepare("SELECT * FROM vars WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkA->bindParam("appkey",$appkey);
    $checkA->bindParam("appownerkey",$acckey);
    $checkA->execute();
    if ($_SESSION['user_data']->RANK === "0")
    {
      $num = $checkA->rowCount();
      if ($num >= 10)
      {
        echo "`The Application has reached the maximum of variables, it\'s time for an upgrade!`"." :x:";
      }
      else
      {
        $addUser = $database->prepare("INSERT INTO vars(VARID,VARNAME,VARVALUE,APPKEY,APPOWNERKEY) VALUES(:VARID,:VARNAME,:VARVALUE,:APPKEY,:APPOWNERKEY)");
        $addUser->bindParam("VARID",$genkey);
        $addUser->bindParam("VARNAME",$Name);
        $addUser->bindParam("VARVALUE",$Value);
        $addUser->bindParam("APPKEY",$appkey);
        $addUser->bindParam("APPOWNERKEY",$acckey);
        if ($addUser->execute())
        {
            echo "`Operation successful`"." :white_check_mark:";
        }
        else
        {
            echo "`Error`"." :x:";
        }
      }
    }
    else
    {
        $addUser = $database->prepare("INSERT INTO vars(VARID,VARNAME,VARVALUE,APPKEY,APPOWNERKEY) VALUES(:VARID,:VARNAME,:VARVALUE,:APPKEY,:APPOWNERKEY)");
        $addUser->bindParam("VARID",$genkey);
        $addUser->bindParam("VARNAME",$Name);
        $addUser->bindParam("VARVALUE",$Value);
        $addUser->bindParam("APPKEY",$appkey);
        $addUser->bindParam("APPOWNERKEY",$acckey);
        if ($addUser->execute())
        {
            echo "`Operation successful`"." :white_check_mark:";
        }
        else
        {
            echo "`Error`"." :x:";
        }
      }
    }
  }
  }
}
}
else if ($type == 'deluser')
{
        if ($_POST['appid'])
    {
            $appid = $_POST['appid'];
            $appkey = $_POST['appkey'];
            $acckey = $_POST['acckey'];
            $username = $_POST['username'];
    }
    else
    {
            $appid = $_GET['appid'];
            $appkey = $_GET['appkey'];
            $acckey = $_GET['acckey'];
            $username = $_GET['username'];
    }
    $findowner = $database->prepare("SELECT * FROM apps WHERE APPID = :appid");
$findowner->bindParam(":appid",$appid);
$findowner->execute();
$data = $findowner->fetchObject();
if ($data->APPKEY == $appkey)
{
        $checkName = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkName->bindParam("name",$username);
    $checkName->bindParam("appkey",$appkey);
    $checkName->bindParam("appownerkey",$acckey);
    $checkName->execute();
    if ($checkName->rowCount() < 1)
    {
        die("`Error`"." :x:");
    }
        $addKey = $database->prepare("DELETE FROM accounts WHERE NAME = :username AND appkey = :appkey AND appownerkey = :appownerkey");
        $addKey->bindParam(":username",$username);
        $addKey->bindParam(":appkey",$appkey);
        $addKey->bindParam(":appownerkey",$acckey);
        if ($addKey->execute())
        {
            echo "`Operation successful`"." :white_check_mark:";
        }
        else
        {
            echo "`Error`"." :x:";
        }
}
}
else if ($type == 'adduser')
{
        function getexp($ex)
    {
        if ($ex == "lifetime")
        {
            return "Lifetime";
        }
        if ($ex == "Lifetime")
        {
            return $ex;
        }
        else
        {
            return strtotime('+' . $ex);
        }
    }
    if ($_POST['appid'])
    {
            $appid = $_POST['appid'];
            $appkey = $_POST['appkey'];
            $acckey = $_POST['acckey'];
            $Username = $_POST['username'];
            $Password = $_POST['password'];
            $Rank = $_POST['rank'];
            $USERexpireDATE = getexp($_POST['expire']);
    }
    else
    {
            $appid = $_GET['appid'];
            $appkey = $_GET['appkey'];
            $acckey = $_GET['acckey'];
            $Username = $_GET['username'];
            $Password = $_GET['password'];
            $Rank = $_GET['rank'];
            $USERexpireDATE = getexp($_GET['expire']);
    }
    $findowner = $database->prepare("SELECT * FROM apps WHERE APPID = :appid");
$findowner->bindParam(":appid",$appid);
$findowner->execute();
$data = $findowner->fetchObject();
if ($data->APPKEY == $appkey)
{
    $checkName = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkName->bindParam("name",$Username);
    $checkName->bindParam("appkey",$appkey);
    $checkName->bindParam("appownerkey",$acckey);
    $checkName->execute();
    if ($checkName->rowCount() > 0)
    {
        die("`Error`"." :x:");
  }
  else
  {
    $checkA = $database->prepare("SELECT * FROM accounts WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
    $checkA->bindParam("appkey",$appkey);
    $checkA->bindParam("appownerkey",$acckey);
    $checkA->execute();
    $timenow =time();
    $checkUR = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :acckey");
    $checkUR->bindParam("acckey",$acckey);
    $checkUR->execute();
    $dataR = $checkUR->fetchObject();
    if ($dataR->RANK === "0")
    {
      $num = $checkA->rowCount();
      if ($num >= 100)
      {
        echo "`The Application has reached the maximum of users, it\'s time for an upgrade!`"." :x:";
      }
      else
      {
        $Username = str_replace(' ', '', $Username);
        $Password = str_replace(' ', '', $Password);
        $addUser = $database->prepare("INSERT INTO accounts(NAME,PASSWORD,RANKUSER,CREATEDATE,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:NAME,:PASSWORD,:RANKUSER,:CREATEDATE,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
        $addUser->bindParam("NAME",$Username);
        $addUser->bindParam("PASSWORD",hash($Secure_Hash_Algorithm,$Password));
        $addUser->bindParam("RANKUSER",$Rank);
        $addUser->bindParam("CREATEDATE",$timenow);
        $addUser->bindParam("EXPIREDATE",$USERexpireDATE);
        $addUser->bindParam("APPKEY",$appkey);
        $addUser->bindParam("APPOWNERKEY",$acckey);
        if ($addUser->execute())
        {
            echo "`Operation successful`"." :white_check_mark:";
        }
        else
        {
            echo "`Error`"." :x:";
        }
      }
    }
    else
    {
        $Username = str_replace(' ', '', $Username);
        $Password = str_replace(' ', '', $Password);
        $addUser = $database->prepare("INSERT INTO accounts(NAME,PASSWORD,RANKUSER,CREATEDATE,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:NAME,:PASSWORD,:RANKUSER,:CREATEDATE,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
        $addUser->bindParam("NAME",$Username);
        $addUser->bindParam("PASSWORD",hash($Secure_Hash_Algorithm,$Password));
        $addUser->bindParam("RANKUSER",$Rank);
        $addUser->bindParam("CREATEDATE",$timenow);
        $addUser->bindParam("EXPIREDATE",$USERexpireDATE);
        $addUser->bindParam("APPKEY",$appkey);
        $addUser->bindParam("APPOWNERKEY",$acckey);
        if ($addUser->execute())
        {
            echo "`Operation successful`"." :white_check_mark:";
        }
        else
        {
            echo "`Error`"." :x:";
        }
      }
    }
}
}
else if ($type == 'keydata')
{
        if ($_POST['appid'])
    {
            $appid = $_POST['appid'];
            $appkey = $_POST['appkey'];
            $acckey = $_POST['acckey'];
            $key = $_POST['keyid'];
    }
    else
    {
            $appid = $_GET['appid'];
            $appkey = $_GET['appkey'];
            $acckey = $_GET['acckey'];
            $key = $_GET['keyid'];
    }
    $findowner = $database->prepare("SELECT * FROM apps WHERE APPID = :appid");
$findowner->bindParam(":appid",$appid);
$findowner->execute();
$data = $findowner->fetchObject();
if ($data->APPKEY == $appkey)
{
        $addKey = $database->prepare("SELECT * FROM userskeys WHERE KEYID = :keyid AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
        $addKey->bindParam(":keyid",$key);
        $addKey->bindParam(":appkey",$appkey);
        $addKey->bindParam(":appownerkey",$acckey);
        if ($addKey->execute())
        {
            if ($addKey->rowCount() > 0)
            {
                $data = $addKey->fetchObject();
                echo 'Key ID: `'.$data->KEYID.'`|Rank: `'.$data->RANKUSER.'`|Expire Date: `'.str_replace("+", "", $data->EXPIREDATE).'`|Status: `'.$data->STATUS.'`';
            }
                    else
        {
            echo "`Error`"." :x:";
        }
        }
        else
        {
            echo "`Error`"." :x:";
        }
}
}
else if ($type == 'userdata')
{
        if ($_POST['appid'])
    {
            $appid = $_POST['appid'];
            $appkey = $_POST['appkey'];
            $acckey = $_POST['acckey'];
            $key = $_POST['username'];
    }
    else
    {
            $appid = $_GET['appid'];
            $appkey = $_GET['appkey'];
            $acckey = $_GET['acckey'];
            $key = $_GET['username'];
    }
    $findowner = $database->prepare("SELECT * FROM apps WHERE APPID = :appid");
$findowner->bindParam(":appid",$appid);
$findowner->execute();
$data = $findowner->fetchObject();
if ($data->APPKEY == $appkey)
{
        $addKey = $database->prepare("SELECT * FROM accounts WHERE NAME = :keyid AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
        $addKey->bindParam(":keyid",$key);
        $addKey->bindParam(":appkey",$appkey);
        $addKey->bindParam(":appownerkey",$acckey);
        if ($addKey->execute())
        {
            if ($addKey->rowCount() > 0)
            {
                $data = $addKey->fetchObject();
                echo 'Username: `'.$data->NAME.'`|Rank: `'.$data->RANKUSER.'`|Create Date: `'.convert_t($data->CREATEDATE, $acckey, $database, $altashfir, $tabadal).'`|Expire Date: `'.convert_t($data->EXPIREDATE, $acckey, $database, $altashfir, $tabadal).'`|Hardware ID: `'.$data->HWID.'`';
            }
                    else
        {
            echo "`Error`"." :x:";
        }
        }
        else
        {
            echo "`Error`"." :x:";
        }
}
}





//other for official sdks...




else if ($type === '-RS-_-IJ--JH--IJ--ON-')
{
$appkey = $_POST['111110'];
$acckey = $_POST['001011'];
$appkey = str_replace($tabadal, $altashfir, $appkey);
$acckey = str_replace($tabadal, $altashfir, $acckey);
$appkey = str_replace($tabadal, $altashfir, $appkey);
$acckey = str_replace($tabadal, $altashfir, $acckey);
$findowner = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :appownerkey");
$findowner->bindParam(":appownerkey",$acckey);
$findowner->execute();
if ($findowner->rowCount() === 1)
{
$isqlreq = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
$isqlreq->bindParam(":appkey",$appkey);
$isqlreq->execute();
if ($isqlreq->rowCount() === 1)
{
    $typeofi = "Initialization";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
    $stmt = $database->prepare('SELECT * FROM apps WHERE APPKEY = :appkey');
    $stmt->bindParam(":appkey",$appkey);
    $stmt->execute();
    $rows = [];
    foreach ($stmt as $row) {
        $rows[] = [
            'APPNAME' => $row['APPNAME'],
            'STATUS' => $row['STATUS'],
            'LOGGED' => $row['LOGGED'],
            'REGISTERED' => $row['REGISTERED'],
            'PAUSED' => $row['PAUSED'],
        ];
    }
    $the_resylT = json_encode($rows);
    echo str_replace($altashfir, $tabadal, $the_resylT);
        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
    $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
    $getINFO->bindParam(":appkey",$appkey);
    $getINFO->execute();
    $appinfo = $getINFO->fetchObject();
    if ($appinfo->DISC == "1")
    {
        $webhookurl = $appinfo->DISCWH;
        $timestamp = date("c", strtotime("now"));
        $json_data = json_encode([
            "content" => "*You have a new alert from your Website!*",
            "tts" => false,
            "embeds" => [
                [
                    "title" => ":grey_question: New connection Detected",
                    "type" => "rich",
                    "timestamp" => $timestamp,
                    "color" => hexdec( "3366ff" ),
                    "footer" => [
                        "text" => "Umar - The Ultimate Solution",
                        "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                    ],
                    "image" => [
                        "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                    ],
                    "fields" => [
                    ]
                ]
            ]
        
        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
        $ch = curl_init( $webhookurl );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec( $ch );
        curl_close( $ch );
    }
}
else
{
    die("-JH--MW--ON-_-EV--MW--WK--JH-MV-");
}
}
else
{
    die("-JH--MW--ON-_-EV--MW--WK--JH-MV-");
}
}
else if ($type === '-IJ--JH--IJ--ON-')
{
$appkey = $_POST['111110'];
$acckey = $_POST['001011'];
$hwid = $_POST['011001'];
$appkey = str_replace($tabadal, $altashfir, $appkey);
$acckey = str_replace($tabadal, $altashfir, $acckey);
$hwid = str_replace($tabadal, $altashfir, $hwid);
$findowner = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :appownerkey");
$findowner->bindParam(":appownerkey",$acckey);
$findowner->execute();
if ($findowner->rowCount() === 1)
{
$isqlreq = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
$isqlreq->bindParam(":appkey",$appkey);
$isqlreq->execute();
if ($isqlreq->rowCount() === 1)
{
        $typeofi = "Initialization";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
    $stmt = $database->prepare('SELECT * FROM apps WHERE APPKEY = :appkey');
    $stmt->bindParam(":appkey",$appkey);
    $stmt->execute();
    $rows = [];
    foreach ($stmt as $row) {
        $rows[] = [
            'APPNAME' => $row['APPNAME'],
            'STATUS' => $row['STATUS'],
            'LOGGED' => $row['LOGGED'],
            'REGISTERED' => $row['REGISTERED'],
            'PAUSED' => $row['PAUSED'],
        ];
    }
    $the_resylT = json_encode($rows);
    echo str_replace($altashfir, $tabadal, $the_resylT);
        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
    $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
    $getINFO->bindParam(":appkey",$appkey);
    $getINFO->execute();
    $appinfo = $getINFO->fetchObject();
    if ($appinfo->DISC == "1")
    {
        $webhookurl = $appinfo->DISCWH;
        $timestamp = date("c", strtotime("now"));
        $json_data = json_encode([
            "content" => "*You have a new alert from your Application!*",
            "tts" => false,
            "embeds" => [
                [
                    "title" => ":grey_question: New connection Detected",
                    "type" => "rich",
                    "timestamp" => $timestamp,
                    "color" => hexdec( "3366ff" ),
                    "footer" => [
                        "text" => "Umar - The Ultimate Solution",
                        "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                    ],
                    "image" => [
                        "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                    ],
                    "fields" => [
                    ]
                ]
            ]
        
        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
        $ch = curl_init( $webhookurl );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec( $ch );
        curl_close( $ch );
    }
}
else
{
    die("-IJ--JH--OL--MW--ME--ME--RS--OL--ON-_-XC--IS--IS--TW--IJ--OL--XC--ON--IJ--MW--JH-_MV--RS--ON--XC--IJ--TW--EP-");
}
}
else
{
    die("-IJ--JH--OL--MW--ME--ME--RS--OL--ON-_-XC--IS--IS--TW--IJ--OL--XC--ON--IJ--MW--JH-_MV--RS--ON--XC--IJ--TW--EP-");
}
}
else if ($type === '-RS-_-TW--QZ--WZ--IJ--JH-')
{
$username = $_POST['username'];
$password = $_POST['passw0rd'];
$appkey = $_POST['appkey'];
$acckey = $_POST['acckey'];
$username = str_replace($tabadal, $altashfir, $username);
$password = str_replace($tabadal, $altashfir, $password);
$appkey = str_replace($tabadal, $altashfir, $appkey);
$acckey = str_replace($tabadal, $altashfir, $acckey);
$sqlreq = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
$sqlreq->bindParam(":name",$username);
$sqlreq->bindParam(":password",$password);
$sqlreq->bindParam(":appkey",$appkey);
$sqlreq->bindParam(":appownerkey",$acckey);
$sqlreq->execute();
if ($sqlreq->rowCount() === 1)
{
    $timenow =time();
    $userinfo = $sqlreq->fetchObject();
    if ($userinfo->EXPIREDATE == "Lifetime")
    {
            $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                $stmt->bindParam(":name",$username);
                $stmt->bindParam(":password",$password);
                $stmt->bindParam(":appkey",$appkey);
                $stmt->bindParam(":appownerkey",$acckey);
                $stmt->execute();
                $rows = [];
                foreach ($stmt as $row) {
                    $rows[] = [
                        'NAME' => $row['NAME'],
                        'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                        'HWID' => $row['HWID'],
                    ];
                }
                $the_resylT = json_encode($rows);
                echo str_replace($altashfir, $tabadal, $the_resylT);
                                $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
                    $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                $getINFO->bindParam(":appkey",$appkey);
                $getINFO->execute();
                $appinfo = $getINFO->fetchObject();
                if ($appinfo->DISC == "1")
                {
                    $webhookurl = $appinfo->DISCWH;
                    $timestamp = date("c", strtotime("now"));
                    $json_data = json_encode([
                        "content" => "_ _",
                        "tts" => false,
                        "embeds" => [
                            [
                                "title" => ":white_check_mark: Sign in Detected",
                                "type" => "rich",
                                "timestamp" => $timestamp,
                                "color" => hexdec( "3366ff" ),
                                "footer" => [
                                    "text" => "Umar - The Ultimate Solution",
                                    "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                ],
                                "image" => [
                                    "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                ],
                                "fields" => [
                                    [
                                        "name" => "• Username :",
                                        "value" => '```' . $userinfo->NAME . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Rank :",
                                        "value" => '```' . $userinfo->RANKUSER . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Create Date :",
                                        "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Expire Date :",
                                        "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                ]
                            ]
                        ]
                    
                    ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                    $ch = curl_init( $webhookurl );
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                    curl_setopt( $ch, CURLOPT_POST, 1);
                    curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt( $ch, CURLOPT_HEADER, 0);
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                    $response = curl_exec( $ch );
                    curl_close( $ch );
                }
    }
    else
    {
        $expireW = $userinfo->EXPIREDATE;
        $dateW = time();
        if (intval($expireW) > intval($dateW))
        {
            if ($userinfo->HWID == "NULL")
            {
                            $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    
    $RegInitw->bindParam("TYPE",$typeofi);
    
    $RegInitw->bindParam("DATE",$timenowww);
    
    $RegInitw->bindParam("APPKEY",$appkey);
    
    
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    
    $RegInitw->execute();
                    $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                    $stmt->bindParam(":name",$username);
                    $stmt->bindParam(":password",$password);
                    $stmt->bindParam(":appkey",$appkey);
                    $stmt->bindParam(":appownerkey",$acckey);
                    $stmt->execute();
                    $rows = [];
                    foreach ($stmt as $row) {
                        $rows[] = [
                            'NAME' => $row['NAME'],
                            'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                            'HWID' => $row['HWID'],
                        ];
                    }
                    $the_klesylT = json_encode($rows);
                    echo str_replace($altashfir, $tabadal, $the_klesylT);
                                    $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
                        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                    $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                    $getINFO->bindParam(":appkey",$appkey);
                    $getINFO->execute();
                    $appinfo = $getINFO->fetchObject();
                    if ($appinfo->DISC == "1")
                    {
                        $webhookurl = $appinfo->DISCWH;
                        $timestamp = date("c", strtotime("now"));
                        $json_data = json_encode([
                            "content" => "_ _",
                            "tts" => false,
                            "embeds" => [
                                [
                                    "title" => ":white_check_mark: Sign in Detected",
                                    "type" => "rich",
                                    "timestamp" => $timestamp,
                                    "color" => hexdec( "3366ff" ),
                                    "footer" => [
                                        "text" => "Umar - The Ultimate Solution",
                                        "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                    ],
                                    "image" => [
                                        "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                    ],
                                    "fields" => [
                                        [
                                            "name" => "• Username :",
                                            "value" => '```' . $userinfo->NAME . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Rank :",
                                            "value" => '```' . $userinfo->RANKUSER . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Create Date :",
                                            "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Expire Date :",
                                            "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                    ]
                                ]
                            ]
                        
                        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                        $ch = curl_init( $webhookurl );
                        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                        curl_setopt( $ch, CURLOPT_POST, 1);
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt( $ch, CURLOPT_HEADER, 0);
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                        $response = curl_exec( $ch );
                        curl_close( $ch );
                    }
            }
            else
            {
                $hwidlockop = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                $hwidlockop->bindParam(":appkey",$appkey);
                $hwidlockop->execute();
                $hwidlockinfo = $hwidlockop->fetchObject();
                    $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                    $stmt->bindParam(":name",$username);
                    $stmt->bindParam(":password",$password);
                    $stmt->bindParam(":appkey",$appkey);
                    $stmt->bindParam(":appownerkey",$acckey);
                    $stmt->execute();
                                $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                    $rows = [];
                    foreach ($stmt as $row) {
                        $rows[] = [
                            'NAME' => $row['NAME'],
                            'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                            'HWID' => $row['HWID'],
                        ];
                    }
                    $the_tnesylT = json_encode($rows);
                    echo str_replace($altashfir, $tabadal, $the_tnesylT);
                                    $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
                        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                    $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                    $getINFO->bindParam(":appkey",$appkey);
                    $getINFO->execute();
                    $appinfo = $getINFO->fetchObject();
                    if ($appinfo->DISC == "1")
                    {
                        $webhookurl = $appinfo->DISCWH;
                        $timestamp = date("c", strtotime("now"));
                        $json_data = json_encode([
                            "content" => "_ _",
                            "tts" => false,
                            "embeds" => [
                                [
                                    "title" => ":white_check_mark: Sign in Detected",
                                    "type" => "rich",
                                    "timestamp" => $timestamp,
                                    "color" => hexdec( "3366ff" ),
                                    "footer" => [
                                        "text" => "Umar - The Ultimate Solution",
                                        "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                    ],
                                    "image" => [
                                        "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                    ],
                                    "fields" => [
                                        [
                                            "name" => "• Username :",
                                            "value" => '```' . $userinfo->NAME . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Rank :",
                                            "value" => '```' . $userinfo->RANKUSER . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Create Date :",
                                            "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Expire Date :",
                                            "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                    ]
                                ]
                            ]
                        
                        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                        $ch = curl_init( $webhookurl );
                        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                        curl_setopt( $ch, CURLOPT_POST, 1);
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt( $ch, CURLOPT_HEADER, 0);
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                        $response = curl_exec( $ch );
                        curl_close( $ch );
                    }
            }
        }
        else
        {
            die("-RS--RE--IS--IJ--ME--RS-MV-");
        }
    }
}
else
{
    die("-JH--MW--ON-_-EV--MW--WK--JH-MV-");
}
}
else if ($type === '-TW--QZ--WZ--IJ--JH-')
{
$username = $_POST['username'];
$password = $_POST['passw0rd'];
$hwid = $_POST['hwid'];
$appkey = $_POST['appkey'];
$acckey = $_POST['acckey'];
$username = str_replace($tabadal, $altashfir, $username);
$password = str_replace($tabadal, $altashfir, $password);
$hwid = str_replace($tabadal, $altashfir, $hwid);
$appkey = str_replace($tabadal, $altashfir, $appkey);
$acckey = str_replace($tabadal, $altashfir, $acckey);
$sqlreq = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
$sqlreq->bindParam(":name",$username);
$sqlreq->bindParam(":password",$password);
$sqlreq->bindParam(":appkey",$appkey);
$sqlreq->bindParam(":appownerkey",$acckey);
$sqlreq->execute();
if ($sqlreq->rowCount() === 1)
{
    $timenow =time();
    $userinfo = $sqlreq->fetchObject();
    if ($userinfo->EXPIREDATE == "Lifetime")
    {
        if ($userinfo->HWID == "NULL")
        {
                        $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
            $changeHwid = $database->prepare("UPDATE accounts SET HWID = :hwid WHERE NAME = :name AND PASSWORD = :password");
            $changeHwid->bindParam(":name",$username);
            $changeHwid->bindParam(":hwid",$hwid);
            $changeHwid->bindParam(":password",$password);
            if ($changeHwid->execute())
            {
                $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                $stmt->bindParam(":name",$username);
                $stmt->bindParam(":password",$password);
                $stmt->bindParam(":appkey",$appkey);
                $stmt->bindParam(":appownerkey",$acckey);
                $stmt->execute();
                $rows = [];
                foreach ($stmt as $row) {
                    $rows[] = [
                        'NAME' => $row['NAME'],
                        'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                        'HWID' => $row['HWID'],
                    ];
                }
                $the_resylT = json_encode($rows);
                echo str_replace($altashfir, $tabadal, $the_resylT);
                                $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
                    $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                $getINFO->bindParam(":appkey",$appkey);
                $getINFO->execute();
                $appinfo = $getINFO->fetchObject();
                if ($appinfo->DISC == "1")
                {
                    $webhookurl = $appinfo->DISCWH;
                    $timestamp = date("c", strtotime("now"));
                    $json_data = json_encode([
                        "content" => "_ _",
                        "tts" => false,
                        "embeds" => [
                            [
                                "title" => ":white_check_mark: Sign in Detected",
                                "type" => "rich",
                                "timestamp" => $timestamp,
                                "color" => hexdec( "3366ff" ),
                                "footer" => [
                                    "text" => "Umar - The Ultimate Solution",
                                    "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                ],
                                "image" => [
                                    "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                ],
                                "fields" => [
                                    [
                                        "name" => "• Username :",
                                        "value" => '```' . $userinfo->NAME . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Rank :",
                                        "value" => '```' . $userinfo->RANKUSER . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Create Date :",
                                        "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Expire Date :",
                                        "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• HWID :",
                                        "value" => '```' . $userinfo->HWID . '```',
                                        "inline" => true
                                    ],
                                ]
                            ]
                        ]
                    
                    ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                    $ch = curl_init( $webhookurl );
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                    curl_setopt( $ch, CURLOPT_POST, 1);
                    curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt( $ch, CURLOPT_HEADER, 0);
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                    $response = curl_exec( $ch );
                    curl_close( $ch );
                }

            }
            else
            {
                die("-IJ--JH--OL--MW--ME--ME--RS--OL--ON-_-WK--EP--RS--ME-_MV--RS--ON--XC--IJ--TW--EP-");
            }
        }
        else
        {
            $hwidlockop = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
            $hwidlockop->bindParam(":appkey",$appkey);
            $hwidlockop->execute();
            $hwidlockinfo = $hwidlockop->fetchObject();
            if ($hwidlockinfo->HWID == "1")
            {
                if ($userinfo->HWID == $hwid)
                {
                                $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                    $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                    $stmt->bindParam(":name",$username);
                    $stmt->bindParam(":password",$password);
                    $stmt->bindParam(":appkey",$appkey);
                    $stmt->bindParam(":appownerkey",$acckey);
                    $stmt->execute();
                    $rows = [];
                    foreach ($stmt as $row) {
                        $rows[] = [
                            'NAME' => $row['NAME'],
                            'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                            'HWID' => $row['HWID'],
                        ];
                    }
                    $the_rresylT = json_encode($rows);
                    echo str_replace($altashfir, $tabadal, $the_rresylT);
                                    $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
                        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                    $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                    $getINFO->bindParam(":appkey",$appkey);
                    $getINFO->execute();
                    $appinfo = $getINFO->fetchObject();
                    if ($appinfo->DISC == "1")
                    {
                        $webhookurl = $appinfo->DISCWH;
                        $timestamp = date("c", strtotime("now"));
                        $json_data = json_encode([
                            "content" => "_ _",
                            "tts" => false,
                            "embeds" => [
                                [
                                    "title" => ":white_check_mark: Sign in Detected",
                                    "type" => "rich",
                                    "timestamp" => $timestamp,
                                    "color" => hexdec( "3366ff" ),
                                    "footer" => [
                                        "text" => "Umar - The Ultimate Solution",
                                        "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                    ],
                                    "image" => [
                                        "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                    ],
                                    "fields" => [
                                        [
                                            "name" => "• Username :",
                                            "value" => '```' . $userinfo->NAME . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Rank :",
                                            "value" => '```' . $userinfo->RANKUSER . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Create Date :",
                                            "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Expire Date :",
                                            "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• HWID :",
                                            "value" => '```' . $userinfo->HWID . '```',
                                            "inline" => true
                                        ],
                                    ]
                                ]
                            ]
                        
                        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                        $ch = curl_init( $webhookurl );
                        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                        curl_setopt( $ch, CURLOPT_POST, 1);
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt( $ch, CURLOPT_HEADER, 0);
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                        $response = curl_exec( $ch );
                        curl_close( $ch );
                    }
                }
                else if ($userinfo->HWID != $hwid)
                {
                    die("DP--BA--IJ-MV-_MV--MW--RS--EP-_-JH--MW--ON-_-BI--XC--ON--OL-DP-");
                }
            }
            else
            {
                            $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                $stmt->bindParam(":name",$username);
                $stmt->bindParam(":password",$password);
                $stmt->bindParam(":appkey",$appkey);
                $stmt->bindParam(":appownerkey",$acckey);
                $stmt->execute();
                $rows = [];
                foreach ($stmt as $row) {
                    $rows[] = [
                        'NAME' => $row['NAME'],
                        'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                        'HWID' => $row['HWID'],
                    ];
                }
                $the_kresylT = json_encode($rows);
                echo str_replace($altashfir, $tabadal, $the_kresylT);
                                $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
                    $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                $getINFO->bindParam(":appkey",$appkey);
                $getINFO->execute();
                $appinfo = $getINFO->fetchObject();
                if ($appinfo->DISC == "1")
                {
                    $webhookurl = $appinfo->DISCWH;
                    $timestamp = date("c", strtotime("now"));
                    $json_data = json_encode([
                        "content" => "_ _",
                        "tts" => false,
                        "embeds" => [
                            [
                                "title" => ":white_check_mark: Sign in Detected",
                                "type" => "rich",
                                "timestamp" => $timestamp,
                                "color" => hexdec( "3366ff" ),
                                "footer" => [
                                    "text" => "Umar - The Ultimate Solution",
                                    "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                ],
                                "image" => [
                                    "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                ],
                                "fields" => [
                                    [
                                        "name" => "• Username :",
                                        "value" => '```' . $userinfo->NAME . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Rank :",
                                        "value" => '```' . $userinfo->RANKUSER . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Create Date :",
                                        "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• Expire Date :",
                                        "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                        "inline" => true
                                    ],
                                    [
                                        "name" => "• HWID :",
                                        "value" => '```' . $userinfo->HWID . '```',
                                        "inline" => true
                                    ],
                                ]
                            ]
                        ]
                    
                    ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                    $ch = curl_init( $webhookurl );
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                    curl_setopt( $ch, CURLOPT_POST, 1);
                    curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                    curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                    curl_setopt( $ch, CURLOPT_HEADER, 0);
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                    $response = curl_exec( $ch );
                    curl_close( $ch );
                }
            }
        }
    }
    else
    {
        $expireW = $userinfo->EXPIREDATE;
        $dateW = time();
        if (intval($expireW) > intval($dateW))
        {
            if ($userinfo->HWID == "NULL")
            {
                $changeHwid = $database->prepare("UPDATE accounts SET HWID = :hwid WHERE NAME = :name AND PASSWORD = :password");
                $changeHwid->bindParam(":name",$username);
                $changeHwid->bindParam(":hwid",$hwid);
                $changeHwid->bindParam(":password",$password);
                if ($changeHwid->execute())
                {
                                $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                    $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                    $stmt->bindParam(":name",$username);
                    $stmt->bindParam(":password",$password);
                    $stmt->bindParam(":appkey",$appkey);
                    $stmt->bindParam(":appownerkey",$acckey);
                    $stmt->execute();
                    $rows = [];
                    foreach ($stmt as $row) {
                        $rows[] = [
                            'NAME' => $row['NAME'],
                            'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                            'HWID' => $row['HWID'],
                        ];
                    }
                    $the_klesylT = json_encode($rows);
                    echo str_replace($altashfir, $tabadal, $the_klesylT);
                                    $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
                        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                    $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                    $getINFO->bindParam(":appkey",$appkey);
                    $getINFO->execute();
                    $appinfo = $getINFO->fetchObject();
                    if ($appinfo->DISC == "1")
                    {
                        $webhookurl = $appinfo->DISCWH;
                        $timestamp = date("c", strtotime("now"));
                        $json_data = json_encode([
                            "content" => "_ _",
                            "tts" => false,
                            "embeds" => [
                                [
                                    "title" => ":white_check_mark: Sign in Detected",
                                    "type" => "rich",
                                    "timestamp" => $timestamp,
                                    "color" => hexdec( "3366ff" ),
                                    "footer" => [
                                        "text" => "Umar - The Ultimate Solution",
                                        "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                    ],
                                    "image" => [
                                        "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                    ],
                                    "fields" => [
                                        [
                                            "name" => "• Username :",
                                            "value" => '```' . $userinfo->NAME . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Rank :",
                                            "value" => '```' . $userinfo->RANKUSER . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Create Date :",
                                            "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Expire Date :",
                                            "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• HWID :",
                                            "value" => '```' . $userinfo->HWID . '```',
                                            "inline" => true
                                        ],
                                    ]
                                ]
                            ]
                        
                        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                        $ch = curl_init( $webhookurl );
                        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                        curl_setopt( $ch, CURLOPT_POST, 1);
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt( $ch, CURLOPT_HEADER, 0);
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                        $response = curl_exec( $ch );
                        curl_close( $ch );
                    }
                }
                else
                {
                    die("-IJ--JH--OL--MW--ME--ME--RS--OL--ON-_-WK--EP--RS--ME-_MV--RS--ON--XC--IJ--TW--EP-");
                }
            }
            else
            {
                $hwidlockop = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                $hwidlockop->bindParam(":appkey",$appkey);
                $hwidlockop->execute();
                $hwidlockinfo = $hwidlockop->fetchObject();
                if ($hwidlockinfo->HWID == "1")
                {
                    if ($userinfo->HWID == $hwid)
                    {
                                    $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                        $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                        $stmt->bindParam(":name",$username);
                        $stmt->bindParam(":password",$password);
                        $stmt->bindParam(":appkey",$appkey);
                        $stmt->bindParam(":appownerkey",$acckey);
                        $stmt->execute();
                        $rows = [];
                        foreach ($stmt as $row) {
                            $rows[] = [
                                'NAME' => $row['NAME'],
                                'RANKUSER' => $row['RANKUSER'],
                                'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                                'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                                'HWID' => $row['HWID'],
                            ];
                        }
                        $the_mmesylT = json_encode($rows);
                        echo str_replace($altashfir, $tabadal, $the_mmesylT);
                                        $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
                            $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                        $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                        $getINFO->bindParam(":appkey",$appkey);
                        $getINFO->execute();
                        $appinfo = $getINFO->fetchObject();
                        if ($appinfo->DISC == "1")
                        {
                            $webhookurl = $appinfo->DISCWH;
                            $timestamp = date("c", strtotime("now"));
                            $json_data = json_encode([
                                "content" => "_ _",
                                "tts" => false,
                                "embeds" => [
                                    [
                                        "title" => ":white_check_mark: Sign in Detected",
                                        "type" => "rich",
                                        "timestamp" => $timestamp,
                                        "color" => hexdec( "3366ff" ),
                                        "footer" => [
                                            "text" => "Umar - The Ultimate Solution",
                                            "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                        ],
                                        "image" => [
                                            "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                        ],
                                        "fields" => [
                                            [
                                                "name" => "• Username :",
                                                "value" => '```' . $userinfo->NAME . '```',
                                                "inline" => true
                                            ],
                                            [
                                                "name" => "• Rank :",
                                                "value" => '```' . $userinfo->RANKUSER . '```',
                                                "inline" => true
                                            ],
                                            [
                                                "name" => "• Create Date :",
                                                "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                                "inline" => true
                                            ],
                                            [
                                                "name" => "• Expire Date :",
                                                "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                                "inline" => true
                                            ],
                                            [
                                                "name" => "• HWID :",
                                                "value" => '```' . $userinfo->HWID . '```',
                                                "inline" => true
                                            ],
                                        ]
                                    ]
                                ]
                            
                            ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                            $ch = curl_init( $webhookurl );
                            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                            curl_setopt( $ch, CURLOPT_POST, 1);
                            curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                            curl_setopt( $ch, CURLOPT_HEADER, 0);
                            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                            $response = curl_exec( $ch );
                            curl_close( $ch );
                        }
                    }
                    else if ($userinfo->HWID != $hwid)
                    {
                        die("DP--BA--IJ-MV-_MV--MW--RS--EP-_-JH--MW--ON-_-BI--XC--ON--OL-DP-");
                    }
                }
                else
                {
                                $typeofi = "Login";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                    $stmt = $database->prepare("SELECT * FROM accounts WHERE NAME = :name AND PASSWORD = :password AND APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
                    $stmt->bindParam(":name",$username);
                    $stmt->bindParam(":password",$password);
                    $stmt->bindParam(":appkey",$appkey);
                    $stmt->bindParam(":appownerkey",$acckey);
                    $stmt->execute();
                    $rows = [];
                    foreach ($stmt as $row) {
                        $rows[] = [
                            'NAME' => $row['NAME'],
                            'RANKUSER' => $row['RANKUSER'],
                        'CREATEDATE' => convert_t($row['CREATEDATE'], $acckey, $database, $altashfir, $tabadal),
                        'EXPIREDATE' => convert_t($row['EXPIREDATE'], $acckey, $database, $altashfir, $tabadal),
                            'HWID' => $row['HWID'],
                        ];
                    }
                    $the_tnesylT = json_encode($rows);
                    echo str_replace($altashfir, $tabadal, $the_tnesylT);
                                    $timen = time();
            $changeLastLogin = $database->prepare("UPDATE accounts SET LASTLOGINDATE = :lastlogindate WHERE NAME = :name AND PASSWORD = :password");
            $changeLastLogin->bindParam(":name",$username);
            $changeLastLogin->bindParam(":lastlogindate",$timen);
            $changeLastLogin->bindParam(":password",$password);
            $changeLastLogin->execute();
                        $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                    $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
                    $getINFO->bindParam(":appkey",$appkey);
                    $getINFO->execute();
                    $appinfo = $getINFO->fetchObject();
                    if ($appinfo->DISC == "1")
                    {
                        $webhookurl = $appinfo->DISCWH;
                        $timestamp = date("c", strtotime("now"));
                        $json_data = json_encode([
                            "content" => "_ _",
                            "tts" => false,
                            "embeds" => [
                                [
                                    "title" => ":white_check_mark: Sign in Detected",
                                    "type" => "rich",
                                    "timestamp" => $timestamp,
                                    "color" => hexdec( "3366ff" ),
                                    "footer" => [
                                        "text" => "Umar - The Ultimate Solution",
                                        "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                                    ],
                                    "image" => [
                                        "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                                    ],
                                    "fields" => [
                                        [
                                            "name" => "• Username :",
                                            "value" => '```' . $userinfo->NAME . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Rank :",
                                            "value" => '```' . $userinfo->RANKUSER . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Create Date :",
                                            "value" => '```' . convert_t($userinfo->CREATEDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• Expire Date :",
                                            "value" => '```' . convert_t($userinfo->EXPIREDATE, $acckey, $database, $altashfir, $tabadal) . '```',
                                            "inline" => true
                                        ],
                                        [
                                            "name" => "• HWID :",
                                            "value" => '```' . $userinfo->HWID . '```',
                                            "inline" => true
                                        ],
                                    ]
                                ]
                            ]
                        
                        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                        $ch = curl_init( $webhookurl );
                        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                        curl_setopt( $ch, CURLOPT_POST, 1);
                        curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt( $ch, CURLOPT_HEADER, 0);
                        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                        $response = curl_exec( $ch );
                        curl_close( $ch );
                    }
                }
            }
        }
        else
        {
            die("-EP--WK--YU--EP--OL--ME--IJ--IS--ON--IJ--MW--JH-_DP--XC--EP-_-RS--RE--IS--IJ--ME--RS-MV-");
        }
    }
}
else
{
    die("-IJ--JH--OL--MW--ME--ME--RS--OL--ON-_-TW--MW--WZ--IJ--JH-_MV--RS--ON--XC--IJ--TW--EP-");
}
}
else if ($type === '-ME--RS--WZ--IJ--EP--ON--RS--ME-')
{
    $name = $_POST['username'];
    $password = $_POST['passw0rd'];
    $invite = $_POST['invite'];
    $hwid = $_POST['hwid'];
    $appkey = $_POST['appkey'];
    $acckey = $_POST['acckey'];
    $name = str_replace($tabadal, $altashfir, $name);
    $password = str_replace($tabadal, $altashfir, $password);
    $invite = str_replace($tabadal, $altashfir, $invite);
    $hwid = str_replace($tabadal, $altashfir, $hwid);
    $appkey = str_replace($tabadal, $altashfir, $appkey);
    $acckey = str_replace($tabadal, $altashfir, $acckey);
    $name = str_replace(' ', '', $name);
    $checkN = $database->prepare("SELECT * FROM accounts WHERE NAME = :NAME AND APPKEY = :APPKEY");
    $checkN->bindParam("NAME",$name);
    $checkN->bindParam("APPKEY",$appkey);
    $checkN->execute();
    if ($checkN->rowCount() > 0)
    {
        die("-JH--XC--BI--RS-_-XC--TW--ME--RS--XC-MV--IN-_-WK--EP--RS-MV-");
    }
    else
    {
    $status = 'UNAPPLIED';
    $unstatus = 'APPLIED';
    $checkK = $database->prepare("SELECT * FROM userskeys WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND STATUS = :STATUS");
    $checkK->bindParam("KEYID",$invite);
    $checkK->bindParam("APPKEY",$appkey);
    $checkK->bindParam("STATUS",$status);
    $checkK->execute();
    $key_infow = $checkK->fetchObject();
    if ($checkK->rowCount() > 0)
    {
        $checkAcc = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :appownerkey");
        $checkAcc->bindParam("appownerkey",$acckey);
        $checkAcc->execute();
        $acc_user_info = $checkAcc->fetchObject();
        if ($acc_user_info->RANK === "0")
        {
            $checkbb = $database->prepare("SELECT * FROM accounts WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
            $checkbb->bindParam("appkey",$appkey);
            $checkbb->bindParam("appownerkey",$acckey);
            $checkbb->execute();
            $num = $checkbb->rowCount();
            if ($num >= 100)
            {
                die("-BI--XC--RE--IJ--BI--WK--BI-_-WK--EP--RS--ME--EP-");
            }
            else
            {
                $checkKw = $database->prepare("SELECT * FROM userskeys WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND STATUS = :STATUS");
                $checkKw->bindParam("KEYID",$invite);
                $checkKw->bindParam("APPKEY",$appkey);
                $checkKw->bindParam("STATUS",$status);
                $checkKw->execute();
                $keyinfof = $checkKw->fetchObject();
                $Rankf = $keyinfof->RANKUSER;
                $EXPIREdatef = expire_t($keyinfof->EXPIREDATE);
                $timenow =time();
                $RegUserw = $database->prepare("INSERT INTO accounts(NAME,PASSWORD,RANKUSER,CREATEDATE,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:NAME,:PASSWORD,:RANKUSER,:CREATEDATE,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
                $RegUserw->bindParam("NAME",$name);
                $RegUserw->bindParam("PASSWORD",$password);
                $RegUserw->bindParam("RANKUSER",$Rankf);
                $RegUserw->bindParam("CREATEDATE",$timenow);
                $RegUserw->bindParam("EXPIREDATE",$EXPIREdatef);
                $RegUserw->bindParam("APPKEY",$appkey);
                $RegUserw->bindParam("APPOWNERKEY",$acckey);
                if ($RegUserw->execute())
                {
                    $changeStatusz = $database->prepare("UPDATE userskeys SET STATUS = :UNSTATUS WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND APPOWNERKEY = :APPOWNERKEY");
                    $changeStatusz->bindParam("UNSTATUS",$unstatus);
                    $changeStatusz->bindParam("KEYID",$invite);
                    $changeStatusz->bindParam("APPKEY",$appkey);
                    $changeStatusz->bindParam("APPOWNERKEY",$acckey);
                    if ($changeStatusz->execute())
                    {
                                    $typeofi = "Register";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                        echo "-IS--XC--EP--EP--RS-MV-";
                            $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                    }
                }
                else
                {
                    die("-IJ--JH--OL--MW--ME--ME--RS--OL--ON-_-ME--RS--WZ--IJ--EP--ON--RS--ME-_MV--RS--ON--XC--IJ--TW--EP-");
                }
            }
        }
        else
        {
            $checkK = $database->prepare("SELECT * FROM userskeys WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND STATUS = :STATUS");
            $checkK->bindParam("KEYID",$invite);
            $checkK->bindParam("APPKEY",$appkey);
            $checkK->bindParam("STATUS",$status);
            $checkK->execute();
        $keyinfo = $checkK->fetchObject();
        $Rank = $keyinfo->RANKUSER;
        $EXPIREdate = expire_t($keyinfo->EXPIREDATE);
        $timenoww =time();
        $RegUser = $database->prepare("INSERT INTO accounts(NAME,PASSWORD,RANKUSER,CREATEDATE,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:NAME,:PASSWORD,:RANKUSER,:CREATEDATE,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
        $RegUser->bindParam("NAME",$name);
        $RegUser->bindParam("PASSWORD",$password);
        $RegUser->bindParam("RANKUSER",$Rank);
        $RegUser->bindParam("CREATEDATE",$timenoww);
        $RegUser->bindParam("EXPIREDATE",$EXPIREdate);
        $RegUser->bindParam("APPKEY",$appkey);
        $RegUser->bindParam("APPOWNERKEY",$acckey);
        if ($RegUser->execute())
        {
            $changeStatusw = $database->prepare("UPDATE userskeys SET STATUS = :UNSTATUS WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND APPOWNERKEY = :APPOWNERKEY");
            $changeStatusw->bindParam("UNSTATUS",$unstatus);
            $changeStatusw->bindParam("KEYID",$invite);
            $changeStatusw->bindParam("APPKEY",$appkey);
            $changeStatusw->bindParam("APPOWNERKEY",$acckey);
            if ($changeStatusw->execute())
            {
                                                    $typeofi = "Register";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
            echo "-IS--XC--EP--EP--RS-MV-";
                $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
            $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
            $getINFO->bindParam(":appkey",$appkey);
            $getINFO->execute();
            $appinfo = $getINFO->fetchObject();
            if ($appinfo->DISC == "1")
            {
                $webhookurl = $appinfo->DISCWH;
                $timestamp = date("c", strtotime("now"));
                $json_data = json_encode([
                    "content" => "_ _",
                    "tts" => false,
                    "embeds" => [
                        [
                            "title" => ":pencil: Sign up Detected",
                            "type" => "rich",
                            "timestamp" => $timestamp,
                            "color" => hexdec( "3366ff" ),
                            "footer" => [
                                "text" => "Umar - The Ultimate Solution",
                                "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                            ],
                            "image" => [
                                "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                            ],
                            "fields" => [
                                [
                                    "name" => "• Username :",
                                    "value" => '```' . $name . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• Rank :",
                                    "value" => '```' . $key_infow->RANKUSER . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• Create Date :",
                                    "value" => '```' . $timenoww . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• Expire Date :",
                                    "value" => '```' . convert_t(expire_t($key_infow->EXPIREDATE), $acckey, $database, $altashfir, $tabadal) . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• HWID :",
                                    "value" => '```' . $hwid . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• Key Used :",
                                    "value" => '```' . $invite . '```',
                                    "inline" => true
                                ],
                            ]
                        ]
                    ]
                
                ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                $ch = curl_init( $webhookurl );
                curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                curl_setopt( $ch, CURLOPT_POST, 1);
                curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt( $ch, CURLOPT_HEADER, 0);
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                $response = curl_exec( $ch );
                curl_close( $ch );
            }
            }
        }
        else
        {
            die("-IJ--JH--OL--MW--ME--ME--RS--OL--ON-_-ME--RS--WZ--IJ--EP--ON--RS--ME-_MV--RS--ON--XC--IJ--TW--EP-");
        }
        }
    }
    else
    {
        die("-CA--RS--IN-_-JH--MW--ON-_-EV--MW--WK--JH-MV-");
    }
    }
}
else if ($type === '-RS-_-ME--RS--WZ--IJ--EP--ON--RS--ME-')
{
    $name = $_POST['username'];
    $password = $_POST['passw0rd'];
    $invite = $_POST['invite'];
    $appkey = $_POST['appkey'];
    $acckey = $_POST['acckey'];
    $name = str_replace($tabadal, $altashfir, $name);
    $password = str_replace($tabadal, $altashfir, $password);
    $invite = str_replace($tabadal, $altashfir, $invite);
    $appkey = str_replace($tabadal, $altashfir, $appkey);
    $acckey = str_replace($tabadal, $altashfir, $acckey);
    $name = str_replace(' ', '', $name);
    $checkN = $database->prepare("SELECT * FROM accounts WHERE NAME = :NAME AND APPKEY = :APPKEY");
    $checkN->bindParam("NAME",$name);
    $checkN->bindParam("APPKEY",$appkey);
    $checkN->execute();
    if ($checkN->rowCount() > 0)
    {
        die("-WK--EP--RS-MV-");
    }
    else
    {
    $status = 'UNAPPLIED';
    $unstatus = 'APPLIED';
    $checkK = $database->prepare("SELECT * FROM userskeys WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND STATUS = :STATUS");
    $checkK->bindParam("KEYID",$invite);
    $checkK->bindParam("APPKEY",$appkey);
    $checkK->bindParam("STATUS",$status);
    $checkK->execute();
    $key_infow = $checkK->fetchObject();
    if ($checkK->rowCount() > 0)
    {
        $checkAcc = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :appownerkey");
        $checkAcc->bindParam("appownerkey",$acckey);
        $checkAcc->execute();
        $acc_user_info = $checkAcc->fetchObject();
        if ($acc_user_info->RANK === "0")
        {
            $checkbb = $database->prepare("SELECT * FROM accounts WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey");
            $checkbb->bindParam("appkey",$appkey);
            $checkbb->bindParam("appownerkey",$acckey);
            $checkbb->execute();
            $num = $checkbb->rowCount();
            if ($num >= 100)
            {
                die("-WK--IS--WZ--ME--XC-MV--RS-");
            }
            else
            {
                $checkKw = $database->prepare("SELECT * FROM userskeys WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND STATUS = :STATUS");
                $checkKw->bindParam("KEYID",$invite);
                $checkKw->bindParam("APPKEY",$appkey);
                $checkKw->bindParam("STATUS",$status);
                $checkKw->execute();
                $keyinfof = $checkKw->fetchObject();
                $Rankf = $keyinfof->RANKUSER;
                $EXPIREdatef = expire_t($keyinfof->EXPIREDATE);
                $timenow =time();
                $RegUserw = $database->prepare("INSERT INTO accounts(NAME,PASSWORD,RANKUSER,CREATEDATE,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:NAME,:PASSWORD,:RANKUSER,:CREATEDATE,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
                $RegUserw->bindParam("NAME",$name);
                $RegUserw->bindParam("PASSWORD",$password);
                $RegUserw->bindParam("RANKUSER",$Rankf);
                $RegUserw->bindParam("CREATEDATE",$timenow);
                $RegUserw->bindParam("EXPIREDATE",$EXPIREdatef);
                $RegUserw->bindParam("APPKEY",$appkey);
                $RegUserw->bindParam("APPOWNERKEY",$acckey);
                if ($RegUserw->execute())
                {
                    $changeStatusz = $database->prepare("UPDATE userskeys SET STATUS = :UNSTATUS WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND APPOWNERKEY = :APPOWNERKEY");
                    $changeStatusz->bindParam("UNSTATUS",$unstatus);
                    $changeStatusz->bindParam("KEYID",$invite);
                    $changeStatusz->bindParam("APPKEY",$appkey);
                    $changeStatusz->bindParam("APPOWNERKEY",$acckey);
                    if ($changeStatusz->execute())
                    {
                                                            $typeofi = "Register";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
                        echo "-IS--XC--EP--EP--RS-MV-";
                            $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
                    }
                }
                else
                {
                    die("-IJ--JH--OL--MW--ME--ME--RS--OL--ON-");
                }
            }
        }
        else
        {
            $checkK = $database->prepare("SELECT * FROM userskeys WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND STATUS = :STATUS");
            $checkK->bindParam("KEYID",$invite);
            $checkK->bindParam("APPKEY",$appkey);
            $checkK->bindParam("STATUS",$status);
            $checkK->execute();
        $keyinfo = $checkK->fetchObject();
        $Rank = $keyinfo->RANKUSER;
        $EXPIREdate = expire_t($keyinfo->EXPIREDATE);
        $timenoww =time();
        $RegUser = $database->prepare("INSERT INTO accounts(NAME,PASSWORD,RANKUSER,CREATEDATE,EXPIREDATE,APPKEY,APPOWNERKEY) VALUES(:NAME,:PASSWORD,:RANKUSER,:CREATEDATE,:EXPIREDATE,:APPKEY,:APPOWNERKEY)");
        $RegUser->bindParam("NAME",$name);
        $RegUser->bindParam("PASSWORD",$password);
        $RegUser->bindParam("RANKUSER",$Rank);
        $RegUser->bindParam("CREATEDATE",$timenoww);
        $RegUser->bindParam("EXPIREDATE",$EXPIREdate);
        $RegUser->bindParam("APPKEY",$appkey);
        $RegUser->bindParam("APPOWNERKEY",$acckey);
        if ($RegUser->execute())
        {
            $changeStatusw = $database->prepare("UPDATE userskeys SET STATUS = :UNSTATUS WHERE KEYID = :KEYID AND APPKEY = :APPKEY AND APPOWNERKEY = :APPOWNERKEY");
            $changeStatusw->bindParam("UNSTATUS",$unstatus);
            $changeStatusw->bindParam("KEYID",$invite);
            $changeStatusw->bindParam("APPKEY",$appkey);
            $changeStatusw->bindParam("APPOWNERKEY",$acckey);
            if ($changeStatusw->execute())
            {
                                                    $typeofi = "Register";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
            echo "-IS--XC--EP--EP--RS-MV-";
                $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
            $getINFO = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
            $getINFO->bindParam(":appkey",$appkey);
            $getINFO->execute();
            $appinfo = $getINFO->fetchObject();
            if ($appinfo->DISC == "1")
            {
                $webhookurl = $appinfo->DISCWH;
                $timestamp = date("c", strtotime("now"));
                $json_data = json_encode([
                    "content" => "_ _",
                    "tts" => false,
                    "embeds" => [
                        [
                            "title" => ":pencil: Sign up Detected",
                            "type" => "rich",
                            "timestamp" => $timestamp,
                            "color" => hexdec( "3366ff" ),
                            "footer" => [
                                "text" => "Umar - The Ultimate Solution",
                                "icon_url" => "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2F5%2F50%2FBlack_colour.jpg&f=1&nofb=1&ipt=4cf4bce0390b47febab7909813a766d6761cda8ce1458959009af90555a10142&ipo=images"
                            ],
                            "image" => [
                                "url" => "https://images-ext-1.discordapp.net/external/ANpg5P0IxvWQ078E6YdEqLoKmnKvzrNLyIgIzD2no1g/https/media.tenor.com/GGyzIMKUTeEAAAPo/message-notification.mp4?size=600"
                            ],
                            "fields" => [
                                [
                                    "name" => "• Username :",
                                    "value" => '```' . $name . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• Rank :",
                                    "value" => '```' . $key_infow->RANKUSER . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• Create Date :",
                                    "value" => '```' . $timenoww . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• Expire Date :",
                                    "value" => '```' . convert_t(expire_t($key_infow->EXPIREDATE), $acckey, $database, $altashfir, $tabadal) . '```',
                                    "inline" => true
                                ],
                                [
                                    "name" => "• Key Used :",
                                    "value" => '```' . $invite . '```',
                                    "inline" => true
                                ],
                            ]
                        ]
                    ]
                
                ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );
                $ch = curl_init( $webhookurl );
                curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
                curl_setopt( $ch, CURLOPT_POST, 1);
                curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
                curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt( $ch, CURLOPT_HEADER, 0);
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                $response = curl_exec( $ch );
                curl_close( $ch );
            }
            }
        }
        else
        {
            die("-IJ--JH--OL--MW--ME--ME--RS--OL--ON-");
        }
        }
    }
    else
    {
        die("-JH--MW--ON-_-EV--MW--WK--JH-MV-");
    }
    }
}
else if ($type === '-NB--XC--ME-')
{
    $varid = $_POST['varid'];
    $appkey = $_POST['appkey'];
    $acckey = $_POST['acckey'];
$varid = str_replace($tabadal, $altashfir, $varid);
$appkey = str_replace($tabadal, $altashfir, $appkey);
$acckey = str_replace($tabadal, $altashfir, $acckey);
$findowner = $database->prepare("SELECT * FROM users WHERE ACCOUNTKEY = :appownerkey");
$findowner->bindParam(":appownerkey",$acckey);
$findowner->execute();
if ($findowner->rowCount() === 1)
{
$isqlreq = $database->prepare("SELECT * FROM apps WHERE APPKEY = :appkey");
$isqlreq->bindParam(":appkey",$appkey);
$isqlreq->execute();
if ($isqlreq->rowCount() === 1)
{
    $variableFIND = $database->prepare("SELECT * FROM vars WHERE APPKEY = :appkey AND APPOWNERKEY = :appownerkey AND VARID = :varid");
    $variableFIND->bindParam(":appkey",$appkey);
    $variableFIND->bindParam(":appownerkey",$acckey);
    $variableFIND->bindParam(":varid",$varid);
    $variableFIND->execute();
    $varVALUE = $variableFIND->fetchObject();
    if ($variableFIND->rowCount() === 1)
    {
                                            $typeofi = "Grabbing Variable";
    $timenowww =time();
    $RegInitw = $database->prepare("INSERT INTO req(TYPE,DATE,APPKEY,APPOWNERKEY) VALUES(:TYPE,:DATE,:APPKEY,:APPOWNERKEY)");
    $RegInitw->bindParam("TYPE",$typeofi);
    $RegInitw->bindParam("DATE",$timenowww);
    $RegInitw->bindParam("APPKEY",$appkey);
    $RegInitw->bindParam("APPOWNERKEY",$acckey);
    $RegInitw->execute();
        die ($varVALUE->VARVALUE);
            $IP = getClientIP();
    $removeIP = $database->prepare("DELETE FROM ips WHERE IP = :ip");
    $removeIP->bindParam(":ip",$IP);    
    $removeIP->execute();
    $removeIPB = $database->prepare("DELETE FROM ipsban WHERE IP = :ip");
    $removeIPB->bindParam(":ip",$IP);    
    $removeIPB->execute();
    }
    else
    {
        die("-NB--XC--ME-_-JH--MW--ON-_-EV--MW--WK--JH-MV-");
    }
}
else
{
    die("-IJ--JH--OL--MW--ME--ME--RS--OL--ON-_-XC--IS--IS--TW--IJ--OL--XC--ON--IJ--MW--JH-_MV--RS--ON--XC--IJ--TW--EP-");
}
}
else
{
    die("-IJ--JH--OL--MW--ME--ME--RS--OL--ON-_-XC--IS--IS--TW--IJ--OL--XC--ON--IJ--MW--JH-_MV--RS--ON--XC--IJ--TW--EP-");
}
}
else if ($type === "e")
{
    $data = $_POST['O'];
    $enc = enc($data);
    echo $enc;
}
else if ($type === "d")
{
    $data = $_POST['O'];
    $dec = dec($data);
    echo $dec;
}
else if ($type === "x")
{
    $data = $_POST['O'];
    $sha512_c = hashpass($data);
    echo $sha512_c;
}
else if ($type === "c")
{
    $data = $_POST['O'];
    $given = $_POST['Y'];
    $jsonData = $data;
    $jsonData = str_replace('\\', '', $jsonData);
    $people= json_decode($jsonData, true);
    $count= count($people);
   for ($i=0; $i < $count; $i++) 
   { 
         echo $people[$i][$given] . "\n";
   }
}
else
{
    readfile("../404.html");
}



function dec($sifr) 
{
    $altashfir = array("0","1","2","3","4","5","6","7","8","9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
    $tabadal = array("-QZ-", "-SA-", "-IF-", "DE-", "-EE-", "-JJ-", "-GG-", "MP-", "-WI-", "-ZF-","-XC-", "-YU-", "-OL-", "MV-", "-RS-", "-EV-", "-WZ-", "DP-", "-IJ-", "-KN-", "-CA-", "-TW-", "-BI-", "-JH-", "-MW-", "-IS-", "-LA-", "-ME-", "-EP-", "-ON-", "-WK-", "-NB-", "-BA-", "-RE-", "-IN-", "-LU-");
    $wahid = str_replace($tabadal, $altashfir, $sifr);
    return $wahid;
} 

function enc($sifr) 
{
    $altashfir = array("0","1","2","3","4","5","6","7","8","9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
    $tabadal = array("-QZ-", "-SA-", "-IF-", "DE-", "-EE-", "-JJ-", "-GG-", "MP-", "-WI-", "-ZF-","-XC-", "-YU-", "-OL-", "MV-", "-RS-", "-EV-", "-WZ-", "DP-", "-IJ-", "-KN-", "-CA-", "-TW-", "-BI-", "-JH-", "-MW-", "-IS-", "-LA-", "-ME-", "-EP-", "-ON-", "-WK-", "-NB-", "-BA-", "-RE-", "-IN-", "-LU-");
    $aithnayn = str_replace($altashfir, $tabadal, $sifr);
    return $aithnayn;
}
function hashpass($sifr) 
{
    $aithnayn = hash($Secure_Hash_Algorithm, $sifr);
    return $aithnayn;
}
?>