<div align="center">
    <img src="./img/preview.png" alt="" align="center" />
</div>

## Overview ##

An advanced authentication system with ready-to-use SDKs.

## Features ##

Manage Applications.
Manage Users accounts.
Manage Variables.
Manage Root accounts.
Manage Discord webhook.
Manage Discord bot.
Admin panel.
Easy-to-use SDKs.
API.
Modern design.

## Requirements ##

- Web server
- MySQL
- PHP

## Installation ##

- Import 'db_structure.sql' into your database.
- Go to 'connection/db.php', and fill the variables needed there.

## API Usage ##

### Example of an Initialization POST request ###
```yaml
POST /api HTTP/1.1
Host: domain.com
user-agent: umar
Content-Type: application/x-www-form-urlencoded
Content-Length: 37

s0rt=init&appkey=yourappkey&acckey=youracckey
```

#### Operation Successful ####
```yaml
{"APPNAME":"appname","STATUS":"1","LOGGED":"loggedmessage","REGISTERED":"registeredmessage","PAUSED":"pausedmessage"}
```

#### Invalidated Application Key ####
```yaml
incorrect_application_key
```

#### Invalidated Account Key ####
```yaml
incorrect_account_key
```

### Example of a Login POST request ###
```yaml
POST /api HTTP/1.1
Host: domain.com
user-agent: umar
Content-Type: application/x-www-form-urlencoded
Content-Length: 100

s0rt=login&username=username&passw0rd=userpassword&hwid=userhwid&appkey=yourappkey&acckey=youracckey
```

#### Operation Successful ####
```yaml
{"NAME":"username","RANKUSER":"rank","CREATEDATE":"createdate","EXPIREDATE":"expiredate","HWID":"hwid"}
```

#### Invalidated User Credentials ####
```yaml
incorrect_user_details
```

#### User Subscription Expired ####
```yaml
subscription_has_expired
```

#### Unmatched HWID ####
```yaml
hwid_does_not_match
```

### Example of a Register POST request ###
```yaml
POST /api HTTP/1.1
Host: domain.com
user-agent: umar
Content-Type: application/x-www-form-urlencoded
Content-Length: 101

s0rt=register&username=username&passw0rd=userpassword&key=userkey&appkey=yourappkey&acckey=youracckey
```

#### Operation Successful ####
```yaml
success
```

#### Name Already Taken ####
```yaml
name_used
```

#### Key Already Used ####
```yaml
key_used
```

#### Invalidated License Key ####
```yaml
key_not_found
```

#### Upgrade to Pro Plan ####
```yaml
limit_reached
```

### Example of a Variable POST request ###
```yaml
POST /api HTTP/1.1
Host: domain.com
user-agent: umar
Content-Type: application/x-www-form-urlencoded
Content-Length: 70

s0rt=variable&varid=yourvariableid&appkey=yourappkey&acckey=youracckey
```

#### Operation Successful ####
```yaml
your variable value
```

#### Invalidated Variable Key ####
```yaml
var_not_found
```

#### Invalidated Application Key ####
```yaml
incorrect_application_key
```

#### Invalidated Account Key ####
```yaml
incorrect_account_key
```

### Example of an Upgrade POST request ###
```yaml
POST /api HTTP/1.1
Host: domain.com
user-agent: umar
Content-Type: application/x-www-form-urlencoded
Content-Length: 78

s0rt=upgrade&username=username&key=userkey&appkey=yourappkey&acckey=youracckey
```

#### Operation Successful ####
```yaml
success
```

#### Invalidated Username ####
```yaml
user_not_found
```

#### Invalidated Key ####
```yaml
key_not_found
```

#### Invalidated Application Key ####
```yaml
incorrect_application_key
```

#### Invalidated Account Key ####
```yaml
incorrect_account_key
```

### Example of a HWID Reset POST request ###
```yaml
POST /api HTTP/1.1
Host: domain.com
user-agent: umar
Content-Type: application/x-www-form-urlencoded
Content-Length: 84

s0rt=hwidreset&username=username&appid=yourappid&appkey=yourappkey&acckey=youracckey
```

#### Operation Successful ####
```yaml
success
```

#### Invalidated User Info ####
```yaml
user_not_found
```

#### Invalidated Application Info ####
```yaml
incorrect_application_info
```

#### Invalidated Account Key ####
```yaml
incorrect_account_key
```

## Note ##

I do not encourage anyone to use any of Twitter, Discord, YouTube or even Google developer API. They are all privacy invaders and the reason why I mentioned them in my code is to show you how things work.

## License ##

see the LICENSE file.

***Uses must be at all costs under Islamic law, no exceptions.***

## Donation ##

Donations of all sizes are appreciated!

<div align="center">
    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Flogos-download.com%2Fwp-content%2Fuploads%2F2018%2F05%2FMonero_logo_colour.png&f=1&nofb=1&ipt=3aa51411bac56ed1aee446eb0b3e621c04e9fe0b5ace0ca0ea9c988aca98536a&ipo=images" alt="" align="center" width="130" height="130"/>


    86k3Sc1RtvYbAuyFbUUfTdRch23dM5tSmTxQFQeDV1TJgfkCXG9Z72YjQvevrvx36wicVADRLhYZu9dCM6AhBSTvAsi2WZz

</div>