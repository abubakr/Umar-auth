SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE `accounts` (
  `ID` int(255) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `RANKUSER` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CREATEDATE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LASTLOGINDATE` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N/A',
  `EXPIREDATE` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Lifetime',
  `HWID` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NULL',
  `APPKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPOWNERKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `apps` (
  `ID` int(255) NOT NULL,
  `STATUS` int(255) NOT NULL DEFAULT '1',
  `APPNAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LOGGED` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'You have successfully logged in!',
  `REGISTERED` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'You have successfully registered!',
  `PAUSED` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Looks like the Application is not enabled at moment, try again later!',
  `HWID` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `REQKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `DISC` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `DISCWH` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'https://discord.com/api/webhooks/xxxxxxxxxxx/xxxxxxxxxxxxx-xxxxxx',
  `APPOWNERID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPUNID` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `blacklisted` (
  `ID` int(11) NOT NULL,
  `HWID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPOWNERKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `discord` (
  `ID` int(11) NOT NULL,
  `USERID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPOWNERKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ips` (
  `ID` int(11) NOT NULL,
  `IP` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `ipsban` (
  `ID` int(11) NOT NULL,
  `IP` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `REVOKETIME` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `req` (
  `ID` int(11) NOT NULL,
  `TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPOWNERKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `rootpanel` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CREATEDATE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPOWNERKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CURRAPP` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `users` (
  `ID` int(255) NOT NULL,
  `BAN` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATEFORMAT` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Y/m/d, h:i a',
  `ACCOUNTKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACCOUNTID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `REF` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `RANK` int(255) NOT NULL DEFAULT '0',
  `APPS` int(255) NOT NULL DEFAULT '0',
  `CURRAPP` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `userskeys` (
  `ID` int(11) NOT NULL,
  `KEYID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `RANKUSER` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `STATUS` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'UNAPPLIED',
  `EXPIREDATE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPOWNERKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `vars` (
  `ID` int(11) NOT NULL,
  `VARID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VARNAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VARVALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `APPOWNERKEY` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `accounts`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `apps`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `blacklisted`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `discord`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `ips`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `ipsban`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `req`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `rootpanel`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `userskeys`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `vars`
  ADD PRIMARY KEY (`ID`);


ALTER TABLE `accounts`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `apps`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `blacklisted`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `discord`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `ips`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `ipsban`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `req`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `rootpanel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `userskeys`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `vars`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
