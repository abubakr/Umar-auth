<?php

global $Host_Name;
global $YouTube;
global $Twitter;
global $Discord_Server_Invite;
global $Discord_Bot_Invite;
global $Support_Email;
global $Support_Phone_Number;
global $Secure_Hash_Algorithm;
global $Default_Discord_Webhook;
global $Default_Login_Message;
global $Default_Register_Message;
global $Default_Pause_Message;

$hostdb = ""; /* -> Your database host. */

$namedb = ""; /* -> Your database name. */

$usernamedb = ""; /* -> Your database username. */

$passworddb = ""; /* -> Your database password. */

$Host_Name = "Website.net"; /* Your hostname here. */

$YouTube = "https://www.youtube.com/channel/XXX"; /* Your auth YouTube channel. */

$Twitter = "https://twitter.com/XXX"; /* Your auth Twitter page. */

$Discord_Server_Invite = "https://discord.gg/XXX"; /* Your auth Discord server. */

$Discord_Bot_Invite = "https://discord.com/oauth2/authorize?client_id=XXX"; /* Your auth Discord bot. */

$Support_Email = "support@website.net"; /* Your auth email here. */

$Support_Phone_Number = "+000"; /* Your auth phone number here. */

$Secure_Hash_Algorithm = "SHA512"; /* Don't touch this. */

$Default_Discord_Webhook = "https://discord.com/api/webhooks/xxxxxxxxxxx/xxxxxxxxxxxxx-xxxxxx"; /* Default discord webhook on reset. */

$Default_Login_Message = "You have successfully logged in!"; /* Default login success message on reset. */

$Default_Register_Message = "You have successfully registered!"; /* Default register success message on reset. */

$Default_Pause_Message = "Looks like the Application is not enabled at moment, try again later!"; /* Default paused app message on reset. */
?>