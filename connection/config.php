<?php

require('db.php');

$dsn = "mysql:host=$hostdb;dbname=$namedb;";
$database = new PDO($dsn, $usernamedb, $passworddb);
if (!$database)
{
  die('down'); /* -> prints "down" message and terminates the current script, if database false. */
}
?>